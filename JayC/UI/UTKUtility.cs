using System;
using UnityEngine;
using UnityEngine.UIElements;

namespace JayC.UI.UIToolkit
{
    public static class UTKUtility
    {
        /// <summary>
        /// Shows a VisualElement with USS-defined animation duration.
        /// </summary>
        public static void ShowWithAnimation(VisualElement element, string customShowAnimationClass = null, Action onComplete = null)
        {
            if (element == null) return;

            // Remove any previous hide animation class
            element.RemoveFromClassList("hide-animation");

            // Apply the custom or default show animation class
            if (!string.IsNullOrEmpty(customShowAnimationClass))
            {
                element.AddToClassList(customShowAnimationClass);
            }
            else
            {
                element.AddToClassList("show-animation"); // Default animation
            }

            // Set element visible
            element.style.display = DisplayStyle.Flex;

            // Retrieve the animation duration from the element's style (transition-duration or animation-duration in USS)
            float animationDuration = GetAnimationDurationFromStyle(element);

            // Remove the show animation class after the animation completes
            element.schedule.Execute(() =>
            {
                element.RemoveFromClassList(customShowAnimationClass ?? "show-animation");
                onComplete?.Invoke(); // Optional callback when animation completes
            }).StartingIn((long)(animationDuration * 1000)); // Convert duration to milliseconds
            Debug.Log(animationDuration * 1000);
        }

        /// <summary>
        /// Hides a VisualElement with USS-defined animation duration.
        /// </summary>
        public static void HideWithAnimation(VisualElement element, string customHideAnimationClass = null, Action onComplete = null)
        {
            if (element == null) return;

            // Apply the custom or default hide animation class
            if (!string.IsNullOrEmpty(customHideAnimationClass))
            {
                element.AddToClassList(customHideAnimationClass);
            }
            else
            {
                element.AddToClassList("hide-animation"); // Default animation
            }

            // Retrieve the animation duration from the element's style (transition-duration or animation-duration in USS)
            float animationDuration = GetAnimationDurationFromStyle(element);

            // Hide the element after the animation completes
            element.schedule.Execute(() =>
            {
                element.style.display = DisplayStyle.None;
                element.RemoveFromClassList(customHideAnimationClass ?? "hide-animation");
                onComplete?.Invoke(); // Optional callback when animation completes
            }).StartingIn((long)(animationDuration * 1000)); // Convert duration to milliseconds
            Debug.Log(animationDuration * 1000);

        }

        /// <summary>
        /// Retrieves the animation duration from the element's style.
        /// </summary>
        private static float GetAnimationDurationFromStyle(VisualElement element)
        {
            foreach (var timeValue in element.resolvedStyle.transitionDuration)
            {
                // Check the time unit and convert to seconds
                if (timeValue.unit == TimeUnit.Second)
                {
                    return timeValue.value;
                }
                else if (timeValue.unit == TimeUnit.Millisecond)
                {
                    return timeValue.value / 1000f; // Convert milliseconds to seconds
                }
            }

            // Default duration in case no transition duration is found
            return 0.5f;
        }
    }
}
