#if JayC_Spine
using Spine.Unity;
#endif

using UnityEngine;
using UnityEngine.UIElements;

namespace JayC.UI.UIToolkit
{
    public static class UTKReferenceTool
    {
        /// <summary>
        /// Retrieves a UI element by name from the given UIDocument.
        /// </summary>
        public static T GetElementByName<T>(UIDocument uiDocument, string name) where T : VisualElement
        {
            if (uiDocument == null)
            {
                Debug.LogError("UIDocument is null.");
                return null;
            }

            VisualElement root = uiDocument.rootVisualElement;
            if (root == null)
            {
                Debug.LogError("Root element is null. Make sure the UIDocument is properly set.");
                return null;
            }

            T element = root.Q<T>(name);
            if (element == null)
            {
                Debug.LogError($"UI Element with the name '{name}' of type '{typeof(T)}' not found.");
            }
            return element;
        }

        /// <summary>
        /// Instantiates a VisualTreeAsset and adds it to the specified parent VisualElement.
        /// </summary>
        /// <param name="visualTreeAsset">The VisualTreeAsset to instantiate.</param>
        /// <param name="parent">The parent VisualElement to attach the instantiated element to.</param>
        /// <returns>The instantiated VisualElement.</returns>
        public static VisualElement InstantiateVisualTree(VisualTreeAsset visualTreeAsset, VisualElement parent)
        {
            if (visualTreeAsset == null)
            {
                Debug.LogError("VisualTreeAsset is null.");
                return null;
            }

            if (parent == null)
            {
                Debug.LogError("Parent VisualElement is null.");
                return null;
            }

            VisualElement instantiatedElement = visualTreeAsset.Instantiate();
            parent.Add(instantiatedElement);
            return instantiatedElement;
        }

        /// <summary>
        /// Clones a VisualTreeAsset, assigns values to its elements, and returns the instantiated element.
        /// </summary>
        /// <param name="visualTreeAsset">The VisualTreeAsset to clone.</param>
        /// <param name="parent">The parent VisualElement to attach the instantiated element to.</param>
        /// <param name="assignActions">Action to assign values to UI elements.</param>
        /// <returns>The instantiated VisualElement.</returns>
        public static VisualElement CloneAndAssign(VisualTreeAsset visualTreeAsset, VisualElement parent, System.Action<VisualElement> assignActions)
        {
            if (visualTreeAsset == null)
            {
                Debug.LogError("VisualTreeAsset is null.");
                return null;
            }

            if (parent == null)
            {
                Debug.LogError("Parent VisualElement is null.");
                return null;
            }

            // Clone the VisualTreeAsset
            VisualElement instantiatedElement = visualTreeAsset.CloneTree();
            parent.Add(instantiatedElement);

            // Assign values using the provided action
            assignActions?.Invoke(instantiatedElement);

            return instantiatedElement;
        }

        /// <summary>
        /// Retrieves a Button by name from the given UIDocument.
        /// </summary>
        public static Button GetButton(UIDocument uiDocument, string name)
        {
            return GetElementByName<Button>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a Label by name from the given UIDocument.
        /// </summary>
        public static Label GetLabel(UIDocument uiDocument, string name)
        {
            return GetElementByName<Label>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a TextField by name from the given UIDocument.
        /// </summary>
        public static TextField GetTextField(UIDocument uiDocument, string name)
        {
            return GetElementByName<TextField>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a Slider by name from the given UIDocument.
        /// </summary>
        public static Slider GetSlider(UIDocument uiDocument, string name)
        {
            return GetElementByName<Slider>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a Toggle by name from the given UIDocument.
        /// </summary>
        public static Toggle GetToggle(UIDocument uiDocument, string name)
        {
            return GetElementByName<Toggle>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a ToggleButtonGroup by name from the given UIDocument.
        /// </summary>
        public static ToggleButtonGroup GetToggleButtonGroup(UIDocument uiDocument, string name)
        {
            return GetElementByName<ToggleButtonGroup>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a DropdownField by name from the given UIDocument.
        /// </summary>
        public static DropdownField GetDropdownField(UIDocument uiDocument, string name)
        {
            return GetElementByName<DropdownField>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a ScrollView by name from the given UIDocument.
        /// </summary>
        public static ScrollView GetScrollView(UIDocument uiDocument, string name)
        {
            return GetElementByName<ScrollView>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a Foldout by name from the given UIDocument.
        /// </summary>
        public static Foldout GetFoldout(UIDocument uiDocument, string name)
        {
            return GetElementByName<Foldout>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a Box by name from the given UIDocument.
        /// </summary>
        public static Box GetBox(UIDocument uiDocument, string name)
        {
            return GetElementByName<Box>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a VisualElement by name from the given UIDocument.
        /// </summary>
        public static VisualElement GetVisualElement(UIDocument uiDocument, string name)
        {
            return GetElementByName<VisualElement>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a EnumField by name from the given UIDocument.
        /// </summary>
        public static EnumField GetEnumField(UIDocument uiDocument, string name)
        {
            return GetElementByName<EnumField>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a RadioButton by name from the given UIDocument.
        /// </summary>
        public static RadioButton GetRadioButton(UIDocument uiDocument, string name)
        {
            return GetElementByName<RadioButton>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a RadioButtonGroup by name from the given UIDocument.
        /// </summary>
        public static RadioButtonGroup GetRadioButtonGroup(UIDocument uiDocument, string name)
        {
            return GetElementByName<RadioButtonGroup>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a Tab by name from the given UIDocument.
        /// </summary>
        public static Tab GetTab(UIDocument uiDocument, string name)
        {
            return GetElementByName<Tab>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a TabView by name from the given UIDocument.
        /// </summary>
        public static TabView GetTabView(UIDocument uiDocument, string name)
        {
            return GetElementByName<TabView>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a ProgressBar by name from the given UIDocument.
        /// </summary>
        public static ProgressBar GetProgressBar(UIDocument uiDocument, string name)
        {
            return GetElementByName<ProgressBar>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a Scroller by name from the given UIDocument.
        /// </summary>
        public static Scroller GetScroller(UIDocument uiDocument, string name)
        {
            return GetElementByName<Scroller>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a MinMaxSlider by name from the given UIDocument.
        /// </summary>
        public static MinMaxSlider GetMinMaxSlider(UIDocument uiDocument, string name)
        {
            return GetElementByName<MinMaxSlider>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a SliderInt by name from the given UIDocument.
        /// </summary>
        public static SliderInt GetSliderInt(UIDocument uiDocument, string name)
        {
            return GetElementByName<SliderInt>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a ListView by name from the given UIDocument.
        /// </summary>
        public static ListView GetListView(UIDocument uiDocument, string name)
        {
            return GetElementByName<ListView>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a TreeView by name from the given UIDocument.
        /// </summary>
        public static TreeView GetTreeView(UIDocument uiDocument, string name)
        {
            return GetElementByName<TreeView>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a MultiColumnListView by name from the given UIDocument.
        /// </summary>
        public static MultiColumnListView GetMultiColumnListView(UIDocument uiDocument, string name)
        {
            return GetElementByName<MultiColumnListView>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a MultiColumnTreeView by name from the given UIDocument.
        /// </summary>
        public static MultiColumnTreeView GetMultiColumnTreeView(UIDocument uiDocument, string name)
        {
            return GetElementByName<MultiColumnTreeView>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a GroupBox by name from the given UIDocument.
        /// </summary>
        public static GroupBox GetGroupBox(UIDocument uiDocument, string name)
        {
            return GetElementByName<GroupBox>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a IntegerField by name from the given UIDocument.
        /// </summary>
        public static IntegerField GetIntegerField(UIDocument uiDocument, string name)
        {
            return GetElementByName<IntegerField>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a FloatField by name from the given UIDocument.
        /// </summary>
        public static FloatField GetFloatField(UIDocument uiDocument, string name)
        {
            return GetElementByName<FloatField>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a LongField by name from the given UIDocument.
        /// </summary>
        public static LongField GetLongField(UIDocument uiDocument, string name)
        {
            return GetElementByName<LongField>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a DoubleField by name from the given UIDocument.
        /// </summary>
        public static DoubleField GetDoubleField(UIDocument uiDocument, string name)
        {
            return GetElementByName<DoubleField>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a Hash128Field by name from the given UIDocument.
        /// </summary>
        public static Hash128Field GetHash128Field(UIDocument uiDocument, string name)
        {
            return GetElementByName<Hash128Field>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a Vector2Field by name from the given UIDocument.
        /// </summary>
        public static Vector2Field GetVector2Field(UIDocument uiDocument, string name)
        {
            return GetElementByName<Vector2Field>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a Vector3Field by name from the given UIDocument.
        /// </summary>
        public static Vector3Field GetVector3Field(UIDocument uiDocument, string name)
        {
            return GetElementByName<Vector3Field>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a Vector4Field by name from the given UIDocument.
        /// </summary>
        public static Vector4Field GetVector4Field(UIDocument uiDocument, string name)
        {
            return GetElementByName<Vector4Field>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a RectField by name from the given UIDocument.
        /// </summary>
        public static RectField GetRectField(UIDocument uiDocument, string name)
        {
            return GetElementByName<RectField>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a BoundsField by name from the given UIDocument.
        /// </summary>
        public static BoundsField GetBoundsField(UIDocument uiDocument, string name)
        {
            return GetElementByName<BoundsField>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a UnsignedIntegerField by name from the given UIDocument.
        /// </summary>
        public static UnsignedIntegerField GetUnsignedIntegerField(UIDocument uiDocument, string name)
        {
            return GetElementByName<UnsignedIntegerField>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a UnsignedLongField by name from the given UIDocument.
        /// </summary>
        public static UnsignedLongField GetUnsignedLongField(UIDocument uiDocument, string name)
        {
            return GetElementByName<UnsignedLongField>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a Vector2IntField by name from the given UIDocument.
        /// </summary>
        public static Vector2IntField GetVector2IntField(UIDocument uiDocument, string name)
        {
            return GetElementByName<Vector2IntField>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a Vector3IntField by name from the given UIDocument.
        /// </summary>
        public static Vector3IntField GetVector3IntField(UIDocument uiDocument, string name)
        {
            return GetElementByName<Vector3IntField>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a RectIntField by name from the given UIDocument.
        /// </summary>
        public static RectIntField GetRectIntField(UIDocument uiDocument, string name)
        {
            return GetElementByName<RectIntField>(uiDocument, name);
        }

        /// <summary>
        /// Retrieves a BoundsIntField by name from the given UIDocument.
        /// </summary>
        public static BoundsIntField GetBoundsIntField(UIDocument uiDocument, string name)
        {
            return GetElementByName<BoundsIntField>(uiDocument, name);
        }

#if JayC_Spine
        /// <summary>
        /// Retrieves a BoundsIntField by name from the given UIDocument.
        /// </summary>
        public static SpineVisualElement GetSpineVisualElement(UIDocument uiDocument, string name)
        {
            return GetElementByName<SpineVisualElement>(uiDocument, name);
        }
#endif


        /// <summary>
        /// Registers a callback to be triggered after the transition ends on a VisualElement.
        /// </summary>
        public static void RegisterTransitionCallback(VisualElement element, System.Action callback)
        {
            if (element == null || callback == null) return;

            element.RegisterCallback<TransitionEndEvent>(evt =>
            {
                if (evt.target == element)
                {
                    callback.Invoke();
                }
            });
        }
    }
}