using UnityEngine;

namespace JayC.UI
{
    public class JUISyncWithWorldspaceBase : MonoBehaviour
    {
        public Transform targetTransform; // Reference to the 3D object's transform.

        // This method can be used by derived classes to set the target transform.
        public virtual void SetTargetTransform(Transform newTargetTransform)
        {
            targetTransform = newTargetTransform;
        }

        // Update is called once per frame
        protected virtual void Update()
        {
            // Check if the targetTransform is not null (optional, for safety).
            if (targetTransform != null)
            {
                // Get the world position of the targetTransform.
                Vector3 worldPosition = targetTransform.position;

                // Convert the world position to screen space.
                Vector3 screenPosition = Camera.main.WorldToScreenPoint(worldPosition);

                // Set the UI object's position to match the screen position.
                transform.position = screenPosition;
            }
        }
    }
}
