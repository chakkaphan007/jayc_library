using System;
using System.Collections.Generic;
using UnityEngine.UIElements;

namespace JayC.UI.UIToolkit
{
    public static class UTKPopupManager
    {
        public static Action<VisualElement> OnPopupShow { get; set; }
        public static Action<VisualElement> OnPopupHide { get; set; }

        private static Dictionary<string, VisualElement> PopupDatabase = new Dictionary<string, VisualElement>();

        /// <summary>
        /// Registers a popup to the database with a unique key.
        /// </summary>
        public static void RegisterPopup(string popupId, VisualElement popup)
        {
            if (string.IsNullOrEmpty(popupId) || popup == null) return;

            if (!PopupDatabase.ContainsKey(popupId))
                PopupDatabase[popupId] = popup;
        }

        /// <summary>
        /// Shows a popup using UTKUtility to handle the animation.
        /// </summary>
        public static void ShowPopup(VisualElement popup, string customShowAnimationClass = null)
        {
            if (popup == null) return;

            // Show with animation via UTKUtility
            UTKUtility.ShowWithAnimation(popup, customShowAnimationClass, () =>
            {
                OnPopupShow?.Invoke(popup); // Callback after animation completes
            });
        }

        /// <summary>
        /// Hides a popup using UTKUtility to handle the animation.
        /// </summary>
        public static void HidePopup(VisualElement popup, string customHideAnimationClass = null)
        {
            if (popup == null) return;

            // Hide with animation via UTKUtility
            UTKUtility.HideWithAnimation(popup, customHideAnimationClass, () =>
            {
                OnPopupHide?.Invoke(popup); // Callback after animation completes
            });
        }

        /// <summary>
        /// Shows a popup from the database with customizable animation.
        /// </summary>
        public static void ShowPopupFromDatabase(string popupId, string customShowAnimationClass = null)
        {
            if (PopupDatabase.TryGetValue(popupId, out VisualElement popup))
            {
                ShowPopup(popup, customShowAnimationClass);
            }
        }

        /// <summary>
        /// Hides a popup from the database with customizable animation.
        /// </summary>
        public static void HidePopupFromDatabase(string popupId, string customHideAnimationClass = null)
        {
            if (PopupDatabase.TryGetValue(popupId, out VisualElement popup))
            {
                HidePopup(popup, customHideAnimationClass);
            }
        }
    }
}
