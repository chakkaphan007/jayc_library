///-----------------------------------------------------------------
///   Author : JayC
///   Date   : 30/10/2024
///-----------------------------------------------------------------
using UnityEngine;

namespace JayC.Utility
{
    public static class MathUtility
    {
        /// <summary>
        /// Divides two values and returns the fractional part of the result.
        /// </summary>
        /// <param name="numerator">The numerator of the division.</param>
        /// <param name="denominator">The denominator of the division.</param>
        /// <returns>The fractional part of the division result.</returns>
        public static float GetFractionalPart(float numerator, float denominator)
        {
            if (denominator == 0)
            {
                Debug.LogError("Denominator cannot be zero.");
                return 0;
            }

            float divisionResult = numerator / denominator;
            return divisionResult - Mathf.Floor(divisionResult);
        }

        /// <summary>
        /// Clamps a value between a minimum and maximum range.
        /// </summary>
        /// <param name="value">Value to be clamped.</param>
        /// <param name="min">Minimum range limit.</param>
        /// <param name="max">Maximum range limit.</param>
        /// <returns>Clamped value within the specified range.</returns>
        public static float Clamp(float value, float min, float max)
        {
            return Mathf.Clamp(value, min, max);
        }

        /// <summary>
        /// Rounds a float to the nearest integer.
        /// </summary>
        /// <param name="value">Value to round.</param>
        /// <returns>Nearest integer to the given value.</returns>
        public static int RoundToNearestInt(float value)
        {
            return Mathf.RoundToInt(value);
        }

        /// <summary>
        /// Calculates the Euclidean distance between two points in 2D space.
        /// </summary>
        /// <param name="pointA">First point.</param>
        /// <param name="pointB">Second point.</param>
        /// <returns>Distance between the two points.</returns>
        public static float Distance2D(Vector2 pointA, Vector2 pointB)
        {
            return Vector2.Distance(pointA, pointB);
        }

        /// <summary>
        /// Calculates the Euclidean distance between two points in 3D space.
        /// </summary>
        /// <param name="pointA">First point.</param>
        /// <param name="pointB">Second point.</param>
        /// <returns>Distance between the two points.</returns>
        public static float Distance3D(Vector3 pointA, Vector3 pointB)
        {
            return Vector3.Distance(pointA, pointB);
        }

        /// <summary>
        /// Linearly interpolates between two values by a given fraction.
        /// </summary>
        /// <param name="start">Starting value.</param>
        /// <param name="end">Ending value.</param>
        /// <param name="t">Interpolation fraction between 0 and 1.</param>
        /// <returns>Interpolated value.</returns>
        public static float Lerp(float start, float end, float t)
        {
            return Mathf.Lerp(start, end, t);
        }

        /// <summary>
        /// Checks if a number is approximately equal to another within a small epsilon value.
        /// </summary>
        /// <param name="a">First number.</param>
        /// <param name="b">Second number.</param>
        /// <param name="tolerance">Tolerance within which the numbers are considered approximately equal.</param>
        /// <returns>True if the values are approximately equal, false otherwise.</returns>
        public static bool ApproximatelyEqual(float a, float b, float tolerance = 0.0001f)
        {
            return Mathf.Abs(a - b) <= tolerance;
        }

        /// <summary>
        /// Maps a value from one range to another.
        /// </summary>
        /// <param name="value">Input value to be mapped.</param>
        /// <param name="inMin">Input range minimum.</param>
        /// <param name="inMax">Input range maximum.</param>
        /// <param name="outMin">Output range minimum.</param>
        /// <param name="outMax">Output range maximum.</param>
        /// <returns>Mapped value in the new range.</returns>
        public static float Map(float value, float inMin, float inMax, float outMin, float outMax)
        {
            return outMin + (outMax - outMin) * ((value - inMin) / (inMax - inMin));
        }

        /// <summary>
        /// Gets the sign of a float number.
        /// </summary>
        /// <param name="value">Input value.</param>
        /// <returns>1 if positive, -1 if negative, 0 if zero.</returns>
        public static int GetSign(float value)
        {
            return Mathf.Sign(value) > 0 ? 1 : value < 0 ? -1 : 0;
        }

        /// <summary>
        /// Clamps an angle to a specified range in degrees.
        /// </summary>
        /// <param name="angle">Angle to be clamped (in degrees).</param>
        /// <param name="min">Minimum angle range.</param>
        /// <param name="max">Maximum angle range.</param>
        /// <returns>Clamped angle.</returns>
        public static float ClampAngle(float angle, float min, float max)
        {
            angle = Mathf.Repeat(angle, 360);
            if (angle > 180) angle -= 360;
            return Mathf.Clamp(angle, min, max);
        }

        /// <summary>
        /// Normalizes a value to a 0-1 range based on specified min and max values.
        /// </summary>
        /// <param name="value">Value to normalize.</param>
        /// <param name="min">Minimum value of the range.</param>
        /// <param name="max">Maximum value of the range.</param>
        /// <returns>Normalized value in the range 0-1.</returns>
        public static float Normalize(float value, float min, float max)
        {
            return (value - min) / (max - min);
        }

        /// <summary>
        /// Wraps a value around a range, useful for looping values (e.g., angles).
        /// </summary>
        /// <param name="value">Value to wrap.</param>
        /// <param name="min">Minimum of the range.</param>
        /// <param name="max">Maximum of the range.</param>
        /// <returns>Wrapped value within the range.</returns>
        public static float Wrap(float value, float min, float max)
        {
            return min + Mathf.Repeat(value - min, max - min);
        }

        /// <summary>
        /// Checks if a float value is a whole number (integer).
        /// </summary>
        /// <param name="value">Value to check.</param>
        /// <returns>True if value is a whole number; false otherwise.</returns>
        public static bool IsWholeNumber(float value)
        {
            return Mathf.Approximately(value % 1, 0);
        }
    }
}