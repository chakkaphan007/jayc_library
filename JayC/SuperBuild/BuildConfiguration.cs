using UnityEngine;

namespace JayC.Config
{
    public class BuildConfiguration : MonoBehaviour
    {
        public static string BuildType = "Unknown"; // This will be set dynamically during the build

        public static void SetBuildType(string buildType)
        {
            BuildType = buildType;
            Debug.Log($"Build type set to: {BuildType}");
        }
    }
}
