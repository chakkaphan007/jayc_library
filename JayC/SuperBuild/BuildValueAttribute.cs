using System;

namespace JayC.Build
{
    [AttributeUsage(AttributeTargets.Field)]
    public class BuildValueAttribute : Attribute
    {
        public string description;

        public BuildValueAttribute(string description = "")
        {
            this.description = description;
        }
    }
}
