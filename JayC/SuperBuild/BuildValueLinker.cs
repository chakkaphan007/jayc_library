using UnityEngine;
using System.Reflection;
using System.Linq;

namespace JayC.Build
{
    [ExecuteInEditMode]
    public class BuildValueLinker : MonoBehaviour
    {
        public BuildPreset buildPreset;

        private void OnValidate()
        {
            if (buildPreset != null)
            {
                SyncBuildValuesWithPreset();
            }
        }

        // Sync the fields marked with [BuildValue] with the ScriptableObject preset
        private void SyncBuildValuesWithPreset()
        {
            // Get all fields marked with [BuildValue] in the current MonoBehaviour
            var fields = GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                .Where(field => field.GetCustomAttribute<BuildValueAttribute>() != null);

            foreach (var field in fields)
            {
                // Check if the field is already in the preset
                var existingValue = buildPreset.dynamicFields.FirstOrDefault(b => b.fieldName == field.Name);

                if (existingValue == null)
                {
                    // Add the field to the preset if it's not already present
                    BuildPreset.DynamicBuildField newField = new BuildPreset.DynamicBuildField
                    {
                        fieldName = field.Name,
                        demoValue = (int)field.GetValue(this),  // Set a default value
                        releaseValue = (int)field.GetValue(this) // Set a default value
                    };

                    buildPreset.dynamicFields.Add(newField);
                    Debug.Log($"Added field '{field.Name}' to BuildPreset.");
                }
            }
        }

        public void ApplyPresetValues(bool isDemoBuild)
        {
            // Apply values from the BuildPreset to the fields marked with [BuildValue]
            foreach (var fieldValue in buildPreset.dynamicFields)
            {
                var field = GetType().GetField(fieldValue.fieldName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                if (field != null)
                {
                    int valueToSet = isDemoBuild ? fieldValue.demoValue : fieldValue.releaseValue;
                    field.SetValue(this, valueToSet);
                    Debug.Log($"Set {fieldValue.fieldName} to {valueToSet} for {(isDemoBuild ? "Demo" : "Release")} build.");
                }
            }
        }
    }
}
