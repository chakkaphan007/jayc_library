using System.Collections.Generic;
using UnityEngine;

namespace JayC.Build
{
    [CreateAssetMenu(fileName = "BuildPreset", menuName = "Build/BuildPreset", order = 1)]
    public class BuildPreset : ScriptableObject
    {
        [System.Serializable]
        public class DynamicBuildField
        {
            public string fieldName;  // Field name in the MonoBehaviour
            public int demoValue;     // Value for demo build
            public int releaseValue;  // Value for release build
        }

        public List<DynamicBuildField> dynamicFields = new List<DynamicBuildField>();
    }
}
