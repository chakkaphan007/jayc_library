#if UNITY_EDITOR
#if JayC_Addressable
using JayC.Utility.Addressable;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace JayC.SceneManagement
{
    [CustomEditor(typeof(JSceneManager))]
    public class JSceneManagerEditor : Editor
    {
        private SerializedProperty fadeDuration;
        private SerializedProperty uiDocument;
        private SerializedProperty sceneKey;

        private JSceneManager sceneManager;
        private List<SceneEntry> sceneEntries;

        private Vector2 scrollPosition;

        private void OnEnable()
        {
            InitializeProperties();
            InitializeSceneList();
            SubscribeToEvents();
        }

        private void OnDisable()
        {
            UnsubscribeFromEvents();
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            DisplayGeneralSettings();
            DisplaySceneActions();
            DisplayActiveSceneList();

            serializedObject.ApplyModifiedProperties();

            if (EditorApplication.isPlaying)
            {
                Repaint();
            }
        }

        #region Initialization

        private void InitializeProperties()
        {
            fadeDuration = serializedObject.FindProperty("fadeDuration");
            uiDocument = serializedObject.FindProperty("uiDocument");
            sceneKey = serializedObject.FindProperty("sceneKey");
            sceneManager = (JSceneManager)target;
        }

        private void InitializeSceneList()
        {
            sceneEntries = new List<SceneEntry>();
            PopulateSceneList();
        }

        private void SubscribeToEvents()
        {
            EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
            EditorSceneManager.sceneOpened += OnSceneOpened;
            EditorSceneManager.sceneClosed += OnSceneClosed;
            EditorApplication.update += OnEditorUpdate;
        }

        private void UnsubscribeFromEvents()
        {
            EditorApplication.playModeStateChanged -= OnPlayModeStateChanged;
            EditorSceneManager.sceneOpened -= OnSceneOpened;
            EditorSceneManager.sceneClosed -= OnSceneClosed;
            EditorApplication.update -= OnEditorUpdate;
        }

        #endregion Initialization

        #region Event Handlers

        private void OnPlayModeStateChanged(PlayModeStateChange state)
        {
            PopulateSceneList();
            Repaint();
        }

        private void OnSceneOpened(Scene scene, OpenSceneMode mode)
        {
            PopulateSceneList();
            Repaint();
        }

        private void OnSceneClosed(Scene scene)
        {
            PopulateSceneList();
            Repaint();
        }

        private void OnEditorUpdate()
        {
            if (EditorApplication.isPlaying)
            {
                PopulateSceneList();
                Repaint();
            }
        }

        #endregion Event Handlers

        #region Scene List Management

        private void PopulateSceneList()
        {
            sceneEntries.Clear();

            if (EditorApplication.isPlaying)
            {
                // We will create a temporary list to iterate over instead of modifying the original AddressableSceneManager.loadedScenes
                foreach (var sceneKey in new List<string>(AddressableSceneManager.loadedScenes.Keys))
                {
                    sceneEntries.Add(new SceneEntry
                    {
                        SceneName = sceneKey,
                        IsActive = SceneManager.GetActiveScene().name == sceneKey,
                        LoadMode = LoadSceneMode.Additive
                    });
                }
            }
        }

        #endregion Scene List Management

        #region GUI Display Methods

        private void DisplayGeneralSettings()
        {
            EditorGUILayout.BeginVertical("box");
            EditorGUILayout.LabelField("General Settings", EditorStyles.boldLabel);
            EditorGUILayout.PropertyField(fadeDuration, new GUIContent("Fade Duration"));
            EditorGUILayout.PropertyField(sceneKey, new GUIContent("Scene Key"));
            EditorGUILayout.PropertyField(uiDocument, new GUIContent("UI Document"));
            EditorGUILayout.EndVertical();
            EditorGUILayout.Space();
        }

        private void DisplaySceneActions()
        {
            EditorGUILayout.BeginVertical("box");
            EditorGUILayout.LabelField("Scene Actions", EditorStyles.boldLabel);

            DisplayLoadButtons();
            DisplayReloadButtons();

            EditorGUILayout.EndVertical();
            EditorGUILayout.Space();
        }

        private void DisplayLoadButtons()
        {
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Load Additive Scene (With Fade)"))
            {
                sceneManager.LoadScene(sceneKey.stringValue, fade: true, singleMode: false);
                PopulateSceneList();
            }
            if (GUILayout.Button("Load Single Scene (With Fade)"))
            {
                sceneManager.LoadScene(sceneKey.stringValue, fade: true, singleMode: true);
                PopulateSceneList();
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Load Additive Scene (No Fade)"))
            {
                sceneManager.LoadScene(sceneKey.stringValue, fade: false, singleMode: false);
                PopulateSceneList();
            }
            if (GUILayout.Button("Load Single Scene (No Fade)"))
            {
                sceneManager.LoadScene(sceneKey.stringValue, fade: false, singleMode: true);
                PopulateSceneList();
            }
            EditorGUILayout.EndHorizontal();
        }

        private void DisplayReloadButtons()
        {
            if (GUILayout.Button("Reload Active Scene (With Fade)"))
            {
                sceneManager.ReloadActiveScene(fade: true);
                PopulateSceneList();
            }
            if (GUILayout.Button("Reload Active Scene (No Fade)"))
            {
                sceneManager.ReloadActiveScene(fade: false);
                PopulateSceneList();
            }
        }

        private void DisplayActiveSceneList()
        {
            EditorGUILayout.BeginVertical("box");
            EditorGUILayout.LabelField("Active Scenes", EditorStyles.boldLabel);

            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));

            EditorGUILayout.BeginVertical();

            // Table header
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            GUILayout.Label("Scene Name", EditorStyles.toolbarButton, GUILayout.ExpandWidth(true));
            GUILayout.Label("Status", EditorStyles.toolbarButton, GUILayout.Width(60));
            GUILayout.Label("Load Mode", EditorStyles.toolbarButton, GUILayout.Width(80));
            GUILayout.Label("Actions", EditorStyles.toolbarButton, GUILayout.Width(180));
            EditorGUILayout.EndHorizontal();

            // Table content
            foreach (var entry in sceneEntries)
            {
                EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);

                EditorGUILayout.LabelField(entry.SceneName, GUILayout.ExpandWidth(true));
                EditorGUILayout.LabelField(entry.IsActive ? "Active" : "Inactive", GUILayout.Width(60));
                EditorGUILayout.LabelField(entry.LoadMode.ToString(), GUILayout.Width(80));

                EditorGUILayout.BeginHorizontal(GUILayout.Width(180));

                GUI.enabled = !entry.IsActive && entry.LoadMode != LoadSceneMode.Single;
                if (GUILayout.Button("Unload No Fade", GUILayout.Width(90)))
                {
                    sceneManager.UnloadScene(entry.SceneName, fade: false);
                    PopulateSceneList();
                }

                GUI.enabled = true;
                if (GUILayout.Button("Reload", GUILayout.Width(60)))
                {
                    sceneManager.ReloadScene(entry.SceneName, fade: true);
                    PopulateSceneList();
                }

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.EndVertical();
            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();
        }

        #endregion GUI Display Methods

        private class SceneEntry
        {
            public string SceneName { get; set; }
            public bool IsActive { get; set; }
            public LoadSceneMode LoadMode { get; set; }
        }
    }
}
#endif
#endif