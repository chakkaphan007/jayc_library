#if UNITY_EDITOR

using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;
using UnityEditorInternal;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace JayC.Utility
{
    public class StartUpEditorWindow : EditorWindow
    {
        // Define Symbols Management
        private static readonly string[] DefineSymbols = { "JayC_Playfab", "JayC_FilePicker", "JayC_Tween", "JayC_Spine", "JayC_Addressable" };

        private bool[] symbolChecks;

        // Package URLs
        private static readonly Dictionary<string, string> PackageUrls = new Dictionary<string, string>
        {
            { "JayC_FilePicker", "https://github.com/yasirkula/UnityNativeFilePicker.git" },
            { "JayC_Tween", "https://github.com/jeffreylanters/unity-tweens.git" },
            { "JayC_Spine", "https://gitlab.com/chakkaphan007/spine_unity_fork.git" }
        };

        // Scoped Registry Management
        private string registryName = "package.openupm.com";

        private string registryUrl = "https://package.openupm.com";

        private List<string> scopes = new List<string>
        {
            "com.studio23", "com.needle", "com.ltmx", "ga.fuquna", "com.github.siccity",
            "com.textus-games", "com.solidalloy", "com.uurha", "com.tdw", "com.tayx",
            "com.cysharp", "com.github.superunitybuild"
        };

        private ReorderableList reorderableScopesList;
        private const string DontShowAgainPrefKey = "DefineSymbolsAndRegistryManager_DontShowAgain";
        private bool isInstallingPackage = false;

        [MenuItem("JayC/StartUp")]
        public static void ShowWindow()
        {
            GetWindow<StartUpEditorWindow>("JayC StartUp Window");
        }

        private void OnEnable()
        {
            InitializeDefineSymbols();
            InitializeScopedRegistry();
        }

        private void InitializeDefineSymbols()
        {
            symbolChecks = new bool[DefineSymbols.Length];
            BuildTargetGroup buildTargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
            string currentDefines = PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTargetGroup);

            for (int i = 0; i < DefineSymbols.Length; i++)
            {
                symbolChecks[i] = currentDefines.Contains(DefineSymbols[i]);
            }
        }

        private void InitializeScopedRegistry()
        {
            reorderableScopesList = new ReorderableList(scopes, typeof(string), true, true, true, true)
            {
                drawHeaderCallback = (Rect rect) => EditorGUI.LabelField(rect, "Scopes"),
                drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
                {
                    scopes[index] = EditorGUI.TextField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), scopes[index]);
                },
                onAddCallback = (ReorderableList list) => scopes.Add(string.Empty),
                onRemoveCallback = (ReorderableList list) => scopes.RemoveAt(list.index)
            };
        }

        private void OnGUI()
        {
            EditorGUILayout.BeginVertical("box");
            GUILayout.Label("Define Symbols Manager", EditorStyles.boldLabel);
            EditorGUILayout.Space();

            for (int i = 0; i < DefineSymbols.Length; i++)
            {
                symbolChecks[i] = EditorGUILayout.Toggle(DefineSymbols[i], symbolChecks[i]);
            }

            EditorGUILayout.Space();

            GUI.enabled = !isInstallingPackage;
            if (GUILayout.Button("Update Define Symbols"))
            {
                UpdateDefineSymbols();
            }
            GUI.enabled = true;

            EditorGUILayout.EndVertical();

            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical("box");
            GUILayout.Label("Add Scoped Registry", EditorStyles.boldLabel);

            registryName = EditorGUILayout.TextField("Registry Name", registryName);
            registryUrl = EditorGUILayout.TextField("Registry URL", registryUrl);

            reorderableScopesList.DoLayoutList();

            if (GUILayout.Button("Add Registry"))
            {
                AddScopedRegistry();
            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            bool dontShowAgain = EditorPrefs.GetBool(DontShowAgainPrefKey, false);
            dontShowAgain = EditorGUILayout.ToggleLeft("Don't show this window again", dontShowAgain);
            EditorPrefs.SetBool(DontShowAgainPrefKey, dontShowAgain);
            EditorGUILayout.EndHorizontal();
        }

        private async void UpdateDefineSymbols()
        {
            isInstallingPackage = true;
            BuildTargetGroup buildTargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
            string currentDefines = PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTargetGroup);
            var definesList = new List<string>(currentDefines.Split(';'));

            for (int i = 0; i < DefineSymbols.Length; i++)
            {
                if (symbolChecks[i])
                {
                    if (!definesList.Contains(DefineSymbols[i]))
                    {
                        definesList.Add(DefineSymbols[i]);
                    }
                    if (PackageUrls.TryGetValue(DefineSymbols[i], out string packageUrl))
                    {
                        await InstallPackageAsync(packageUrl);
                    }
                }
                else
                {
                    definesList.Remove(DefineSymbols[i]);
                }
            }

            PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, string.Join(";", definesList.ToArray()));
            EditorUtility.DisplayDialog("Success", "Define symbols updated.", "OK");
            AssetDatabase.Refresh();
            isInstallingPackage = false;
        }

        private async Task InstallPackageAsync(string packageUrl)
        {
            var tcs = new TaskCompletionSource<bool>();
            var request = Client.Add(packageUrl);

            EditorApplication.update += CheckProgress;

            void CheckProgress()
            {
                if (request.IsCompleted)
                {
                    if (request.Status == StatusCode.Success)
                        Debug.Log($"Package installed successfully: {packageUrl}");
                    else
                        Debug.LogError($"Failed to install package: {packageUrl}. Error: {request.Error.message}");

                    EditorApplication.update -= CheckProgress;
                    tcs.SetResult(true);
                }
            }

            await tcs.Task;
        }

        private void AddScopedRegistry()
        {
            string manifestFilePath = Path.Combine(Application.dataPath, "../Packages/manifest.json");
            if (!File.Exists(manifestFilePath))
            {
                Debug.LogError("Manifest file not found: " + manifestFilePath);
                return;
            }

            var manifestJson = File.ReadAllText(manifestFilePath);

            if (IsRegistryAlreadyPresent(manifestJson, registryUrl, scopes.ToArray()))
            {
                Debug.Log("Scoped registry already present.");
                return;
            }

            var updatedManifestJson = AddRegistryToManifest(manifestJson, registryName, registryUrl, scopes.ToArray());
            File.WriteAllText(manifestFilePath, updatedManifestJson);
            Debug.Log("Scoped registry added.");

            AssetDatabase.Refresh();
        }

        private bool IsRegistryAlreadyPresent(string manifestJson, string registryUrl, string[] scopes)
        {
            return manifestJson.Contains(registryUrl) || scopes.Any(scope => manifestJson.Contains(scope));
        }

        private string AddRegistryToManifest(string manifestJson, string registryName, string registryUrl, string[] scopes)
        {
            var registryEntry = $@"
    {{
        ""name"": ""{registryName}"",
        ""url"": ""{registryUrl}"",
        ""scopes"": [
            {string.Join(", ", scopes.Select(scope => $"\"{scope}\""))}
        ]
    }}";

            var scopedRegistriesPattern = new Regex(@"""scopedRegistries""\s*:\s*\[(.*?)\]", RegexOptions.Singleline);
            var match = scopedRegistriesPattern.Match(manifestJson);

            if (match.Success)
            {
                var updatedRegistries = match.Groups[1].Value.Trim() == string.Empty
                    ? registryEntry
                    : match.Groups[1].Value + "," + registryEntry;
                return scopedRegistriesPattern.Replace(manifestJson, $"\"scopedRegistries\": [{updatedRegistries}]");
            }
            else
            {
                var lastBraceIndex = manifestJson.LastIndexOf('}');
                if (lastBraceIndex != -1)
                {
                    var registriesSection = $@",
  ""scopedRegistries"": [
    {registryEntry}
  ]
}}";
                    return manifestJson.Substring(0, lastBraceIndex) + registriesSection;
                }
            }

            return manifestJson;
        }
    }
}

#endif