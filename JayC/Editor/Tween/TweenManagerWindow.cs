#if JayC_Tween
#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class TweenManagerWindow : EditorWindow
{
    private Vector2 scrollPosition;
    private List<TweenController> tweenControllers = new List<TweenController>();

    [MenuItem("Window/Tween Manager")]
    public static void ShowWindow()
    {
        GetWindow<TweenManagerWindow>("Tween Manager");
    }

    private void OnEnable()
    {
        RefreshTweenControllers();
        EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
    }

    private void OnDisable()
    {
        EditorApplication.playModeStateChanged -= OnPlayModeStateChanged;
    }

    private void OnPlayModeStateChanged(PlayModeStateChange state)
    {
        if (state == PlayModeStateChange.EnteredEditMode)
        {
            RefreshTweenControllers();
        }
    }

    private void OnGUI()
    {
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Global Tween Controls", EditorStyles.boldLabel);

        if (GUILayout.Button("Refresh Tween Controllers"))
        {
            RefreshTweenControllers();
        }

        EditorGUILayout.Space();

        if (GUILayout.Button("Play All Tweens"))
        {
            ExecuteForAllControllers((controller) =>
            {
                controller.PlayPositionTween();
                controller.PlayScaleTween();
                controller.PlayRotationTween();
            });
        }

        if (GUILayout.Button("Pause/Resume All Tweens"))
        {
            ExecuteForAllControllers((controller) =>
            {
                controller.PauseTween("position");
                controller.PauseTween("scale");
                controller.PauseTween("rotation");
            });
        }

        if (GUILayout.Button("Cancel All Tweens"))
        {
            ExecuteForAllControllers((controller) =>
            {
                controller.CancelTween("position");
                controller.CancelTween("scale");
                controller.CancelTween("rotation");
            });
        }

        if (GUILayout.Button("Reset All To Origin"))
        {
            ExecuteForAllControllers((controller) =>
            {
                controller.ResetToOrigin();
            });
        }

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Individual Tween Controllers", EditorStyles.boldLabel);

        scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);

        for (int i = 0; i < tweenControllers.Count; i++)
        {
            var controller = tweenControllers[i];
            if (controller != null)
            {
                EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                EditorGUILayout.LabelField(controller.gameObject.name, EditorStyles.boldLabel);

                if (GUILayout.Button("Select in Hierarchy"))
                {
                    Selection.activeGameObject = controller.gameObject;
                }

                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("Play All"))
                {
                    controller.PlayPositionTween();
                    controller.PlayScaleTween();
                    controller.PlayRotationTween();
                }
                if (GUILayout.Button("Pause/Resume All"))
                {
                    controller.PauseTween("position");
                    controller.PauseTween("scale");
                    controller.PauseTween("rotation");
                }
                if (GUILayout.Button("Cancel All"))
                {
                    controller.CancelTween("position");
                    controller.CancelTween("scale");
                    controller.CancelTween("rotation");
                }
                EditorGUILayout.EndHorizontal();

                if (GUILayout.Button("Reset to Origin"))
                {
                    controller.ResetToOrigin();
                }

                EditorGUILayout.EndVertical();
                EditorGUILayout.Space();
            }
        }

        EditorGUILayout.EndScrollView();
    }

    private void RefreshTweenControllers()
    {
        tweenControllers.Clear();
        tweenControllers.AddRange(FindObjectsOfType<TweenController>());
    }

    private void ExecuteForAllControllers(System.Action<TweenController> action)
    {
        for (int i = tweenControllers.Count - 1; i >= 0; i--)
        {
            if (tweenControllers[i] != null)
            {
                action(tweenControllers[i]);
            }
            else
            {
                tweenControllers.RemoveAt(i);
            }
        }
    }
}
#endif
#endif