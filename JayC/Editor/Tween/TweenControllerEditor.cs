#if JayC_Tween
#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TweenController))]
public class TweenControllerEditor : Editor
{
    private TweenController tweenController;
    private string[] tabs = { "Position", "Scale", "Rotation" };
    private int selectedTab = 0;

    private void OnEnable()
    {
        tweenController = (TweenController)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.Space();
        selectedTab = GUILayout.Toolbar(selectedTab, tabs);

        EditorGUILayout.Space();

        switch (selectedTab)
        {
            case 0:
                DrawTweenSettings("positionSettings", "Position");
                break;

            case 1:
                DrawTweenSettings("scaleSettings", "Scale");
                break;

            case 2:
                DrawTweenSettings("rotationSettings", "Rotation");
                break;
        }

        EditorGUILayout.Space();

        if (GUILayout.Button("Reset to Origin"))
        {
            tweenController.ResetToOrigin();
        }

        serializedObject.ApplyModifiedProperties();
    }

    private void DrawTweenSettings(string settingsPropertyName, string tweenType)
    {
        var settingsProperty = serializedObject.FindProperty(settingsPropertyName);

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel(tweenType + " to");
        var targetValueProp = settingsProperty.FindPropertyRelative("targetValue");
        targetValueProp.vector3Value = EditorGUILayout.Vector3Field("", targetValueProp.vector3Value);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.PropertyField(settingsProperty.FindPropertyRelative("duration"), new GUIContent("Duration"));

        var useAnimationCurveProp = settingsProperty.FindPropertyRelative("useAnimationCurve");
        useAnimationCurveProp.boolValue = EditorGUILayout.Toggle("Use Animation Curve", useAnimationCurveProp.boolValue);

        if (useAnimationCurveProp.boolValue)
        {
            EditorGUILayout.PropertyField(settingsProperty.FindPropertyRelative("animationCurve"), new GUIContent("Animation Curve"));
        }
        else
        {
            EditorGUILayout.PropertyField(settingsProperty.FindPropertyRelative("easeType"), new GUIContent("Ease Type"));
        }

        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Play Tween"))
        {
            switch (tweenType)
            {
                case "Position":
                    tweenController.PlayPositionTween();
                    break;

                case "Scale":
                    tweenController.PlayScaleTween();
                    break;

                case "Rotation":
                    tweenController.PlayRotationTween();
                    break;
            }
        }
        if (GUILayout.Button("Pause/Resume"))
        {
            tweenController.PauseTween(tweenType.ToLower());
        }
        if (GUILayout.Button("Cancel Tween"))
        {
            tweenController.CancelTween(tweenType.ToLower());
        }
        EditorGUILayout.EndHorizontal();
    }
}
#endif
#endif