#if UNITY_EDITOR
#if JayC_Spine
using UnityEditor;
using UnityEngine;
using Spine.Unity;
using System.Collections.Generic;

namespace JayC.Utility.Editor
{
    [CustomEditor(typeof(SpineAnimationUtility))]
    public class SpineAnimationUtilityEditor : UnityEditor.Editor
    {
        private SpineAnimationUtility _spineAnimationUtility;
        private List<string> _animationNames = new List<string>();
        private int _selectedAnimationIndex;
        private float _animationSpeed = 1f;
        private bool _isLooping;

        private bool _isPlayOnstart;

        private void OnEnable()
        {
            _spineAnimationUtility = (SpineAnimationUtility)target;
            UpdateAnimationNames();
        }

        private void UpdateAnimationNames()
        {
            SkeletonAnimation skeletonAnimation = _spineAnimationUtility.GetComponent<SkeletonAnimation>();
            if (skeletonAnimation != null && skeletonAnimation.Skeleton != null)
            {
                // Clear previous animation names and fetch new ones
                _animationNames.Clear();

                foreach (var animation in skeletonAnimation.Skeleton.Data.Animations)
                {
                    _animationNames.Add(animation.Name);
                }
            }
            else
            {
                _animationNames.Clear();
            }
        }

        public override void OnInspectorGUI()
        {
            // Draw the default inspector
            DrawDefaultInspector();

            EditorGUILayout.Space();

            if (_animationNames != null && _animationNames.Count > 0)
            {
                // Dropdown for selecting animations
                _selectedAnimationIndex = EditorGUILayout.Popup("Select Animation", _selectedAnimationIndex, _animationNames.ToArray());

                // Speed control only in play mode
                if (Application.isPlaying)
                {
                    _animationSpeed = EditorGUILayout.Slider("Animation Speed", _animationSpeed, 0.1f, 3f);
                    _spineAnimationUtility.SetAnimationSpeed(_animationSpeed);
                }
                else
                {
                    EditorGUILayout.LabelField("Animation Speed (Play Mode Only)");
                }

                // Looping toggle
                _isLooping = EditorGUILayout.Toggle("Looping", _isLooping);
                _isPlayOnstart = EditorGUILayout.Toggle("Play On Start", _isPlayOnstart);

                EditorGUILayout.Space();

                // Play, Pause, Resume, and Stop buttons
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Play"))
                {
                    if (Application.isPlaying)
                    {
                        _spineAnimationUtility.PlayAnimation(_animationNames[_selectedAnimationIndex], _isLooping,
    onStart: () => Debug.Log("Animation Started"),
    onUpdate: () => Debug.Log("Animation Updated"),
    onEnd: () => Debug.Log("Animation Ended"));
                    }
                    else
                    {
                        EditorUtility.DisplayDialog("Cannot Pause", "Pause is only available in Play mode.", "OK");
                    }
                }

                if (GUILayout.Button("Stop"))
                {
                    if (Application.isPlaying)
                    {
                        _spineAnimationUtility.StopAnimation();
                    }
                    else
                    {
                        EditorUtility.DisplayDialog("Cannot Pause", "Pause is only available in Play mode.", "OK");
                    }
                }

                GUILayout.EndHorizontal();
            }
            else
            {
                EditorGUILayout.HelpBox("No animations available. Ensure a SkeletonDataAsset is assigned.", MessageType.Warning);
            }

            if (GUI.changed)
            {
                EditorUtility.SetDirty(_spineAnimationUtility);
            }
        }
    }
}
#endif
#endif