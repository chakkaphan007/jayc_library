#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace JayC.Utility.Animation
{
    [CustomEditor(typeof(SpriteAnimationUtility))]
    public class SpriteAnimationUtilityEditor : UnityEditor.Editor
    {
        private SpriteAnimationUtility _spriteAnimationUtility;
        private bool _isPreviewing;
        private float _previewTime;
        private int _currentPreviewFrame;

        private void OnEnable()
        {
            _spriteAnimationUtility = (SpriteAnimationUtility)target;
        }

        public override void OnInspectorGUI()
        {
            // Draw the default inspector
            DrawDefaultInspector();

            // Add a separator
            EditorGUILayout.Space();

            // Create a button to assign sprites via a folder picker
            if (GUILayout.Button("Assign Sprites from Folder"))
            {
                AssignSpritesFromFolder();
            }

            EditorGUILayout.Space();

            // Add buttons for Play, Pause, Resume, and Stop
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Play"))
            {
                _spriteAnimationUtility.PlayAnimation(isLooping: false);
                StartPreview();
            }

            if (GUILayout.Button("Pause"))
            {
                _spriteAnimationUtility.PauseAnimation();
                _isPreviewing = false; // Stop preview updates
            }

            if (GUILayout.Button("Resume"))
            {
                _spriteAnimationUtility.ResumeAnimation();
                StartPreview();
            }

            if (GUILayout.Button("Stop"))
            {
                _spriteAnimationUtility.StopAnimation();
                StopPreview();
            }
            GUILayout.EndHorizontal();

            EditorGUILayout.Space();

            // Preview current frame number
            EditorGUILayout.LabelField("Current Frame: " + _spriteAnimationUtility.GetCurrentFrame());

            // Add a button for previewing the animation in the editor
            if (_isPreviewing)
            {
                if (GUILayout.Button("Stop Preview"))
                {
                    StopPreview();
                }
            }
            else
            {
                if (GUILayout.Button("Preview Animation"))
                {
                    StartPreview();
                }
            }

            // Update the editor preview if in preview mode
            if (_isPreviewing)
            {
                UpdatePreview();
            }

            // Display preview information
            if (_isPreviewing)
            {
                EditorGUILayout.LabelField("Preview Frame: " + _currentPreviewFrame);
            }

            // Make sure changes are applied
            if (GUI.changed)
            {
                EditorUtility.SetDirty(_spriteAnimationUtility);
            }
        }

        // Assign sprites from a folder
        private void AssignSpritesFromFolder()
        {
            string folderPath = EditorUtility.OpenFolderPanel("Select Sprite Folder", "Assets", "");

            if (!string.IsNullOrEmpty(folderPath))
            {
                folderPath = "Assets" + folderPath.Replace(Application.dataPath, "").Replace("\\", "/");
                var spriteGuids = AssetDatabase.FindAssets("t:Sprite", new[] { folderPath });
                Sprite[] sprites = new Sprite[spriteGuids.Length];

                for (int i = 0; i < spriteGuids.Length; i++)
                {
                    string assetPath = AssetDatabase.GUIDToAssetPath(spriteGuids[i]);
                    sprites[i] = AssetDatabase.LoadAssetAtPath<Sprite>(assetPath);
                }

                // Assign the sorted sprites to the frames array
                _spriteAnimationUtility.frames = sprites;
                EditorUtility.SetDirty(_spriteAnimationUtility); // Mark dirty to save changes
            }
        }

        // Start previewing the animation
        private void StartPreview()
        {
            _isPreviewing = true;
            _previewTime = 0f;
            _currentPreviewFrame = 0;
        }

        // Stop previewing the animation
        private void StopPreview()
        {
            _isPreviewing = false;
            _spriteAnimationUtility.GetComponent<SpriteRenderer>().sprite = null; // Reset the sprite
        }

        // Update the preview animation in the editor
        private void UpdatePreview()
        {
            if (_spriteAnimationUtility.frames == null || _spriteAnimationUtility.frames.Length == 0)
            {
                return;
            }

            float frameTime = 1f / _spriteAnimationUtility.framesPerSecond;

            // Update the preview time
            _previewTime += Time.deltaTime;

            if (_previewTime >= frameTime)
            {
                _previewTime -= frameTime;
                _currentPreviewFrame = (_currentPreviewFrame + 1) % _spriteAnimationUtility.frames.Length;

                // Set the current preview sprite in the SpriteRenderer
                _spriteAnimationUtility.GetComponent<SpriteRenderer>().sprite = _spriteAnimationUtility.frames[_currentPreviewFrame];

                // Repaint the SceneView to reflect the changes in the editor
                SceneView.RepaintAll();
            }
        }
    }
}

#endif