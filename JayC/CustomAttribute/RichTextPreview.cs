using UnityEditor;
using UnityEngine;

public class RichTextPreviewAttribute : PropertyAttribute
{ }

#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(RichTextPreviewAttribute))]
public class RichTextPreviewDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        // Calculate rect for the serialized field
        Rect serializedRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);

        // Draw the serialized field
        EditorGUI.PropertyField(serializedRect, property, label);

        // Calculate rect for the rich text preview
        Rect richTextRect = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight, position.width, position.height - EditorGUIUtility.singleLineHeight);

        // Draw box around the rich text preview
        EditorGUI.DrawRect(richTextRect, new Color(10f, 10f, 10f)); // Light gray background

        // Get the text from the property
        string text = property.stringValue;

        // Parse the rich text tags and display the preview
        GUIContent richTextContent = new GUIContent(text);
        GUIStyle richTextStyle = new GUIStyle(EditorStyles.label);
        richTextStyle.richText = true;
        richTextStyle.wordWrap = true; // Enable word wrapping

        float textHeight = richTextStyle.CalcHeight(richTextContent, EditorGUIUtility.currentViewWidth);
        richTextRect.height = textHeight;

        // Split text into lines
        string[] lines = text.Split('\n');

        // Draw each line separately to prevent overlap
        float yOffset = richTextRect.y;
        foreach (var line in lines)
        {
            float lineHeight = richTextStyle.CalcHeight(new GUIContent(line), richTextRect.width);
            Rect lineRect = new Rect(richTextRect.x, yOffset, richTextRect.width, lineHeight);
            EditorGUI.LabelField(lineRect, line, richTextStyle);
            yOffset += lineHeight;
        }

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        string text = property.stringValue;
        GUIContent richTextContent = new GUIContent(text);
        GUIStyle richTextStyle = new GUIStyle(EditorStyles.label);
        richTextStyle.richText = true;
        richTextStyle.wordWrap = true; // Enable word wrapping

        float textHeight = richTextStyle.CalcHeight(richTextContent, EditorGUIUtility.currentViewWidth);

        return textHeight + EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
    }
}

#endif