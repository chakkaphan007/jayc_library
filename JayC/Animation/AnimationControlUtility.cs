using System;
using UnityEngine;

namespace JayC.Utility
{
    public class AnimationControlUtility : MonoBehaviour
    {
        private Animator _animator;

        public event Action OnAnimationStart;  // Callback for when animation starts

        public event Action OnAnimationUpdate; // Callback for every frame during animation

        public event Action OnAnimationEnd;    // Callback for when animation ends

        private bool _isAnimationPlaying = false;
        private string _currentAnimationName;

        // Cache Animator to avoid repeated GetComponent calls
        private void Awake()
        {
            _animator = GetComponent<Animator>();
            if (_animator == null)
            {
                Debug.LogError("Animator component not found on the GameObject.");
            }
        }

        // Play animation by its name and assign callbacks
        public void PlayAnimation(string animationName, Action onStart = null, Action onUpdate = null, Action onEnd = null)
        {
            if (!IsAnimationValid(animationName)) return;

            // Assign callbacks
            OnAnimationStart = onStart;
            OnAnimationUpdate = onUpdate;
            OnAnimationEnd = onEnd;

            _currentAnimationName = animationName;

            // Trigger the start event
            OnAnimationStart?.Invoke();

            // Play animation
            _animator.Play(animationName);

            _isAnimationPlaying = true;
        }

        // Update to check if animation is running and call OnUpdate callback
        private void Update()
        {
            if (_isAnimationPlaying)
            {
                // Check if the animation is still playing
                var stateInfo = _animator.GetCurrentAnimatorStateInfo(0);
                if (stateInfo.IsName(_currentAnimationName))
                {
                    // OnUpdate callback
                    OnAnimationUpdate?.Invoke();

                    // If the animation is at the end
                    if (stateInfo.normalizedTime >= 1f && !stateInfo.loop)
                    {
                        // Animation finished
                        OnAnimationEnd?.Invoke();
                        _isAnimationPlaying = false;
                    }
                }
            }
        }

        // Crossfade between animations smoothly
        public void CrossfadeAnimation(string animationName, float transitionDuration, Action onStart = null, Action onUpdate = null, Action onEnd = null)
        {
            if (!IsAnimationValid(animationName)) return;

            // Assign callbacks
            OnAnimationStart = onStart;
            OnAnimationUpdate = onUpdate;
            OnAnimationEnd = onEnd;

            _currentAnimationName = animationName;

            // Trigger the start event
            OnAnimationStart?.Invoke();

            // Crossfade to new animation
            _animator.CrossFade(animationName, transitionDuration);

            _isAnimationPlaying = true;
        }

        // Stop the currently playing animation and reset callbacks
        public void StopCurrentAnimation()
        {
            _animator.StopPlayback();
            _isAnimationPlaying = false;
            _currentAnimationName = null;

            // Reset callbacks
            OnAnimationStart = null;
            OnAnimationUpdate = null;
            OnAnimationEnd = null;
        }

        // Utility to check if animation exists in the Animator Controller
        private bool IsAnimationValid(string animationName)
        {
            if (_animator.HasState(0, Animator.StringToHash(animationName)))
            {
                return true;
            }
            else
            {
                Debug.LogWarning($"Animation '{animationName}' not found.");
                return false;
            }
        }
    }
}