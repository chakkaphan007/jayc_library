#if JayC_Spine
using UnityEngine;
using Spine;
using Spine.Unity;
using UnityEngine.Events;

namespace JayC.Utility
{
    public class SpineAnimationUtility : MonoBehaviour
    {
        private SkeletonAnimation _skeletonAnimation;
        private TrackEntry _currentTrackEntry;
        private float _originalTimeScale = 1f;
        [SerializeField] private bool _isPlaying = false;

        public UnityEvent OnAnimationStart;
        public UnityEvent OnAnimationEnd;

        private void Awake()
        {
            _skeletonAnimation = GetComponent<SkeletonAnimation>();
        }

        public void PlayAnimation(string animationName, bool loop = false, System.Action onStart = null, System.Action onUpdate = null, System.Action onEnd = null)
        {
            if (_skeletonAnimation == null) return;

            _originalTimeScale = _skeletonAnimation.timeScale;
            _currentTrackEntry = _skeletonAnimation.AnimationState.SetAnimation(0, animationName, loop);

            OnAnimationStart?.Invoke();
            _isPlaying = true;

            if (onStart != null)
            {
                _currentTrackEntry.Complete += (entry) =>
                {
                    onEnd?.Invoke();
                    OnAnimationEnd?.Invoke();
                    _isPlaying = false;
                };
            }

            _skeletonAnimation.AnimationState.Event += (trackEntry, e) => onUpdate?.Invoke();
        }

        public void PauseAnimation()
        {
            if (_skeletonAnimation != null && _currentTrackEntry != null)
            {
                _skeletonAnimation.timeScale = 0f;
            }
        }

        public void ResumeAnimation()
        {
            if (_skeletonAnimation != null)
            {
                _skeletonAnimation.timeScale = _originalTimeScale;
            }
        }

        public void StopAnimation()
        {
            if (_skeletonAnimation != null)
            {
                _skeletonAnimation.AnimationState.ClearTrack(0);
            }
        }

        public void SetAnimationSpeed(float speed)
        {
            if (_skeletonAnimation != null)
            {
                _skeletonAnimation.timeScale = speed;
            }
        }

        private void Update()
        {
            if (_isPlaying && _skeletonAnimation != null)
            {
                _skeletonAnimation.Update(Time.deltaTime * _skeletonAnimation.timeScale);
            }
        }
    }
}
#endif