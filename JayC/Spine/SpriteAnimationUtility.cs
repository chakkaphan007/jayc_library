using System;
using UnityEngine;

namespace JayC.Utility.Animation
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class SpriteAnimationUtility : MonoBehaviour
    {
        public Sprite[] frames; // Array of sprite frames for the animation
        public float framesPerSecond = 10f; // Speed of animation

        private SpriteRenderer _spriteRenderer;
        private int _currentFrame;
        private float _timer;
        private bool _isPlaying;
        private bool _isPaused;
        private bool _isLooping;

        // Callbacks for animation events
        public event Action OnAnimationStart;

        public event Action OnAnimationUpdate;

        public event Action OnAnimationEnd;

        // Initialize the SpriteRenderer
        private void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            if (_spriteRenderer == null)
            {
                Debug.LogError("SpriteRenderer component not found on the GameObject.");
            }
        }

        // Play animation with optional callbacks and looping
        public void PlayAnimation(bool isLooping = false, Action onStart = null, Action onUpdate = null, Action onEnd = null)
        {
            if (frames == null || frames.Length == 0)
            {
                Debug.LogWarning("No frames assigned for sprite animation.");
                return;
            }

            // Set callbacks
            OnAnimationStart = onStart;
            OnAnimationUpdate = onUpdate;
            OnAnimationEnd = onEnd;

            // Animation state management
            _isPlaying = true;
            _isPaused = false;
            _isLooping = isLooping;
            _currentFrame = 0;
            _timer = 0f;

            // Invoke start callback
            OnAnimationStart?.Invoke();

            // Set the first frame
            _spriteRenderer.sprite = frames[_currentFrame];
        }

        // Pause the animation
        public void PauseAnimation()
        {
            if (_isPlaying)
            {
                _isPaused = true;
                _isPlaying = false;
            }
        }

        // Resume the paused animation
        public void ResumeAnimation()
        {
            if (_isPaused)
            {
                _isPaused = false;
                _isPlaying = true;
            }
        }

        // Stop the animation and reset the state
        public void StopAnimation()
        {
            _isPlaying = false;
            _isPaused = false;
            _currentFrame = 0;
            _spriteRenderer.sprite = null; // Clear sprite
        }

        // Update function to control the animation frame switching
        private void Update()
        {
            if (_isPlaying)
            {
                // Update the timer
                _timer += Time.deltaTime;

                // Calculate the time for each frame
                float frameTime = 1f / framesPerSecond;

                // If it's time to change the frame
                if (_timer >= frameTime)
                {
                    _timer -= frameTime;
                    _currentFrame++;

                    // Check if the animation has reached the last frame
                    if (_currentFrame >= frames.Length)
                    {
                        if (_isLooping)
                        {
                            _currentFrame = 0; // Loop back to the first frame
                        }
                        else
                        {
                            _currentFrame = frames.Length - 1; // Stay at the last frame
                            _isPlaying = false; // Stop animation

                            // Invoke the end callback
                            OnAnimationEnd?.Invoke();
                            return;
                        }
                    }

                    // Set the new frame
                    _spriteRenderer.sprite = frames[_currentFrame];

                    // Invoke the update callback
                    OnAnimationUpdate?.Invoke();
                }
            }
        }

        // Public method to get the current frame index
        public int GetCurrentFrame()
        {
            return _currentFrame;
        }
    }
}