///-----------------------------------------------------------------
///   Author : JayC         
///   Date   : 29/11/2024
///-----------------------------------------------------------------
#if JayC_Spine

using Spine;
using Spine.Unity;
using UnityEngine;
using System;
using System.Reflection;
using JayC.Utility;
using AnimationState = Spine.AnimationState;

namespace JayC.UI
{
    public static class SpineVisualElementExtensions
    {
        // Store the starting animation name for each SpineVisualElement
        private static readonly System.Collections.Generic.Dictionary<SpineVisualElement, string> StartingAnimations = new();

        // Play an animation and save the starting animation
        public static void PlayAnimation(this SpineVisualElement spineElement, string animationName, bool backToStartingState = true,bool loop = false, bool saveAsStarting = false, Action callback = null)
        {
            if (!spineElement.Validate())
                return;
            
            if (!StartingAnimations.ContainsKey(spineElement))
            {
                StartingAnimations.Add(spineElement, spineElement.startingAnimation);
            }
            
            if (string.IsNullOrEmpty(animationName))
            {
                LogError("Animation name is null or empty.");
                return;
            }

            // Save the starting animation if required
            if (saveAsStarting)
            {
                StartingAnimations[spineElement] = animationName;
            }

            // Set the requested animation
            spineElement.AnimationState.SetAnimation(0, animationName, loop);


            // If the animation is non-looping and we need to revert to the starting animation, set up the callback
            if (!loop && backToStartingState)
            {
                // Define the complete event listener
                AnimationState.TrackEntryDelegate completeHandler = null;
                
                completeHandler += entry =>
                {
                    // Check if the animation is complete
                    if (entry.IsComplete)
                    {
                        // Revert to the starting animation
                        RevertToStartingAnimation(spineElement);

                        // Trigger the callback
                        callback.SafeInvoke();
                        // Remove the listener to avoid multiple invocations
                        spineElement.AnimationState.Complete -= completeHandler;
                    }

                };
                    spineElement.AnimationState.Complete += completeHandler;
            }
            else
            {
                // Call callback immediately if looping or no back-to-start behavior
                callback.SafeInvoke();
            }
        }

        // Revert to the starting animation
        public static void RevertToStartingAnimation(this SpineVisualElement spineElement)
        {
            if (!spineElement.Validate())
                return;

            if (StartingAnimations.TryGetValue(spineElement, out var startingAnimation))
            {
                PlayAnimation(spineElement, startingAnimation, loop: true, backToStartingState: false);
            }
            else
            {
                LogError("Starting animation not found. Ensure PlayAnimation was called with saveAsStarting set to true.");
            }
        }
        
                // Change the SkeletonDataAsset at runtime
        public static void ChangeSkeletonDataAsset(this SpineVisualElement spineElement, SkeletonDataAsset newSkeletonDataAsset, string initialAnimation = null, bool loop = true, Action callback = null)
        {
            if (!spineElement.Validate())
                return;

            if (newSkeletonDataAsset == null)
            {
                LogError("New SkeletonDataAsset is null.");
                return;
            }

            // Clear the existing element
            spineElement.ClearElement();

            // Set the new SkeletonDataAsset
            spineElement.skeletonDataAsset = newSkeletonDataAsset;

            // Reinitialize the SpineVisualElement
            spineElement.GetType()
                .GetMethod("Initialize", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance)
                ?.Invoke(spineElement, new object[] { true });
            
            if (string.IsNullOrEmpty(initialAnimation))
            {
                var skeletonData = newSkeletonDataAsset.GetSkeletonData(false);

                if (skeletonData == null)
                {
                    LogError("SkeletonData is null. Cannot play animation.");
                    return;
                }
            
                // If no specific animation, find the first available animation
                if (skeletonData.Animations.Count > 0)
                {
                    string firstAnimationName = skeletonData.Animations.Items[0].Name;
                    PlayAnimation(spineElement, firstAnimationName, true, loop, true, callback);
                }
                else
                {
                    LogError("No animations found in the SkeletonDataAsset.");
                callback.SafeInvoke();
                }
            }
            else
            {
                try
                {
                    PlayAnimation(spineElement, initialAnimation, true, loop, true, callback);
                    
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                callback.SafeInvoke();
                }
            }
        }

        // Change the active skin of the skeleton
        public static void ChangeSkin(this SpineVisualElement spineElement, string skinName, Action callback = null)
        {
            if (!spineElement.Validate())
                return;

            var skeleton = GetSkeleton(spineElement);
            if (skeleton == null)
            {
                LogError("Skeleton is null. Cannot change skin.");
                callback?.Invoke();
                return;
            }

            var skeletonData = skeleton.Data;
            var skin = skeletonData.FindSkin(skinName);
            if (skin == null)
            {
                LogError($"Skin '{skinName}' not found in the skeleton data.");
                callback?.Invoke();
                return;
            }

            // Set the new skin and reset to setup pose
            skeleton.SetSkin(skin);
            skeleton.SetToSetupPose();

            // Trigger callback after changing the skin
            callback?.Invoke();
        }
        
        // Set TimeScale
        public static void SetTimeScale(this SpineVisualElement spineElement,float timeScale, Action callback = null)
        {
            if (!spineElement.Validate())
                return;

            spineElement.timeScale = timeScale;
            callback.SafeInvoke();
        }

        // Pause animation
        public static void PauseAnimation(this SpineVisualElement spineElement, Action callback = null)
        {
            if (!spineElement.Validate())
                return;

            spineElement.timeScale = 0;
            callback.SafeInvoke();
        }

        // Resume animation
        public static void ResumeAnimation(this SpineVisualElement spineElement, Action callback = null)
        {
            if (!spineElement.Validate())
                return;

            spineElement.timeScale = 1;
            callback.SafeInvoke();
        }

        // Stop all animations
        public static void StopAllAnimations(this SpineVisualElement spineElement, Action callback = null)
        {
            if (!spineElement.Validate())
                return;

            var state = spineElement.GetAnimationState();
            state?.ClearTracks();
            callback.SafeInvoke();
        }

        // Helper methods
        private static bool Validate(this SpineVisualElement spineElement)
        {
            if (spineElement == null)
            {
                LogError("SpineVisualElement is null.");
                return false;
            }

            return true;
        }

        // Get the AnimationState
        private static AnimationState GetAnimationState(this SpineVisualElement spineElement)
        {
            if (spineElement == null)
            {
                LogError("SpineVisualElement is null.");
                return null;
            }

            // Use reflection to access the private state field
            return GetProperty(spineElement, "state") as AnimationState;
        }

        // Get the Skeleton
        private static Skeleton GetSkeleton(this SpineVisualElement spineElement)
        {
            if (spineElement == null)
            {
                LogError("SpineVisualElement is null.");
                return null;
            }

            // Use reflection to access the private skeleton field
            return GetProperty(spineElement, "skeleton") as Skeleton;
        }

        // Generic method to get a private property using reflection
        private static object GetProperty(this object obj, string propertyName)
        {
            if (obj == null)
            {
                LogError($"Object is null when trying to get property {propertyName}.");
                return null;
            }

            try
            {
                // Get the property with non-public binding
                var property = obj.GetType().GetProperty(propertyName, 
                    BindingFlags.NonPublic | 
                    BindingFlags.Public | 
                    BindingFlags.Instance);

                // Return the value if the property exists
                return property?.GetValue(obj);
            }
            catch (Exception ex)
            {
                LogError($"Error getting property {propertyName}: {ex.Message}");
                return null;
            }
        }

        // Generic method to set a private property using reflection
        private static void SetProperty(this object obj, string propertyName, object value)
        {
            if (obj == null)
            {
                LogError($"Object is null when trying to set property {propertyName}.");
                return;
            }

            try
            {
                // Get the property with non-public binding
                var property = obj.GetType().GetProperty(propertyName, 
                    BindingFlags.NonPublic | 
                    BindingFlags.Public | 
                    BindingFlags.Instance);

                // Set the value if the property exists
                property?.SetValue(obj, value);
            }
            catch (Exception ex)
            {
                LogError($"Error setting property {propertyName}: {ex.Message}");
            }
        }

        private static void LogError(string message)
        {
            Debug.LogError($"[SpineVisualElementExtensions] {message}");
        }
    }
}

#endif
