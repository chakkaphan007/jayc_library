using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace JayC.Utility.EventSystem
{
    /// <summary>
    /// Represents a strongly-typed event in the system
    /// </summary>
    public interface IEvent { }

    /// <summary>
    /// Base class for events that need to be canceled
    /// </summary>
    public class CancellableEvent : IEvent
    {
        public bool IsCancelled { get; private set; }
        public void Cancel() => IsCancelled = true;
    }

    /// <summary>
    /// Event priority levels
    /// </summary>
    public enum EventPriority
    {
        Lowest = 0,
        Low = 1,
        Normal = 2,
        High = 3,
        Highest = 4,
        Monitor = 5
    }

    /// <summary>
    /// Subscription options for event handlers
    /// </summary>
    public class SubscriptionOptions
    {
        public EventPriority Priority { get; set; } = EventPriority.Normal;
        public string Group { get; set; }
        public Func<IEvent, bool> Filter { get; set; }
        public bool RunOnMainThread { get; set; }
        public bool HandleCancelled { get; set; }
    }

    /// <summary>
    /// Advanced EventBus implementation with support for strongly-typed events,
    /// priorities, filtering, and thread management
    /// </summary>
    public static class EventBus
    {
        private class Subscription
        {
            public Delegate Handler { get; set; }
            public EventPriority Priority { get; set; }
            public string Group { get; set; }
            public Func<IEvent, bool> Filter { get; set; }
            public bool RunOnMainThread { get; set; }
            public bool HandleCancelled { get; set; }
            public Type EventType { get; set; }
        }

        private static readonly Dictionary<Type, List<Subscription>> _subscriptions = new();
        private static readonly object _lock = new();
        private static readonly Queue<Action> _mainThreadActions = new();
        private static readonly AsyncLock _asyncLock = new();
        private static bool _isInitialized;

        #region Initialization

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void Initialize()
        {
            if (_isInitialized) return;
            
            // Create an empty GameObject to run the MainThreadExecutor
            var go = new GameObject("EventBus_MainThreadExecutor");
            go.AddComponent<MainThreadExecutor>();
            GameObject.DontDestroyOnLoad(go);
            
            _isInitialized = true;
        }

        private class MainThreadExecutor : MonoBehaviour
        {
            private void Update()
            {
                while (_mainThreadActions.Count > 0)
                {
                    if (_mainThreadActions.TryDequeue(out var action))
                    {
                        try
                        {
                            action?.Invoke();
                        }
                        catch (Exception ex)
                        {
                            Debug.LogError($"Error executing action on main thread: {ex}");
                        }
                    }
                }
            }
        }

        #endregion

        #region Subscription Methods

        /// <summary>
        /// Fluent API for subscribing to events
        /// </summary>
        public static IEventSubscriber<T> Subscribe<T>() where T : IEvent
        {
            return new EventSubscriber<T>();
        }

        private class EventSubscriber<T> : IEventSubscriber<T> where T : IEvent
        {
            private readonly SubscriptionOptions _options = new();

            public IEventSubscriber<T> WithPriority(EventPriority priority)
            {
                _options.Priority = priority;
                return this;
            }

            public IEventSubscriber<T> InGroup(string group)
            {
                _options.Group = group;
                return this;
            }

            public IEventSubscriber<T> WithFilter(Func<T, bool> filter)
            {
                _options.Filter = e => filter((T)e);
                return this;
            }

            public IEventSubscriber<T> OnMainThread()
            {
                _options.RunOnMainThread = true;
                return this;
            }

            public IEventSubscriber<T> HandleCancelledEvents()
            {
                _options.HandleCancelled = true;
                return this;
            }

            public IDisposable Handle(Action<T> handler)
            {
                var subscription = new Subscription
                {
                    Handler = handler,
                    Priority = _options.Priority,
                    Group = _options.Group,
                    Filter = _options.Filter,
                    RunOnMainThread = _options.RunOnMainThread,
                    HandleCancelled = _options.HandleCancelled,
                    EventType = typeof(T)
                };

                AddSubscription(subscription);
                return new SubscriptionHandle(() => RemoveSubscription(subscription));
            }
        }

        private class SubscriptionHandle : IDisposable
        {
            private readonly Action _unsubscribeAction;
            private bool _disposed;

            public SubscriptionHandle(Action unsubscribeAction)
            {
                _unsubscribeAction = unsubscribeAction;
            }

            public void Dispose()
            {
                if (_disposed) return;
                _unsubscribeAction?.Invoke();
                _disposed = true;
            }
        }

        #endregion

        #region Publishing Methods
        
        /// <summary>
        /// Publishes an event synchronously on the main thread to prevent Unity freezes
        /// </summary>
        public static void Publish<T>(T eventData) where T : IEvent
        {
            if (eventData == null) throw new ArgumentNullException(nameof(eventData));

            var eventType = typeof(T);
            List<Subscription> subscriptions;

            lock (_lock)
            {
                if (!_subscriptions.TryGetValue(eventType, out subscriptions))
                    return;

                // Create a copy of subscriptions to avoid modification during enumeration
                subscriptions = subscriptions.ToList();
            }

            // Sort by priority
            subscriptions = subscriptions.OrderByDescending(s => s.Priority).ToList();

            // Safely cast to CancellableEvent if applicable
            var cancellableEvent = eventData as CancellableEvent;
            var isCancellable = cancellableEvent != null;
            var isCancelled = isCancellable && cancellableEvent.IsCancelled;

            foreach (var subscription in subscriptions)
            {
                // Skip if event is cancelled and handler doesn't handle cancelled events
                if (isCancelled && !subscription.HandleCancelled)
                    continue;

                // Apply filter if exists
                if (subscription.Filter != null && !subscription.Filter(eventData))
                    continue;

                try
                {
                    if (subscription.RunOnMainThread || SynchronizationContext.Current == null)
                    {
                        // Execute directly if we're already on the main thread
                        if (Thread.CurrentThread.ManagedThreadId == 1)
                        {
                            ((Action<T>)subscription.Handler).Invoke(eventData);
                        }
                        else
                        {
                            _mainThreadActions.Enqueue(() => ((Action<T>)subscription.Handler).Invoke(eventData));
                        }
                    }
                    else
                    {
                        ((Action<T>)subscription.Handler).Invoke(eventData);
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogError($"Error publishing event {eventType.Name}: {ex}");
                }

                // Check if event was cancelled after handler execution
                if (isCancellable && cancellableEvent.IsCancelled)
                    break;
            }
        }

        /// <summary>
        /// Publishes an event asynchronously
        /// </summary>
        public static async Task PublishAsync<T>(T eventData) where T : IEvent
        {
            if (eventData == null) throw new ArgumentNullException(nameof(eventData));

            var eventType = typeof(T);
            List<Subscription> subscriptions;

            lock (_lock)
            {
                if (!_subscriptions.TryGetValue(eventType, out subscriptions))
                    return;

                subscriptions = subscriptions.ToList();
            }

            subscriptions = subscriptions.OrderByDescending(s => s.Priority).ToList();

            var cancellableEvent = eventData as CancellableEvent;
            var isCancellable = cancellableEvent != null;
            var isCancelled = isCancellable && cancellableEvent.IsCancelled;

            foreach (var subscription in subscriptions)
            {
                if (isCancelled && !subscription.HandleCancelled)
                    continue;

                if (subscription.Filter != null && !subscription.Filter(eventData))
                    continue;

                try
                {
                    if (subscription.RunOnMainThread)
                    {
                        var tcs = new TaskCompletionSource<bool>();
                        _mainThreadActions.Enqueue(() =>
                        {
                            try
                            {
                                ((Action<T>)subscription.Handler).Invoke(eventData);
                                tcs.SetResult(true);
                            }
                            catch (Exception ex)
                            {
                                tcs.SetException(ex);
                            }
                        });
                        await tcs.Task;
                    }
                    else
                    {
                        await Task.Run(() => ((Action<T>)subscription.Handler).Invoke(eventData));
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogError($"Error publishing event {eventType.Name}: {ex}");
                }

                if (isCancellable && cancellableEvent.IsCancelled)
                    break;
            }
        }
        
        /// <summary>
        /// Publishes an event to a specific group synchronously.
        /// </summary>
        public static void PublishGroup<T>(T eventData, string group) where T : IEvent
        {
            if (eventData == null) throw new ArgumentNullException(nameof(eventData));
            if (string.IsNullOrEmpty(group)) throw new ArgumentNullException(nameof(group));

            var eventType = typeof(T);
            List<Subscription> subscriptions;

            lock (_lock)
            {
                if (!_subscriptions.TryGetValue(eventType, out subscriptions))
                    return;

                // Filter by group
                subscriptions = subscriptions.Where(s => s.Group == group).ToList();
            }

            // Sort by priority
            subscriptions = subscriptions.OrderByDescending(s => s.Priority).ToList();

            // Process as a cancellable event if applicable
            var cancellableEvent = eventData as CancellableEvent;
            var isCancellable = cancellableEvent != null;
            var isCancelled = isCancellable && cancellableEvent.IsCancelled;

            foreach (var subscription in subscriptions)
            {
                if (isCancelled && !subscription.HandleCancelled)
                    continue;

                if (subscription.Filter != null && !subscription.Filter(eventData))
                    continue;

                try
                {
                    if (subscription.RunOnMainThread || SynchronizationContext.Current == null)
                    {
                        // Execute on the main thread if required
                        if (Thread.CurrentThread.ManagedThreadId == 1)
                        {
                            ((Action<T>)subscription.Handler).Invoke(eventData);
                        }
                        else
                        {
                            _mainThreadActions.Enqueue(() => ((Action<T>)subscription.Handler).Invoke(eventData));
                        }
                    }
                    else
                    {
                        ((Action<T>)subscription.Handler).Invoke(eventData);
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogError($"Error publishing event {eventType.Name} to group '{group}': {ex}");
                }

                // Stop if the event is cancelled after the handler
                if (isCancellable && cancellableEvent.IsCancelled)
                    break;
            }
        }

        #endregion

        #region Helper Methods

        private static void AddSubscription(Subscription subscription)
        {
            lock (_lock)
            {
                if (!_subscriptions.TryGetValue(subscription.EventType, out var subscriptions))
                {
                    subscriptions = new List<Subscription>();
                    _subscriptions[subscription.EventType] = subscriptions;
                }
                subscriptions.Add(subscription);
            }
        }

        private static void RemoveSubscription(Subscription subscription)
        {
            lock (_lock)
            {
                if (_subscriptions.TryGetValue(subscription.EventType, out var subscriptions))
                {
                    subscriptions.Remove(subscription);
                    if (subscriptions.Count == 0)
                    {
                        _subscriptions.Remove(subscription.EventType);
                    }
                }
            }
        }

        /// <summary>
        /// Clears all subscriptions in a specific group
        /// </summary>
        public static void ClearGroup(string group)
        {
            if (string.IsNullOrEmpty(group)) return;

            lock (_lock)
            {
                foreach (var subscriptions in _subscriptions.Values)
                {
                    subscriptions.RemoveAll(s => s.Group == group);
                }
                // Clean up empty event types
                var emptyTypes = _subscriptions.Where(kvp => kvp.Value.Count == 0)
                                             .Select(kvp => kvp.Key)
                                             .ToList();
                foreach (var type in emptyTypes)
                {
                    _subscriptions.Remove(type);
                }
            }
        }

        /// <summary>
        /// Clears all subscriptions
        /// </summary>
        public static void ClearAll()
        {
            lock (_lock)
            {
                _subscriptions.Clear();
            }
        }

        #endregion
    }

    #region Interfaces

    public interface IEventSubscriber<T> where T : IEvent
    {
        IEventSubscriber<T> WithPriority(EventPriority priority);
        IEventSubscriber<T> InGroup(string group);
        IEventSubscriber<T> WithFilter(Func<T, bool> filter);
        IEventSubscriber<T> OnMainThread();
        IEventSubscriber<T> HandleCancelledEvents();
        IDisposable Handle(Action<T> handler);
    }

    #endregion
}