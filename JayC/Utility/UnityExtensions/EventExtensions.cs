///-----------------------------------------------------------------
///   Author : JayC
///   Date   : 30/10/2024
///-----------------------------------------------------------------
using System;
using UnityEngine;
using UnityEngine.Events;

namespace JayC.Utility
{
    public static class EventExtensions
    {
        /// <summary>
        /// Safely invokes a UnityAction with null checking and logs a debug message if invoked.
        /// </summary>
        public static void SafeInvoke(this UnityAction unityAction, string message = "UnityAction invoked",bool showDebug = true)
        {
            if (unityAction != null)
            {
                if (showDebug)
                {
                    Debug.Log(message);
                }
                unityAction.Invoke();
            }
            else
            {
                Debug.LogWarning("UnityAction is null; invocation skipped.");
            }
        }

        /// <summary>
        /// Safely invokes a UnityAction with a single parameter, with null checking and logs a debug message if invoked.
        /// </summary>
        public static void SafeInvoke<T>(this UnityAction<T> unityAction, T arg, string message = "UnityAction<T> invoked",bool showDebug = true)
        {
            if (unityAction != null)
            {
                if (showDebug)
                {
                    Debug.Log(message);
                }
                unityAction.Invoke(arg);
            }
            else
            {
                Debug.LogWarning("UnityAction<T> is null; invocation skipped.");
            }
        }

        // Add overloads for UnityAction with up to 6 parameters

        public static void SafeInvoke<T1, T2>(this UnityAction<T1, T2> unityAction, T1 arg1, T2 arg2, string message = "UnityAction<T1, T2> invoked",bool showDebug = true)
        {
            if (unityAction != null)
            {
                if (showDebug)
                {
                    Debug.Log(message);
                }
                unityAction.Invoke(arg1, arg2);
            }
            else
            {
                Debug.LogWarning("UnityAction<T1, T2> is null; invocation skipped.");
            }
        }

        public static void SafeInvoke<T1, T2, T3>(this UnityAction<T1, T2, T3> unityAction, T1 arg1, T2 arg2, T3 arg3, string message = "UnityAction<T1, T2, T3> invoked",bool showDebug = true)
        {
            if (unityAction != null)
            {
                if (showDebug)
                {
                    Debug.Log(message);
                }
                unityAction.Invoke(arg1, arg2, arg3);
            }
            else
            {
                Debug.LogWarning("UnityAction<T1, T2, T3> is null; invocation skipped.");
            }
        }

        public static void SafeInvoke<T1, T2, T3, T4>(this UnityAction<T1, T2, T3, T4> unityAction, T1 arg1, T2 arg2, T3 arg3, T4 arg4, string message = "UnityAction<T1, T2, T3, T4> invoked",bool showDebug = true)
        {
            if (unityAction != null)
            {
                if (showDebug)
                {
                    Debug.Log(message);
                }
                unityAction.Invoke(arg1, arg2, arg3, arg4);
            }
            else
            {
                Debug.LogWarning("UnityAction<T1, T2, T3, T4> is null; invocation skipped.");
            }
        }

        // Add overloads for Action with up to 6 parameters

        public static void SafeInvoke(this Action action, string message = "Action invoked",bool showDebug = false)
        {
            if (action != null)
            {
                if (showDebug)
                {
                    Debug.Log(message);
                }
                action.Invoke();
            }
            else
            {
                if (showDebug)
                {
                    Debug.LogWarning("Action is null; invocation skipped.");
                }
            }
        }

        public static void SafeInvoke<T>(this Action<T> action, T arg, string message = "Action<T> invoked",bool showDebug = false)
        {
            if (action != null)
            {
                if (showDebug)
                {
                    Debug.Log(message);
                }
                action.Invoke(arg);
            }
            else
            {
                if (showDebug)
                {
                    Debug.LogWarning("Action is null; invocation skipped.");
                }
            }
        }

        public static void SafeInvoke<T1, T2>(this Action<T1, T2> action, T1 arg1, T2 arg2, string message = "Action<T1, T2> invoked",bool showDebug = false)
        {
            if (action != null)
            {
                if (showDebug)
                {
                    Debug.Log(message);
                }
                action.Invoke(arg1, arg2);
            }
            else
            {
                if (showDebug)
                {
                    Debug.LogWarning("Action<T1, T2> is null; invocation skipped.");
                }
            }
        }

        public static void SafeInvoke<T1, T2, T3>(this Action<T1, T2, T3> action, T1 arg1, T2 arg2, T3 arg3, string message = "Action<T1, T2, T3> invoked",bool showDebug = false)
        {
            if (action != null)
            {
                if (showDebug)
                {
                    Debug.Log(message);
                }
                action.Invoke(arg1, arg2, arg3);
            }
            else
            {
                if (showDebug)
                {
                    Debug.LogWarning("Action<T1, T2, T3> is null; invocation skipped.");
                }
            }
        }

        public static void SafeInvoke<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> action, T1 arg1, T2 arg2, T3 arg3, T4 arg4, string message = "Action<T1, T2, T3, T4> invoked",bool showDebug = false)
        {
            if (action != null)
            {
                if (showDebug)
                {
                    Debug.Log(message);
                }
                action.Invoke(arg1, arg2, arg3, arg4);
            }
            else
            {
                if (showDebug)
                {
                    Debug.LogWarning("Action<T1, T2, T3, T4> is null; invocation skipped.");
                }
            }
        }

        public static void SafeInvoke<T1, T2, T3, T4, T5>(this Action<T1, T2, T3, T4, T5> action, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, string message = "Action<T1, T2, T3, T4, T5> invoked",bool showDebug = false)
        {
            if (action != null)
            {
                if (showDebug)
                {
                    Debug.Log(message);
                }
                action.Invoke(arg1, arg2, arg3, arg4, arg5);
            }
            else
            {
                if (showDebug)
                {
                    Debug.LogWarning("Action<T1, T2, T3, T4, T5> is null; invocation skipped.");
                }
            }
        }

        public static void SafeInvoke<T1, T2, T3, T4, T5, T6>(this Action<T1, T2, T3, T4, T5, T6> action, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, string message = "Action<T1, T2, T3, T4, T5, T6> invoked",bool showDebug = false)
        {
            if (action != null)
            {
                if (showDebug)
                {
                    Debug.Log(message);
                }
                action.Invoke(arg1, arg2, arg3, arg4, arg5, arg6);
            }
            else
            {
                if (showDebug)
                {
                    Debug.LogWarning("Action<T1, T2, T3, T4, T5, T6> is null; invocation skipped.");
                }
            }
        }
    }
}
