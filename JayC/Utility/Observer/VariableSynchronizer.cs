///-----------------------------------------------------------------
///   Author : JayC          
///   Date   : 22/11/2024
///-----------------------------------------------------------------

using System.Collections.Generic;
using JayC.Utility;
using UnityEngine;

namespace JayC
{
        public class VariableSynchronizer<T>
        {
            private readonly List<ObservableVariable<T>> variables = new List<ObservableVariable<T>>();

            /// <summary>
            /// Adds a variable to the synchronizer and sets up change listeners.
            /// </summary>
            public void Add(ObservableVariable<T> variable)
            {
                if (!variables.Contains(variable))
                {
                    variables.Add(variable);
                    variable.OnValueChange += HandleValueChange;
                }
            }

            /// <summary>
            /// Removes a variable from the synchronizer and cleans up listeners.
            /// </summary>
            public void Remove(ObservableVariable<T> variable)
            {
                if (variables.Contains(variable))
                {
                    variables.Remove(variable);
                    variable.OnValueChange -= HandleValueChange;
                }
            }

            /// <summary>
            /// Handles synchronization when a variable changes.
            /// </summary>
            private void HandleValueChange(T oldValue, T newValue)
            {
                foreach (var variable in variables)
                {
                    if (!EqualityComparer<T>.Default.Equals(variable.Value, newValue))
                    {
                        variable.Value = newValue;
                    }
                }
            }

            /// <summary>
            /// Clears all variables from the synchronizer.
            /// </summary>
            public void Clear()
            {
                foreach (var variable in variables)
                {
                    variable.OnValueChange -= HandleValueChange;
                }
                variables.Clear();
            }
        }
    }
