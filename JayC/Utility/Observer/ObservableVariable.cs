///-----------------------------------------------------------------
///   Author : JayC               
///   Date   : 22/11/2024
///-----------------------------------------------------------------

using System;
using System.Collections.Generic;
using UnityEngine;

namespace JayC.Utility
{
[Serializable]
    public class ObservableVariable<T>
    {
        [SerializeField] private T value;

        /// <summary>
        /// Event triggered whenever the value changes.
        /// </summary>
        public event Action<T, T> OnValueChange;

        /// <summary>
        /// Get or set the value with event notification on change.
        /// </summary>
        public T Value
        {
            get => value;
            set
            {
                if (!EqualityComparer<T>.Default.Equals(this.value, value))
                {
                    T oldValue = this.value;
                    this.value = value;
                    OnValueChange?.Invoke(oldValue, this.value);
                }
            }
        }

        public ObservableVariable() { }

        public ObservableVariable(T initialValue)
        {
            value = initialValue;
        }

        /// <summary>
        /// Resets the value to its default.
        /// </summary>
        public void ResetValue()
        {
            Value = default;
        }

        /// <summary>
        /// Checks if the current value matches the provided value.
        /// </summary>
        /// <param name="comparison">Value to compare against.</param>
        /// <returns>True if values match; otherwise, false.</returns>
        public bool IsEqual(T comparison)
        {
            return EqualityComparer<T>.Default.Equals(value, comparison);
        }

        /// <summary>
        /// Utility to add listeners to the OnValueChange event.
        /// </summary>
        /// <param name="listener">Listener to add.</param>
        public void AddListener(Action<T, T> listener)
        {
            OnValueChange += listener;
        }

        /// <summary>
        /// Utility to remove listeners from the OnValueChange event.
        /// </summary>
        /// <param name="listener">Listener to remove.</param>
        public void RemoveListener(Action<T, T> listener)
        {
            OnValueChange -= listener;
        }
    }

    // Specific implementations for common types
    [Serializable]
    public class ObservableBool : ObservableVariable<bool> { public ObservableBool(bool value = false) : base(value) { } }
    [Serializable]
    public class ObservableInt : ObservableVariable<int> { public ObservableInt(int value = 0) : base(value) { } }
    [Serializable]
    public class ObservableFloat : ObservableVariable<float> { public ObservableFloat(float value = 0f) : base(value) { } }
    [Serializable]
    public class ObservableString : ObservableVariable<string> { public ObservableString(string value = "") : base(value) { } }
}

