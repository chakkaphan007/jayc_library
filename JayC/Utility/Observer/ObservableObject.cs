using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using UnityEngine;

namespace JayC.Utility
{
    // Attribute to mark properties as observable
    [AttributeUsage(AttributeTargets.Property)]
    public class ObservablePropertyAttribute : Attribute { }

    // Base class for objects with observable properties
    public abstract class ObservableObject : MonoBehaviour,INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private Dictionary<string, object> _propertyValues = new Dictionary<string, object>();

        protected virtual void Awake()
        {
            InitializeObservableProperties();
        }

        private void InitializeObservableProperties()
        {
            foreach (var property in GetType().GetProperties())
            {
                if (Attribute.IsDefined(property, typeof(ObservablePropertyAttribute)))
                {
                    _propertyValues[property.Name] = property.GetValue(this);
                }
            }
        }

        protected T GetValue<T>(string propertyName)
        {
            if (_propertyValues.TryGetValue(propertyName, out object value))
            {
                return (T)value;
            }
            return default;
        }

        protected bool SetValue<T>(T value, string propertyName)
        {
            if (!_propertyValues.TryGetValue(propertyName, out object currentValue) || !EqualityComparer<T>.Default.Equals((T)currentValue, value))
            {
                _propertyValues[propertyName] = value;
                NotifyPropertyChanged(propertyName);
                return true;
            }
            return false;
        }

        protected virtual void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            PropertyChangeNotifier.Notify(propertyName, this, new PropertyChangedEventArgs(propertyName));
        }
    }

    // Central event hub for property changes
    public static class PropertyChangeNotifier
    {
        private static Dictionary<string, Action<object, PropertyChangedEventArgs>> _listeners = new Dictionary<string, Action<object, PropertyChangedEventArgs>>();

        public static void Subscribe(string propertyName, Action<object, PropertyChangedEventArgs> listener)
        {
            if (!_listeners.ContainsKey(propertyName))
            {
                _listeners[propertyName] = listener;
            }
            else
            {
                _listeners[propertyName] += listener;
            }
        }

        public static void Unsubscribe(string propertyName, Action<object, PropertyChangedEventArgs> listener)
        {
            if (_listeners.ContainsKey(propertyName))
            {
                _listeners[propertyName] -= listener;
            }
        }

        public static void Notify(string propertyName, object sender, PropertyChangedEventArgs args)
        {
            if (_listeners.TryGetValue(propertyName, out var listener))
            {
                listener?.Invoke(sender, args);
            }
        }
    }
}