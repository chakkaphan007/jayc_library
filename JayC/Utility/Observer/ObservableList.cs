///-----------------------------------------------------------------
///   Author : JayC 
///   Date   : 22/11/2024
///-----------------------------------------------------------------

using System;
using System.Collections.Generic;
using UnityEngine;

namespace JayC.Utility
{
    public class ObservableList<T> : List<T>
    {
        /// <summary>
        /// Event triggered when an item is added.
        /// </summary>
        public event Action<T> OnItemAdded;

        /// <summary>
        /// Event triggered when an item is removed.
        /// </summary>
        public event Action<T> OnItemRemoved;

        /// <summary>
        /// Event triggered when the list is cleared.
        /// </summary>
        public event Action OnListCleared;

        /// <summary>
        /// Adds an item to the list and triggers OnItemAdded.
        /// </summary>
        public new void Add(T item)
        {
            base.Add(item);
            OnItemAdded?.Invoke(item);
        }

        /// <summary>
        /// Removes an item from the list and triggers OnItemRemoved.
        /// </summary>
        public new bool Remove(T item)
        {
            bool removed = base.Remove(item);
            if (removed)
            {
                OnItemRemoved?.Invoke(item);
            }
            return removed;
        }

        /// <summary>
        /// Clears all items in the list and triggers OnListCleared.
        /// </summary>
        public new void Clear()
        {
            base.Clear();
            OnListCleared?.Invoke();
        }

        /// <summary>
        /// Adds a range of items to the list and triggers OnItemAdded for each item.
        /// </summary>
        public new void AddRange(IEnumerable<T> collection)
        {
            foreach (var item in collection)
            {
                Add(item); // This ensures OnItemAdded is triggered for each item.
            }
        }

        /// <summary>
        /// Removes all items matching a predicate and triggers OnItemRemoved for each.
        /// </summary>
        public void RemoveAll(Predicate<T> match)
        {
            for (int i = Count - 1; i >= 0; i--)
            {
                if (match(this[i]))
                {
                    Remove(this[i]); // This ensures OnItemRemoved is triggered.
                }
            }
        }
    }
}
