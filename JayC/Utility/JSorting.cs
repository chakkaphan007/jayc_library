using System;
using System.Collections.Generic;
using UnityEngine;

namespace JayC.Utility
{
    public class JSorting : MonoBehaviour
    {
        // Bubble Sort
        public static T[] BubbleSort<T>(T[] arr)
        {
            int n = arr.Length;
            bool swapped;

            do
            {
                swapped = false;
                for (int i = 1; i < n; i++)
                {
                    if (Comparer<T>.Default.Compare(arr[i - 1], arr[i]) > 0)
                    {
                        (arr[i - 1], arr[i]) = (arr[i], arr[i - 1]);
                        swapped = true;
                    }
                }
            } while (swapped);

            return arr;
        }

        // Heap Sort
        public static T[] HeapSort<T>(T[] arr) where T : IComparable<T>
        {
            int n = arr.Length;

            // Build max heap
            for (int i = n / 2 - 1; i >= 0; i--)
                Heapify(arr, n, i);

            // Extract elements one by one
            for (int i = n - 1; i > 0; i--)
            {
                (arr[0], arr[i]) = (arr[i], arr[0]);

                Heapify(arr, i, 0);
            }

            return arr;
        }

        private static void Heapify<T>(T[] arr, int n, int i) where T : IComparable<T>
        {
            int largest = i;
            int left = 2 * i + 1;
            int right = 2 * i + 2;

            if (left < n && arr[left].CompareTo(arr[largest]) > 0)
                largest = left;

            if (right < n && arr[right].CompareTo(arr[largest]) > 0)
                largest = right;

            if (largest != i)
            {
                (arr[i], arr[largest]) = (arr[largest], arr[i]);

                Heapify(arr, n, largest);
            }
        }

        // Quick Sort
        public static T[] QuickSort<T>(T[] arr) where T : IComparable<T>
        {
            QuickSortRecursive(arr, 0, arr.Length - 1);
            return arr;
        }

        static void QuickSortRecursive<T>(T[] arr, int low, int high) where T : IComparable<T>
        {
            if (low < high)
            {
                int pivotIndex = Partition(arr, low, high);

                QuickSortRecursive(arr, low, pivotIndex - 1);
                QuickSortRecursive(arr, pivotIndex + 1, high);
            }
        }

        static int Partition<T>(T[] arr, int low, int high) where T : IComparable<T>
        {
            T pivot = arr[high];
            int i = low - 1;

            for (int j = low; j < high; j++)
            {
                if (arr[j].CompareTo(pivot) < 0)
                {
                    i++;
                    (arr[i], arr[j]) = (arr[j], arr[i]);
                }
            }

            (arr[i + 1], arr[high]) = (arr[high], arr[i + 1]);

            return i + 1;
        }

        //Debugging
        public static string ArrayToString<T>(T[] arr)
        {
            string result = "[";
            for (int i = 0; i < arr.Length; i++)
            {
                result += arr[i].ToString();
                if (i < arr.Length - 1)
                    result += ", ";
            }

            result += "]";
            return result;
        }
    }
}
