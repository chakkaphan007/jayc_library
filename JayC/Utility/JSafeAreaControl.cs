using UnityEngine;

namespace JayC
{
    [ExecuteInEditMode]
    public class JSafeAreaControl : MonoBehaviour
    {
        private RectTransform Panel;
        private Rect LastSafeArea = new Rect(0, 0, 0, 0);

        private void Start()
        {
            Panel = GetComponent<RectTransform>();
            ApplySafeArea(GetSafeArea());
        }

        private void Update()
        {
            Refresh();
        }

        private void Refresh()
        {
            Rect safeArea = GetSafeArea();

            if (safeArea != LastSafeArea)
                ApplySafeArea(safeArea);
        }

        private Rect GetSafeArea()
        {
            return Screen.safeArea;
        }

        private void ApplySafeArea(Rect r)
        {
            LastSafeArea = r;

            // Convert safe area rectangle from absolute pixels to normalized anchor coordinates
            Vector2 anchorMin = r.position;
            Vector2 anchorMax = r.position + r.size;
            anchorMin.x /= Screen.width;
            anchorMin.y /= Screen.height;
            anchorMax.x /= Screen.width;
            anchorMax.y /= Screen.height;
            Panel.anchorMin = anchorMin;
            Panel.anchorMax = anchorMax;
        }
    }
}