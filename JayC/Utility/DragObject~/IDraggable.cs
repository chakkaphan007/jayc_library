using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JayC
{
    public interface IDraggable
    {
        bool _isDragging { get; set; }

        public void OnDragStart();
        public void OnDragEnd();
        
        
    }
}
