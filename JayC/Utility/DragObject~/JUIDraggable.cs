using UnityEngine;
using UnityEngine.EventSystems;

namespace JayC.Utility
{
    public class JUIDraggable : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler,IDraggable
    {
        public bool _isDragging { get; set; }
        protected RectTransform rectTransform;
        protected Vector2 offset;
        protected Vector2 targetPosition;
        private PointerEventData _pointerEventData;

        public float dragSmoothness = 5f; // Adjust this smoothness as needed

        protected Vector2 currentVelocity; // For smooth dragging

        protected virtual void Start()
        {
            rectTransform = GetComponent<RectTransform>();
            targetPosition = rectTransform.position; // Initialize target position
        }

        public virtual void OnPointerDown(PointerEventData eventData)
        {
            _pointerEventData = eventData;
            OnDragStart();
        }

        public virtual void OnDrag(PointerEventData eventData)
        {
            if (_isDragging)
            {
                targetPosition = eventData.position - offset;
            }
        }

        public virtual void OnPointerUp(PointerEventData eventData)
        {
            _isDragging = false;
        }

        protected virtual void Update()
        {
            // Smoothly interpolate to the new target position
            rectTransform.position = Vector2.SmoothDamp(
                rectTransform.position
                , targetPosition
                , ref currentVelocity,
                dragSmoothness);
        }
        
        public void OnDragStart()
        {
            _isDragging = true;
            offset = _pointerEventData.position - (Vector2)rectTransform.position;
        }
        
        public void OnDragEnd()
        {
            _isDragging = false;
        }

    }
}