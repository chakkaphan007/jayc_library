using UnityEngine;
#if ENABLE_INPUT_SYSTEM
using UnityEngine.InputSystem;
#endif

namespace JayC.Utility
{
    public class JObjectDraggable : MonoBehaviour ,IDraggable
    {
        public bool _isDragging { get; set; }

        protected Vector3 _dragOffset;
        protected Vector2 _lastInputPosition; // Store the last input position
        public Camera _mainCamera;

        public float _heightAdjustmentSpeed = 10f; // Speed at which the object adjusts its height
        [SerializeField] private float _yOffset = 0f; // Y-position offset
        protected float _targetHeight; // Target height for the object
        
#if ENABLE_INPUT_SYSTEM
        public InputActionReference ClickAction; // Reference to the click action
        public InputActionReference dragAction; // Reference to the drag action
#endif

        protected virtual void Start()
        {
            if (!_mainCamera) _mainCamera = Camera.main;
        }

        protected virtual void OnEnable()
        {
#if ENABLE_INPUT_SYSTEM
            if (ClickAction&&dragAction)
            {
                ClickAction.action.Enable();
                dragAction.action.Enable();
                ClickAction.action.performed += DragStart;
                ClickAction.action.canceled += DragEnd;
            }            
            else
                Debug.LogError("ClickAction or dragAction is null");
#endif
        }
        
        protected virtual void OnDisable()
        {
#if ENABLE_INPUT_SYSTEM
            ClickAction.action.Disable();
            dragAction.action.Disable();
            
            ClickAction.action.performed -= DragStart;
            ClickAction.action.canceled -= DragEnd;
#endif
        }

        protected virtual void Update()
        {
#if ENABLE_INPUT_SYSTEM
            if (!dragAction||!ClickAction) return;
#else
            // Check for dragging initiation or end here
            if (!isDragging)
            {

                if (Input.GetMouseButtonDown(0) ||
                    (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))
                {
                    OnDragStart();
                }

            }
#endif
            else
            {
#if ENABLE_INPUT_SYSTEM
#else
                if (Input.GetMouseButtonUp(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended))
                {
                    OnDragEnd();
                }
#endif
            }

            Follow();
        }
#if ENABLE_INPUT_SYSTEM
        private void DragStart(InputAction.CallbackContext obj)
        {
            OnDragStart();
        }        
        private void DragEnd(InputAction.CallbackContext obj)
        {
            OnDragEnd();
        }
#endif
        
        public void OnDragStart()
        {
            Vector2 inputPosition;
#if ENABLE_INPUT_SYSTEM
            inputPosition = dragAction.action.ReadValue<Vector2>();
#else
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                inputPosition = touch.position;
            }
            else
            {
                inputPosition = Input.mousePosition;
            }
#endif

            Ray ray = _mainCamera.ScreenPointToRay(inputPosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit) && hit.collider.gameObject == gameObject)
            {
                _isDragging = true;
                // Calculate the offset between the input position and the object position
                _dragOffset = transform.position - ray.GetPoint(hit.distance);
            }
        }

        public virtual void OnDragEnd()
        {
            _isDragging = false;
        }

        protected virtual void Follow()
        {
            if (_isDragging)
            {
                Vector2 inputPosition;
#if ENABLE_INPUT_SYSTEM
            inputPosition = dragAction.action.ReadValue<Vector2>();
#else
                if (Input.touchCount > 0)
                {
                    Touch touch = Input.GetTouch(0);
                    inputPosition = touch.position;
                }
                else
                {
                    inputPosition = Input.mousePosition;
                }
#endif

                Ray ray = _mainCamera.ScreenPointToRay(inputPosition);
                Plane plane = new Plane(Vector3.up, Vector3.zero); // Assuming your ground is aligned with the Y-axis
                float distance;
                if (plane.Raycast(ray, out distance))
                {
                    Vector3 inputWorldPosition = ray.GetPoint(distance) + _dragOffset;

                    // Adjust the target height based on the input position and yOffset
                    _targetHeight =
                        Mathf.Clamp(inputWorldPosition.y + _yOffset, 0f, 10f); // Adjust the min and max height as needed

                    // Smoothly move the object to the target height
                    Vector3 newPosition = new Vector3(inputWorldPosition.x, _targetHeight, inputWorldPosition.z);
                    transform.position = Vector3.Lerp(transform.position, newPosition,
                        _heightAdjustmentSpeed * Time.deltaTime);

                    // Update the last input position
                    _lastInputPosition = inputPosition;
                }
            }
            else if (!_isDragging && _lastInputPosition != Vector2.zero)
            {
                // If dragging has ended and we have a last input position, continue moving to that position
                Vector2 newPosition = _lastInputPosition;
                Ray ray = _mainCamera.ScreenPointToRay(newPosition);
                Plane plane = new Plane(Vector3.up, Vector3.zero); // Assuming your ground is aligned with the Y-axis
                float distance;
                if (plane.Raycast(ray, out distance))
                {
                    Vector3 inputWorldPosition = ray.GetPoint(distance);

                    // Adjust the target height based on the input position and yOffset
                    _targetHeight =
                        Mathf.Clamp(inputWorldPosition.y + _yOffset, 0f, 10f); // Adjust the min and max height as needed

                    // Smoothly move the object to the target height
                    Vector3 finalPosition = new Vector3(inputWorldPosition.x, _targetHeight, inputWorldPosition.z);
                    transform.position = Vector3.Lerp(transform.position, finalPosition,
                        _heightAdjustmentSpeed * Time.deltaTime);
                }
            }
        }
    }
}
