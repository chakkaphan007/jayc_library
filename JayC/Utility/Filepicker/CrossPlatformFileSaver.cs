using System;
using System.Threading.Tasks;

namespace JayC.Utility.Filepicker
{
    public static class CrossPlatformFileSaver
    {
        private static IFileSaver fileSaver = FileSaverFactory.CreateFileSaver();

        public static Task<bool> SaveFileAsync(string path, string extension, string title)
        {
            return fileSaver.SaveFileAsync(path, extension, title);
        }
    }
}