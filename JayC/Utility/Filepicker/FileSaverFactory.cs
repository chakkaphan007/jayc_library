namespace JayC.Utility.Filepicker
{
    public static class FileSaverFactory
    {
        public static IFileSaver CreateFileSaver()
        {
#if UNITY_EDITOR && JayC_FilePicker
            return new UnityEditorFileSaver();
#elif UNITY_ANDROID && JayC_FilePicker
        return new AndroidFileSaver();
#elif UNITY_IOS
        return new IOSFileSaver();
#elif UNITY_STANDALONE_WIN
            return new WindowsFileSaver();
#elif UNITY_STANDALONE_OSX
        return new MacOSFileSaver();
#elif UNITY_SWITCH
        return new NintendoSwitchFileSaver();
#elif UNITY_PS4 || UNITY_PS5
        return new PlayStationFileSaver();
#else
            return new GenericFileSaver();
#endif
        }
    }
}