#if UNITY_PS4 || UNITY_PS5
using UnityEngine;
using System;
namespace JayC.Utility.Filepicker
{
public class PlayStationFileSaver : AbstractFileSaver
{
//w8 for fix
    public override void SaveFile(string defaultName, string extension, string title, Action<bool, string> callback)
    {
        // PlayStation saving requires the official PlayStation SDK
        // This is a placeholder implementation
        Debug.LogWarning("PlayStation file saving not implemented. Requires official SDK.");
        string path = $"{Application.persistentDataPath}/{defaultName}.{extension}";
        callback(true, path);
    }
}
}
#endif