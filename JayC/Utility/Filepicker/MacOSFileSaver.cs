#if UNITY_STANDALONE_OSX
using UnityEngine;
using System;
using System.IO;
namespace JayC.Utility.Filepicker
{
public class MacOSFileSaver : AbstractFileSaver
{
//w8 for fix
    public override void SaveFile(string defaultName, string extension, string title, Action<bool, string> callback)
    {
        // For MacOS, you might want to use a native plugin or a third-party asset
        // Here's a placeholder using direct file writing for demonstration
        string path = $"{GetDocumentsPath()}/{defaultName}.{extension}";
        try
        {
            File.WriteAllText(path, ""); // Create an empty file
            callback(true, path);
        }
        catch (Exception e)
        {
            Debug.LogError($"Error saving file on MacOS: {e.Message}");
            callback(false, null);
        }
    }
}
}
#endif