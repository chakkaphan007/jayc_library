#if JayC_FilePicker
using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;

namespace JayC.Utility.Filepicker
{
    public class FileExportManager : MonoBehaviour
    {
        public enum ExportFormat
        {
            CSV,
            JSON,
            XML,
            TXT
        }

        [System.Serializable]
        public abstract class ExportableData
        {
            public abstract string[] ToCSV();

            public abstract string[] GetCSVHeaders();

            public virtual string ToTXT() => string.Join("\n", ToCSV());
        }

        public static FileExportManager Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public async Task<bool> ExportDataAsync<T>(List<T> dataList, ExportFormat format, string fileName, Encoding encoding = null) where T : ExportableData
        {
            if (dataList == null || dataList.Count == 0)
            {
                Debug.LogWarning("No data to export.");
                return false;
            }

            encoding ??= Encoding.UTF8;
            string extension = GetFileExtension(format);
            string defaultName = $"{fileName}_{DateTime.Now:yyyyMMdd_HHmmss}";
            string tempFilePath = Path.Combine(AbstractFileSaver.GetDocumentsPath(), defaultName + extension);

            try
            {
                await ExportToFileAsync(dataList, format, tempFilePath, encoding);
                bool success = await SaveFileWithPicker(tempFilePath, extension, defaultName);

                if (success)
                {
                    Debug.Log($"File successfully exported: {tempFilePath}");
                }
                else
                {
                    Debug.LogError("Failed to export file");
                }

                return success;
            }
            catch (Exception ex)
            {
                Debug.LogError($"Error exporting file: {ex.Message}");
                return false;
            }
            finally
            {
#if UNITY_ANDROID
                if (File.Exists(tempFilePath))
                {
                    File.Delete(tempFilePath);
                }
#endif
            }
        }

        private string GetFileExtension(ExportFormat format)
        {
            return format switch
            {
                ExportFormat.CSV => ".csv",
                ExportFormat.JSON => ".json",
                ExportFormat.XML => ".xml",
                ExportFormat.TXT => ".txt",
                _ => throw new ArgumentException("Unsupported format"),
            };
        }

        private async Task ExportToFileAsync<T>(List<T> dataList, ExportFormat format, string fullPath, Encoding encoding) where T : ExportableData
        {
            switch (format)
            {
                case ExportFormat.CSV:
                    await ExportToCSVAsync(dataList, fullPath, encoding);
                    break;

                case ExportFormat.JSON:
                    await ExportToJSONAsync(dataList, fullPath, encoding);
                    break;

                case ExportFormat.XML:
                    await ExportToXMLAsync(dataList, fullPath, encoding);
                    break;

                case ExportFormat.TXT:
                    await ExportToTXTAsync(dataList, fullPath, encoding);
                    break;
            }
        }

        private async Task ExportToCSVAsync<T>(List<T> dataList, string fullPath, Encoding encoding) where T : ExportableData
        {
            using var writer = new StreamWriter(fullPath, false, encoding);
            await writer.WriteLineAsync(string.Join(",", dataList[0].GetCSVHeaders()));
            foreach (T data in dataList)
            {
                await writer.WriteLineAsync(string.Join(",", data.ToCSV()));
            }
        }

        private async Task ExportToJSONAsync<T>(List<T> dataList, string fullPath, Encoding encoding) where T : ExportableData
        {
            string json = JsonConvert.SerializeObject(dataList, Formatting.Indented);
            await File.WriteAllTextAsync(fullPath, json, encoding);
        }

        private async Task ExportToXMLAsync<T>(List<T> dataList, string fullPath, Encoding encoding) where T : ExportableData
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<T>));
            using var writer = new StreamWriter(fullPath, false, encoding);
            serializer.Serialize(writer, dataList);
            await writer.FlushAsync();
        }

        private async Task ExportToTXTAsync<T>(List<T> dataList, string fullPath, Encoding encoding) where T : ExportableData
        {
            using var writer = new StreamWriter(fullPath, false, encoding);
            foreach (T data in dataList)
            {
                await writer.WriteLineAsync(data.ToTXT());
            }
        }

        private async Task<bool> SaveFileWithPicker(string tempFilePath, string extension, string defaultName)
        {
            try
            {
                return await CrossPlatformFileSaver.SaveFileAsync(tempFilePath, extension, defaultName);
            }
            catch (Exception ex)
            {
                Debug.LogError($"Error saving file: {ex.Message}");
                return false;
            }
        }

        public async Task<List<T>> LoadAllDataAsync<T>(ExportFormat format, Encoding encoding = null) where T : ExportableData, new()
        {
            encoding ??= Encoding.UTF8;
            string folderPath = GetSaveFolder();
            string extension = GetFileExtension(format);
            List<T> allLoadedData = new List<T>();

            foreach (string filePath in Directory.GetFiles(folderPath, $"*{extension}"))
            {
                allLoadedData.AddRange(await ParseFileAsync<T>(filePath, format, encoding));
            }

            return allLoadedData;
        }

        private async Task<List<T>> ParseFileAsync<T>(string filePath, ExportFormat format, Encoding encoding) where T : ExportableData, new()
        {
            return format switch
            {
                ExportFormat.CSV => await ParseCSVAsync<T>(filePath, encoding),
                ExportFormat.JSON => await ParseJSONAsync<T>(filePath, encoding),
                ExportFormat.XML => await ParseXMLAsync<T>(filePath, encoding),
                ExportFormat.TXT => await ParseTXTAsync<T>(filePath, encoding),
                _ => throw new ArgumentException("Unsupported format"),
            };
        }

        private async Task<List<T>> ParseCSVAsync<T>(string filePath, Encoding encoding) where T : ExportableData, new()
        {
            List<T> parsedData = new List<T>();
            string[] lines = await File.ReadAllLinesAsync(filePath, encoding);

            for (int i = 1; i < lines.Length; i++) // Skip header
            {
                string[] values = lines[i].Split(',');
                T item = new T();
                // Implement a method to populate the item from CSV values
                PopulateFromCSV(item, values);
                parsedData.Add(item);
            }

            return parsedData;
        }

        private async Task<List<T>> ParseJSONAsync<T>(string filePath, Encoding encoding) where T : ExportableData, new()
        {
            string json = await File.ReadAllTextAsync(filePath, encoding);
            return JsonConvert.DeserializeObject<List<T>>(json);
        }

        private async Task<List<T>> ParseXMLAsync<T>(string filePath, Encoding encoding) where T : ExportableData, new()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<T>));
            using var stream = new FileStream(filePath, FileMode.Open);
            using var reader = new StreamReader(stream, encoding);
            return (List<T>)await Task.Run(() => serializer.Deserialize(reader));
        }

        private async Task<List<T>> ParseTXTAsync<T>(string filePath, Encoding encoding) where T : ExportableData, new()
        {
            List<T> parsedData = new List<T>();
            string[] lines = await File.ReadAllLinesAsync(filePath, encoding);

            foreach (string line in lines)
            {
                T item = new T();
                // Implement a method to populate the item from TXT line
                PopulateFromTXT(item, line);
                parsedData.Add(item);
            }

            return parsedData;
        }

        // Implement these methods in your specific ExportableData subclasses
        protected virtual void PopulateFromCSV(ExportableData item, string[] values)
        { }

        protected virtual void PopulateFromTXT(ExportableData item, string line)
        { }

        public string GetSaveFolder()
        {
            string folderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "MyGameData");
            Directory.CreateDirectory(folderPath);
            return folderPath;
        }

        // New utility methods
        public async Task<bool> FileExistsAsync(string filePath)
        {
            return await Task.Run(() => File.Exists(filePath));
        }

        public async Task<long> GetFileSizeAsync(string filePath)
        {
            var fileInfo = new FileInfo(filePath);
            return await Task.Run(() => fileInfo.Length);
        }

        public async Task<DateTime> GetLastModifiedTimeAsync(string filePath)
        {
            var fileInfo = new FileInfo(filePath);
            return await Task.Run(() => fileInfo.LastWriteTime);
        }

        public async Task<List<string>> GetAvailableFilesAsync(ExportFormat format)
        {
            string folderPath = GetSaveFolder();
            string extension = GetFileExtension(format);
            return await Task.Run(() => Directory.GetFiles(folderPath, $"*{extension}").ToList());
        }

        public async Task DeleteFileAsync(string filePath)
        {
            await Task.Run(() => File.Delete(filePath));
        }

        public async Task<bool> MergeFilesAsync<T>(List<string> filePaths, ExportFormat format, string outputFileName, Encoding encoding = null) where T : ExportableData, new()
        {
            encoding ??= Encoding.UTF8;
            List<T> mergedData = new List<T>();

            foreach (string filePath in filePaths)
            {
                mergedData.AddRange(await ParseFileAsync<T>(filePath, format, encoding));
            }

            return await ExportDataAsync(mergedData, format, outputFileName, encoding);
        }

#if JayC_FilePicker
        public async Task<NativeFilePicker.Permission> RequestPermissionAsync(bool readPermissionOnly = false)
        {
            return await NativeFilePicker.RequestPermissionAsync(readPermissionOnly);
        }
#endif
    }
}
#endif