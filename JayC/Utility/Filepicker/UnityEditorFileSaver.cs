#if UNITY_EDITOR
#if JayC_FilePicker

using System.IO;
using System.Threading.Tasks;
using UnityEngine;

namespace JayC.Utility.Filepicker
{
    public class UnityEditorFileSaver : AbstractFileSaver
    {
        //Save to C:\Users\{User}\AppData\Local\Temp\{Company}\{Project}
        public override Task<bool> SaveFileAsync(string path, string extension, string title)
        {
            NativeFilePicker.Permission permission = NativeFilePicker.ExportFile(path, (success) =>
            {
                if (success)
                {
                    Debug.Log($"File successfully exported: {path}");
                }
                else
                {
                    Debug.LogError("Failed to export file");
                }
                // Clean up the temporary file
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
            });
            return (permission == NativeFilePicker.Permission.Granted) ? Task.FromResult(true) : Task.FromResult(false);
        }
    }
}

#endif
#endif