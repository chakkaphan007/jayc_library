///-----------------------------------------------------------------
///   Author : JayC         
///   Date   : 6/12/2024
///-----------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace JayC.Utility
{
    public static class CSVExporter
    {
        /// <summary>
        /// Converts a list of objects to a CSV string.
        /// </summary>
        /// <typeparam name="T">The type of the objects to convert.</typeparam>
        /// <param name="data">The list of objects to convert.</param>
        /// <returns>A CSV formatted string.</returns>
        public static string ConvertToCSV<T>(List<T> data)
        {
            if (data == null || !data.Any())
                return string.Empty;

            var properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var header = string.Join(",", properties.Select(p => p.Name));
            var rows = data.Select(item => string.Join(",", properties.Select(p => GetFormattedValue(p.GetValue(item)))));
            
            return $"{header}\n{string.Join("\n", rows)}";
        }

        /// <summary>
        /// Exports a list of objects to a CSV file.
        /// </summary>
        /// <typeparam name="T">The type of the objects to export.</typeparam>
        /// <param name="filePath">The file path to save the CSV.</param>
        /// <param name="data">The list of objects to export.</param>
        public static void ExportToCSV<T>(string filePath, List<T> data)
        {
            var csvContent = ConvertToCSV(data);
            if (string.IsNullOrEmpty(csvContent))
            {
                Debug.LogWarning("No data provided for CSV export.");
                return;
            }

            File.WriteAllText(filePath, csvContent);
            Debug.Log($"CSV exported successfully to {filePath}");
        }

        /// <summary>
        /// Formats a value for CSV output (e.g., escaping commas and quotes).
        /// </summary>
        /// <param name="value">The value to format.</param>
        /// <returns>The formatted value as a string.</returns>
        private static string GetFormattedValue(object value)
        {
            if (value == null) return "";

            string stringValue = value.ToString();

            // Escape commas and quotes for CSV format
            if (stringValue.Contains(",") || stringValue.Contains("\""))
            {
                stringValue = $"\"{stringValue.Replace("\"", "\"\"")}\"";
            }

            return stringValue;
        }
    }
}

