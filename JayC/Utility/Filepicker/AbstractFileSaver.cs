using System;
using System.Threading.Tasks;
using UnityEngine;

namespace JayC.Utility.Filepicker
{
    public abstract class AbstractFileSaver : IFileSaver
    {
        public abstract Task<bool> SaveFileAsync(string path, string extension, string title);

        public static string GetDocumentsPath()
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
#elif UNITY_ANDROID
        return Application.temporaryCachePath;
#elif UNITY_IOS
        return Application.temporaryCachePath;
#else
        return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
#endif
        }
    }
}