#if UNITY_STANDALONE_WIN

using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace JayC.Utility.Filepicker
{
    public class WindowsFileSaver : AbstractFileSaver
    {
        [DllImport("Comdlg32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern bool GetSaveFileName([In, Out] OpenFileName ofn);

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        private class OpenFileName
        {
            public int structSize = 0;
            public IntPtr dlgOwner = IntPtr.Zero;
            public IntPtr instance = IntPtr.Zero;
            public string filter = null;
            public string customFilter = null;
            public int maxCustFilter = 0;
            public int filterIndex = 0;
            public string file = null;
            public int maxFile = 0;
            public string fileTitle = null;
            public int maxFileTitle = 0;
            public string initialDir = null;
            public string title = null;
            public int flags = 0;
            public short fileOffset = 0;
            public short fileExtension = 0;
            public string defExt = null;
            public IntPtr custData = IntPtr.Zero;
            public IntPtr hook = IntPtr.Zero;
            public string templateName = null;
            public IntPtr reservedPtr = IntPtr.Zero;
            public int reservedInt = 0;
            public int flagsEx = 0;
        }

        //public override void SaveFile(string defaultName, string extension, string title, Action<bool, string> callback)
        //{
        //    OpenFileName ofn = new OpenFileName();
        //    ofn.structSize = Marshal.SizeOf(ofn);
        //    ofn.filter = $"{extension.ToUpper()} Files\0*.{extension}\0All Files\0*.*\0";
        //    ofn.file = new string(new char[256]);
        //    ofn.maxFile = ofn.file.Length;
        //    ofn.fileTitle = new string(new char[64]);
        //    ofn.maxFileTitle = ofn.fileTitle.Length;
        //    ofn.initialDir = GetDocumentsPath();
        //    ofn.title = title;
        //    ofn.defExt = extension;
        //    ofn.flags = 0x00080000 | 0x00001000 | 0x00000800 | 0x00000200 | 0x00000008;

        //    if (GetSaveFileName(ofn))
        //    {
        //        callback(true, ofn.file);
        //    }
        //    else
        //    {
        //        callback(false, null);
        //    }
        //}

        //public virtual void SaveFileAt(string path, string defaultName, string extension, string title, Action<bool, string> callback)
        //{
        //    OpenFileName ofn = new OpenFileName();
        //    ofn.structSize = Marshal.SizeOf(ofn);
        //    ofn.filter = $"{extension.ToUpper()} Files\0*.{extension}\0All Files\0*.*\0";
        //    ofn.file = new string(new char[256]);
        //    ofn.maxFile = ofn.file.Length;
        //    ofn.fileTitle = new string(new char[64]);
        //    ofn.maxFileTitle = ofn.fileTitle.Length;
        //    ofn.initialDir = path;
        //    ofn.title = title;
        //    ofn.defExt = extension;
        //    ofn.flags = 0x00080000 | 0x00001000 | 0x00000800 | 0x00000200 | 0x00000008;

        //    if (GetSaveFileName(ofn))
        //    {
        //        callback(true, ofn.file);
        //    }
        //    else
        //    {
        //        callback(false, null);
        //    }
        //}

        public override async Task<bool> SaveFileAsync(string path, string extension, string title)
        {
            OpenFileName ofn = new OpenFileName();
            ofn.structSize = Marshal.SizeOf(ofn);
            ofn.filter = $"{extension.ToUpper()} Files\0*.{extension}\0All Files\0*.*\0";
            ofn.file = new string(new char[256]);
            ofn.maxFile = ofn.file.Length;
            ofn.fileTitle = title;
            ofn.maxFileTitle = ofn.fileTitle.Length;
            ofn.initialDir = path;
            ofn.title = title;
            ofn.defExt = extension;
            ofn.flags = 0x00080000 | 0x00001000 | 0x00000800 | 0x00000200 | 0x00000008;

            if (GetSaveFileName(ofn))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

#endif