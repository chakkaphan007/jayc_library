using System.Threading.Tasks;

namespace JayC.Utility.Filepicker
{
    public interface IFileSaver
    {
        Task<bool> SaveFileAsync(string path, string extension, string title);
    }
}