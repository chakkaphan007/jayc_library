using System;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;

namespace JayC.Utility.Filepicker
{
    public class GenericFileSaver : AbstractFileSaver
    {
        public override async Task<bool> SaveFileAsync(string path, string extension, string title)
        {
            try
            {
                File.WriteAllText(path, "GenericFileSaver Empty File"); // Create an empty file
                return true;
            }
            catch (Exception e)
            {
                Debug.LogError($"Error saving file: {e.Message}");
                return false;
            }
        }
    }
}