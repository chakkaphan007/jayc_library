#if UNITY_ANDROID
#if JayC_FilePicker

using System;
using UnityEngine;
using System.IO;
using System.Threading.Tasks;

namespace JayC.Utility.Filepicker
{
    public class AndroidFileSaver : AbstractFileSaver
    {
        public override async Task<bool> SaveFileAsync(string path, string extension, string title)
        {
            if (!extension.StartsWith(".")) extension = "." + extension;

            try
            {
                await RequestPermissionAsync();
                NativeFilePicker.Permission permission = NativeFilePicker.CheckPermission(false);

                if (permission == NativeFilePicker.Permission.Granted)
                {
                    if (!NativeFilePicker.IsFilePickerBusy())
                    {
                        //string fullPath = Path.Combine(path, title + extension);
                        string fullPath = Path.Combine(path);
                        Debug.Log(fullPath);
                        bool result = await ExportFileAsync(fullPath);

                        // Clean up the temporary file
                        if (File.Exists(fullPath))
                        {
                            File.Delete(fullPath);
                        }

                        return result;
                    }
                    else
                    {
                        Debug.LogWarning("File picker is busy.");
                        return false;
                    }
                }
                else if (permission == NativeFilePicker.Permission.Denied)
                {
                    Debug.LogWarning("Permission denied by user.");
                    return false;
                }
                else
                {
                    Debug.LogWarning("Permission permanently denied. Opening settings.");
                    NativeFilePicker.OpenSettings();
                    return false;
                }
            }
            catch (Exception ex)
            {
                Debug.LogError($"Error in SaveFileAsync: {ex.Message}");
                return false;
            }
        }

        private Task<NativeFilePicker.Permission> RequestPermissionAsync(bool readPermissionOnly = false)
        {
            return NativeFilePicker.RequestPermissionAsync(readPermissionOnly);
        }

        private Task<bool> ExportFileAsync(string fullPath)
        {
            var tcs = new TaskCompletionSource<bool>();

            NativeFilePicker.ExportFile(fullPath, (success) =>
            {
                if (success)
                {
                    Debug.Log($"File successfully exported: {fullPath}");
                    tcs.SetResult(true);
                }
                else
                {
                    Debug.LogError("Failed to export file");
                    tcs.SetResult(false);
                }
            });

            return tcs.Task;
        }
    }
}

#endif
#endif