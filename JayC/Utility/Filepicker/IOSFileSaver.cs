#if UNITY_IOS
#if JayC_FilePicker
using UnityEngine;
using System;
namespace JayC.Utility.Filepicker
{
public class IOSFileSaver : AbstractFileSaver
{
    public override void SaveFile(string defaultName, string extension, string title, Action<bool, string> callback)
    {
        if (!extension.StartsWith(".")) extension = "." + extension;
        string fileType = NativeFilePicker.ConvertExtensionToFileType(extension);

        NativeFilePicker.Permission permission = NativeFilePicker.RequestPermission(false);

        if (permission == NativeFilePicker.Permission.Granted && !NativeFilePicker.IsFilePickerBusy())
        {
            NativeFilePicker.ExportFile(GetDocumentsPath() + "/" + defaultName + extension, (success) =>
            {
                if (success)
                {
                    callback(true, GetDocumentsPath() + "/" + defaultName + extension);
                }
                else
                {
                    Debug.LogError("File save failed on iOS");
                    callback(false, null);
                }
            });
        }
        else
        {
            Debug.LogWarning("File save permission denied on iOS or picker is busy.");
            callback(false, null);
        }
    }
}
}
#endif
#endif