///-----------------------------------------------------------------
///   Author : JayC
///   Date   : 30/09/2024
///-----------------------------------------------------------------

#if JayC_FilePicker
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using UnityEngine;

namespace JayC.Utility.Filepicker
{
    public static class JsonToCsvConverter
    {
        /// <summary>
        /// Convert JSON string to CSV format and return the result as a string.
        /// </summary>
        /// <param name="jsonData">The JSON data to convert.</param>
        /// <returns>A string containing the CSV data.</returns>
        public static string ConvertJsonToCsv(string jsonData)
        {
            // Deserialize the JSON string into a list of dynamic objects
            var dataList = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(jsonData);

            if (dataList == null || dataList.Count == 0)
            {
                Debug.LogError("No data to convert");
                return string.Empty;
            }

            // Create a StringBuilder to build the CSV content
            StringBuilder csvBuilder = new StringBuilder();

            // Get the headers from the first dictionary (since all should share the same keys)
            var headers = dataList[0].Keys;
            csvBuilder.AppendLine(string.Join(",", headers));

            // Loop through each dictionary (row) and append the values
            foreach (var row in dataList)
            {
                List<string> values = new List<string>();

                foreach (var header in headers)
                {
                    // Ensure that we handle null values to avoid errors
                    if (row.ContainsKey(header) && row[header] != null)
                    {
                        // Escape any commas or quotes in the data for proper CSV formatting
                        var value = row[header].ToString().Replace("\"", "\"\"");
                        values.Add($"\"{value}\"");
                    }
                    else
                    {
                        values.Add(""); // Empty value
                    }
                }

                csvBuilder.AppendLine(string.Join(",", values));
            }

            return csvBuilder.ToString();
        }

        /// <summary>
        /// Save CSV data to a file.
        /// </summary>
        /// <param name="csvData">The CSV data to save.</param>
        /// <param name="fileName">The name of the file.</param>
        public static void SaveCsvToFile(string csvData, string fileName)
        {
            // Get the file path (use persistentDataPath for mobile)
            string filePath = Path.Combine(Application.persistentDataPath, fileName);

            // Write the CSV data to the file
            File.WriteAllText(filePath, csvData);

            Debug.Log($"CSV file saved at: {filePath}");
        }
    }
}
#endif