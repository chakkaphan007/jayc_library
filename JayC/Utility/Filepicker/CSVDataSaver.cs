///-----------------------------------------------------------------
///   Author : JayC
///   Date   : 30/09/2024
///-----------------------------------------------------------------

#if JayC_FilePicker

using System;
using System.IO;
using System.Text;
using UnityEngine;

namespace JayC.Utility.Filepicker
{
    public static class CSVDataSaver
    {
        /// <summary>
        /// Appends a single row of data to the CSV file.
        /// </summary>
        /// <param name="filePath">The path of the CSV file.</param>
        /// <param name="data">Array of string data to be appended as a new row.</param>
        public static void AppendToCSV(string filePath, string[] data)
        {
            try
            {
                // Check if the file exists; if not, create it and add header row
                if (!File.Exists(filePath))
                {
                    File.WriteAllText(filePath, "Column1,Column2,Column3\n"); // Adjust your headers as needed
                }

                // Create a CSV formatted string for the new data row
                StringBuilder csvLine = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                {
                    csvLine.Append(data[i]);

                    // Append comma for all but the last value
                    if (i < data.Length - 1)
                    {
                        csvLine.Append(",");
                    }
                }

                // Append the new line to the file
                using (StreamWriter sw = new StreamWriter(filePath, true)) // true for appending
                {
                    sw.WriteLine(csvLine.ToString());
                }

                Debug.Log("Data appended to CSV file successfully.");
            }
            catch (Exception e)
            {
                Debug.LogError($"Error appending data to CSV: {e.Message}");
            }
        }

        /// <summary>
        /// Appends a list of exportable data to the CSV file.
        /// </summary>
        /// <param name="filePath">The path of the CSV file.</param>
        /// <param name="dataList">List of exportable data.</param>
        public static void AppendToCSV<T>(string filePath, System.Collections.Generic.List<T> dataList) where T : FileExportManager.ExportableData
        {
            try
            {
                if (dataList.Count > 0)
                {
                    StringBuilder csvContent = new StringBuilder();
                    if (!File.Exists(filePath))
                    {
                        // Add headers if the file is new
                        csvContent.AppendLine(string.Join(",", dataList[0].GetCSVHeaders()));
                    }

                    // Add rows of data
                    foreach (T data in dataList)
                    {
                        csvContent.AppendLine(string.Join(",", data.ToCSV()));
                    }

                    File.AppendAllText(filePath, csvContent.ToString());
                    Debug.Log("Data appended to CSV file successfully.");
                }
                else
                {
                    Debug.LogWarning("No data to export.");
                }
            }
            catch (Exception e)
            {
                Debug.LogError($"Error appending data to CSV: {e.Message}");
            }
        }

        /// <summary>
        /// Reads all the data from the CSV file.
        /// </summary>
        /// <param name="filePath">The path of the CSV file.</param>
        /// <returns>A string containing the entire CSV content.</returns>
        public static string ReadCSV(string filePath)
        {
            try
            {
                if (File.Exists(filePath))
                {
                    return File.ReadAllText(filePath);
                }
                else
                {
                    Debug.LogWarning("CSV file not found.");
                    return string.Empty;
                }
            }
            catch (Exception e)
            {
                Debug.LogError($"Error reading CSV: {e.Message}");
                return string.Empty;
            }
        }

        /// <summary>
        /// Deletes the CSV file if it exists.
        /// </summary>
        /// <param name="filePath">The path of the CSV file.</param>
        public static void DeleteCSV(string filePath)
        {
            try
            {
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                    Debug.Log("CSV file deleted.");
                }
                else
                {
                    Debug.LogWarning("CSV file does not exist.");
                }
            }
            catch (Exception e)
            {
                Debug.LogError($"Error deleting CSV: {e.Message}");
            }
        }
    }
}

#endif