using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JayC.Analytics
{
    /// <summary>
    /// Sends analytics data to the server with retry and backoff logic.
    /// </summary>
    public class EventSender : MonoBehaviour
    {
        public static EventSender Instance { get; private set; }

        [Header("Retry Settings")]
        [SerializeField, Tooltip("Maximum number of retry attempts.")]
        private int maxRetries = 3;

        [SerializeField, Tooltip("Initial delay before retrying (in seconds).")]
        private float retryDelay = 2f;

        [SerializeField, Tooltip("Multiplier for retry delay (exponential backoff).")]
        private float backoffMultiplier = 2f;

        [SerializeField, Tooltip("Simulate sending events (for debugging).")]
        private bool simulateSending = true;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        /// <summary>
        /// Sends a batch of events to the server with retry logic.
        /// </summary>
        /// <param name="eventBatch">List of event logs to send.</param>
        public void SendEventBatch(List<EventLogger.EventLog> eventBatch)
        {
            StartCoroutine(SendWithRetry(eventBatch));
        }

        private IEnumerator SendWithRetry(List<EventLogger.EventLog> eventBatch)
        {
            int attempt = 0;
            float delay = retryDelay;

            while (attempt < maxRetries)
            {
                if (simulateSending)
                {
                    Debug.Log($"Simulating server request: Attempt {attempt + 1}");
                    yield return SimulateSendToServer(eventBatch);
                }
                else
                {
                    bool success = SendToServer(eventBatch);
                    if (success)
                    {
                        Debug.Log($"Batch sent successfully on attempt {attempt + 1}");
                        yield break;
                    }
                }

                attempt++;
                Debug.LogWarning($"Attempt {attempt} failed. Retrying in {delay} seconds...");
                yield return new WaitForSeconds(delay);
                delay *= backoffMultiplier;
            }

            Debug.LogError("Failed to send batch after maximum retries.");
        }

        private bool SendToServer(List<EventLogger.EventLog> eventBatch)
        {
            // Implement actual server communication logic
            Debug.LogWarning("SendToServer not implemented.");
            return false;
        }

        private IEnumerator SimulateSendToServer(List<EventLogger.EventLog> eventBatch)
        {
            yield return new WaitForSeconds(1); // Simulate network delay
            Debug.Log($"Simulated batch sent: {eventBatch.Count} events.");
        }
    }
}
