using System;
using TMPro;
using UnityEngine;

namespace JayC.Utility
{
    public enum TimerMode
    {
        Countdown,
        Stopwatch
    }
    public class JTimer : MonoBehaviour
    {
        public TimerMode timerMode = TimerMode.Countdown;
        public float _countdownTime = 60.0f;
        public TMP_Text _timerText;

        protected float _currentTime;
        protected bool _isPause = false;
        protected virtual void Start()
        {
            ResetTimer();
        }
    
        protected virtual void StartTimer()
        {
            ResetTimer();
            _isPause = false;
        }
    
    
        protected virtual void PauseTimer()
        {
            _isPause = true;
        }
    
        protected virtual void FixedUpdate()
        {
            if (_isPause)
                return;
            switch (timerMode)
            {
                case TimerMode.Countdown when _currentTime > 0:
                    _currentTime -= Time.deltaTime;
                    UpdateTimerText();
                    break;
                case TimerMode.Stopwatch:
                    _currentTime += Time.deltaTime;
                    UpdateTimerText();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    
        protected virtual void SwitchMode(TimerMode newMode)
        {
            timerMode = newMode;
            ResetTimer();
        }
    
        protected virtual void ResetTimer()
        {
            if (timerMode == TimerMode.Countdown)
            {
                _currentTime = _countdownTime;
            }
            else
            {
                _currentTime = 0;
            }
            _isPause = true;
            UpdateTimerText();
        }

        protected virtual void UpdateTimerText()
        {
            _timerText.text = timerMode == TimerMode.Countdown ? 
                "Time: " + _currentTime.ToString("F2") : "Stopwatch: " + _currentTime.ToString("F2");
        }
    }
}