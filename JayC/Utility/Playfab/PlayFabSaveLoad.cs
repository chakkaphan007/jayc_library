#if JayC_Playfab
using PlayFab;
using System;
using UnityEngine;

namespace JayC.Utility.Playfab
{
    public class PlayFabSaveLoad<T> where T : new()
    {
        public static PlayfabSaveSystem<T> playfabSaveSystem;

        static PlayFabSaveLoad()
        {
            playfabSaveSystem = new PlayfabSaveSystem<T>();
        }

        public static void SavePlayerData(T playerData, Action<T> onSuccess, Action<PlayFabError> onFailure, string PlayerData)
        {
            playfabSaveSystem.SavePlayerData(playerData, onSuccess, onFailure, PlayerData);
        }

        public static void LoadPlayerData(Action<T> onSuccess, Action<PlayFabError> onFailure, string PlayerData)
        {
            playfabSaveSystem.LoadPlayerData(onSuccess, onFailure, PlayerData);
        }
    }
}

#endif