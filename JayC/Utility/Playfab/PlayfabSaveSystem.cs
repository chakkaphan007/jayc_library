#if JayC_Playfab
using PlayFab.ClientModels;
using PlayFab;
using System;
using UnityEngine;

namespace JayC.Utility.Playfab
{
    public class PlayfabSaveSystem<T> where T : new()
    {
        public void SavePlayerData(T data, Action<T> onSuccess, Action<PlayFabError> onFailure, string PlayerData)
        {
            string serializedData = JsonUtility.ToJson(data);

            var request = new UpdateUserDataRequest
            {
                Data = new System.Collections.Generic.Dictionary<string, string>
            {
                { PlayerData, serializedData }
            }
            };

            PlayFabClientAPI.UpdateUserData(request, result =>
            {
                onSuccess?.Invoke(data);
            }, error =>
            {
                onFailure?.Invoke(error);
            });
        }

        public void LoadPlayerData(Action<T> onSuccess, Action<PlayFabError> onFailure, string PlayerData)
        {
            PlayFabClientAPI.GetUserData(new GetUserDataRequest(), result =>
            {
                if (result.Data.TryGetValue(PlayerData, out var userData))
                {
                    T loadedData = JsonUtility.FromJson<T>(userData.Value);
                    onSuccess?.Invoke(loadedData);
                }
                else
                {
                    onSuccess?.Invoke(default); // Return default value if no data found
                }
            }, error =>
            {
                onFailure?.Invoke(error);
            });
        }
    }
}
#endif