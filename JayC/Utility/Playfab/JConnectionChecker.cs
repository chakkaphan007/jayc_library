using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace JayC.Utility.Playfab
{
    public class JConnectionChecker : MonoBehaviour
    {
        public bool wasConnected = false; // Previous connection status
        public static bool isConnected = false;

        public UnityEvent OnInternetDisconnected;
        public UnityEvent OnInternetConnected;

        protected void Awake()
        {
            StartCoroutine(CheckInternetConnection());
        }

        public virtual IEnumerator CheckInternetConnection()
        {
            while (true)
            {
                bool currentlyConnected = Application.internetReachability != NetworkReachability.NotReachable;

                if (currentlyConnected != wasConnected)
                {
                    if (currentlyConnected)
                    {
                        OnInternetConnected.Invoke();
                    }
                    else
                    {
                        OnInternetDisconnected.Invoke();
                    }

                    wasConnected = currentlyConnected;
                }

                isConnected = currentlyConnected;

                yield return new WaitForSeconds(2.0f);
            }
        }
    }
}