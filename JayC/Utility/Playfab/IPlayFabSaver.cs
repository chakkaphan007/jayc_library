#if JayC_Playfab
using PlayFab;
using System;

namespace JayC.Utility.Playfab
{
    public interface IPlayfabSaver<T>
    {
        void OnSaveSuccess(T data);

        void OnSaveFailure(PlayFabError error);

        void OnLoadSuccess(T data);

        void OnLoadFailure(PlayFabError error);
    }
}

#endif