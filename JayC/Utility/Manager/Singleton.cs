///-----------------------------------------------------------------
///   Author : JayC
///   Date   : 16/10/2024
///-----------------------------------------------------------------
using UnityEngine;

namespace JayC.Utility
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    // Try to find the instance in the scene
                    _instance = FindAnyObjectByType<T>();

                    if (_instance == null)
                    {
                        // If not found, create a new GameObject and add the singleton component
                        GameObject singletonObj = new GameObject(typeof(T).Name);
                        _instance = singletonObj.AddComponent<T>();

                        // Optional: Keep the instance between scene loads
                        DontDestroyOnLoad(singletonObj);

                        Debug.Log($"{typeof(T).Name} was created automatically in the scene.");
                    }
                }

                return _instance;
            }
        }

        // Ensure the singleton instance is assigned correctly during Awake
        protected virtual void Awake()
        {
            if (_instance == null)
            {
                _instance = this as T;
                DontDestroyOnLoad(gameObject); // Keep this singleton between scene loads if needed
            }
            else if (_instance != this)
            {
                // Destroy this instance if another instance already exists
                Destroy(gameObject);
            }
        }
    }
}