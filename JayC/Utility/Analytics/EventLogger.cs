using System;
using System.Collections.Generic;
using UnityEngine;

namespace JayC.Analytics
{
    /// <summary>
    /// Logs and queues analytics events with a retry mechanism.
    /// </summary>
    public class EventLogger : MonoBehaviour
    {
        public static EventLogger Instance { get; private set; }

        [Header("Logger Settings")]
        [SerializeField, Tooltip("Time interval for batching events (in seconds).")]
        private float logBatchInterval = 10f;

        [SerializeField, Tooltip("Maximum size of a batch.")]
        private int maxBatchSize = 50;

        [SerializeField, Tooltip("Enable debug logging.")]
        private bool enableDebugLogs = true;

        private readonly Queue<EventLog> eventQueue = new Queue<EventLog>();
        private float nextSendTime;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Update()
        {
            if (Time.time >= nextSendTime && eventQueue.Count > 0)
            {
                SendEventBatch();
                nextSendTime = Time.time + logBatchInterval;
            }
        }

        public void LogEvent(string eventName, Dictionary<string, object> eventData = null)
        {
            var newEvent = new EventLog
            {
                EventName = eventName,
                EventData = eventData ?? new Dictionary<string, object>(),
                Timestamp = DateTime.UtcNow
            };

            eventQueue.Enqueue(newEvent);

            if (enableDebugLogs)
            {
                Debug.Log($"Event Logged: {newEvent.EventName} at {newEvent.Timestamp}");
            }

            if (eventQueue.Count >= maxBatchSize)
            {
                SendEventBatch();
            }
        }

        private void SendEventBatch()
        {
            if (eventQueue.Count == 0) return;

            var eventBatch = new List<EventLog>();
            while (eventQueue.Count > 0 && eventBatch.Count < maxBatchSize)
            {
                eventBatch.Add(eventQueue.Dequeue());
            }

            EventSender.Instance.SendEventBatch(eventBatch);
        }

        public void ClearEvents()
        {
            eventQueue.Clear();
            if (enableDebugLogs)
            {
                Debug.Log("Event queue cleared.");
            }
        }

        [Serializable]
        public class EventLog
        {
            public string EventName;
            public Dictionary<string, object> EventData;
            public DateTime Timestamp;
        }
    }
}
