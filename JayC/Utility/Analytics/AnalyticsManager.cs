using System.Collections.Generic;
using UnityEngine;

namespace JayC.Analytics
{
    public static class AnalyticsManager
    {
        public static void LogEvent(string eventName, Dictionary<string, object> eventData = null)
        {
            EventLogger.Instance.LogEvent(eventName, eventData);
        }

        public static void ClearAllEvents()
        {
            EventLogger.Instance.ClearEvents();
        }
    }
}