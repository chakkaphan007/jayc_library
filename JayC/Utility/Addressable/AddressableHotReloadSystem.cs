#if JayC_Addressable
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace JayC.Utility.Addressable
{
    public static class AddressableHotReloadSystem
    {
        public static void CheckForUpdates(System.Action<bool> onComplete)
        {
            Addressables.CheckForCatalogUpdates().Completed += (checkOperation) =>
            {
                bool hasUpdates = checkOperation.Result.Count > 0;
                onComplete?.Invoke(hasUpdates);
            };
        }

        public static void DownloadUpdates(System.Action onComplete, System.Action onFail = null)
        {
            Addressables.UpdateCatalogs().Completed += (updateOperation) =>
            {
                if (updateOperation.Status == AsyncOperationStatus.Succeeded)
                {
                    onComplete?.Invoke();
                }
                else
                {
                    onFail?.Invoke();
                    Debug.LogError("Failed to update Addressables.");
                }
            };
        }
    }
}
#endif