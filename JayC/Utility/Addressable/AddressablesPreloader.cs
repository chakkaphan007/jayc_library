#if JayC_Addressable
using UnityEngine;
using UnityEngine.AddressableAssets;
using System.Collections.Generic;

namespace JayC.Utility.Addressable
{
    public static class AddressablesPreloader
    {
        public static void PreloadAssets(List<string> assetKeys, System.Action onComplete)
        {
            int loadedCount = 0;

            foreach (string key in assetKeys)
            {
                Addressables.LoadAssetAsync<object>(key).Completed += (operation) =>
                {
                    loadedCount++;
                    if (loadedCount == assetKeys.Count)
                    {
                        onComplete?.Invoke();
                    }
                };
            }
        }

        public static void ReleasePreloadedAssets(List<string> assetKeys)
        {
            foreach (string key in assetKeys)
            {
                Addressables.Release(key);
            }
            Debug.Log("Preloaded assets released.");
        }
    }
}
#endif