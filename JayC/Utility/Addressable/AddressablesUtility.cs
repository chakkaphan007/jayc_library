﻿#if JayC_Addressable
using System;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace JayC.Utility.Addressable
{
    /// <summary>
    /// A utility class for various Addressables functionality
    /// </summary>
    public static class AddressablesUtility
    {
        /// <summary>
        /// Get the address of a given AssetReference.
        /// </summary>
        /// <param name="reference">The AssetReference you want to find the address of.</param>
        /// <returns>The address of a given AssetReference.</returns>
        public static string GetAddressFromAssetReference(AssetReference reference)
        {
            var loadResourceLocations = Addressables.LoadResourceLocationsAsync(reference);
            var result = loadResourceLocations.WaitForCompletion();
            if (result.Count > 0)
            {
                string key = result[0].PrimaryKey;
                Addressables.Release(loadResourceLocations);
                return key;
            }

            Addressables.Release(loadResourceLocations);
            return string.Empty;
        }

        /// <summary>
        /// Load an asset using AssetReference.
        /// </summary>
        public static void LoadAsset(AssetReference assetReference, Action<object> onComplete, Action onFail = null)
        {
            AsyncOperationHandle<object> handle = assetReference.LoadAssetAsync<object>();
            handle.Completed += (operation) =>
            {
                if (operation.Status == AsyncOperationStatus.Succeeded)
                {
                    onComplete?.Invoke(operation.Result);
                }
                else
                {
                    onFail?.Invoke();
                    Debug.LogError("Failed to load asset: " + operation.OperationException);
                }
            };
        }

        /// <summary>
        /// Unload an asset using AssetReference.
        /// </summary>
        public static void UnloadAsset(AssetReference loadedAsset, Action onComplete, Action onFail = null)
        {
            AsyncOperationHandle<object> handle = loadedAsset.LoadAssetAsync<object>();
            handle.Completed += (operation) =>
            {
                if (operation.Status == AsyncOperationStatus.Succeeded)
                {
                    Addressables.Release(handle);
                    onComplete?.Invoke();
                }
                else
                {
                    onFail?.Invoke();
                    Debug.LogError("Failed to unload asset: " + operation.OperationException);
                }
            };
        }

        /// <summary>
        /// Destroy a GameObject and release the Addressable instance.
        /// </summary>
        public static void DestroyGameObject(GameObject loadedAsset, Action onComplete, Action onFail = null)
        {
            if (loadedAsset == null)
            {
                Debug.LogError("Attempted to destroy a null object.");
                onFail?.Invoke();
                return;
            }

            // Destroy the GameObject
            GameObject.Destroy(loadedAsset);

            // Release the addressable instance
            bool releaseSuccess = Addressables.ReleaseInstance(loadedAsset);

            if (releaseSuccess)
            {
                onComplete?.Invoke();
                Debug.Log("GameObject destroyed and released successfully.");
            }
            else
            {
                onFail?.Invoke();
                Debug.LogError("Failed to release the asset.");
            }
        }

        /// <summary>
        /// Load a GameObject from an AssetReference.
        /// </summary>
        public static void LoadGameObject(AssetReference objectReference, Transform parent, Action<GameObject> onComplete, Action onFail = null)
        {
            AsyncOperationHandle<GameObject> handle = Addressables.InstantiateAsync(objectReference, parent);
            handle.Completed += (operation) =>
            {
                if (operation.Status == AsyncOperationStatus.Succeeded)
                {
                    onComplete?.Invoke(operation.Result);
                    Debug.Log("GameObject instantiated successfully.");
                }
                else
                {
                    onFail?.Invoke();
                    Debug.LogError("Failed to instantiate GameObject: " + operation.OperationException);
                }
            };
        }
    }
}
#endif