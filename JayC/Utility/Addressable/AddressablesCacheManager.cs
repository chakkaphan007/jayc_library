#if JayC_Addressable
using UnityEngine;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.AddressableAssets;

namespace JayC.Utility.Addressable
{
    public static class AddressablesCacheManager
    {
        public static void ClearCache()
        {
            Caching.ClearCache();
            Debug.Log("Cache Cleared");
        }

        public static bool IsCached(string key)
        {
            return Caching.IsVersionCached(key, new Hash128());
        }

        public static void RemoveCachedAsset(string key)
        {
            Caching.ClearCachedVersion(key, new Hash128());
            Debug.Log($"Cache for asset {key} cleared.");
        }

        public static long GetCacheSize()
        {
            return Caching.defaultCache.spaceOccupied;
        }
    }
}
#endif