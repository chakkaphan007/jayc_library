#if JayC_Addressable
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using System.Collections.Generic;
using UnityEngine.Audio;

namespace JayC.Utility.Addressable
{
    public static class AddressableAudioManager
    {
        private static Dictionary<string, AudioClip> audioClipCache = new Dictionary<string, AudioClip>();

        public static void LoadAudio(string address, System.Action<AudioClip> onComplete, System.Action onFail = null)
        {
            if (audioClipCache.ContainsKey(address))
            {
                onComplete?.Invoke(audioClipCache[address]);
                return;
            }

            Addressables.LoadAssetAsync<AudioClip>(address).Completed += (operation) =>
            {
                if (operation.Status == AsyncOperationStatus.Succeeded)
                {
                    AudioClip clip = operation.Result;
                    audioClipCache[address] = clip;
                    onComplete?.Invoke(clip);
                }
                else
                {
                    Debug.LogError($"Failed to load audio: {address}");
                    onFail?.Invoke();
                }
            };
        }
    }
}
#endif