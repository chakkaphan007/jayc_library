#if JayC_Addressable
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace JayC.Utility.Addressable
{
    /// <summary>
    /// A manager class to handle loading and unloading scenes via Addressables.
    /// </summary>
    public static class AddressableSceneManager
    {
        public static Dictionary<string, AsyncOperationHandle<SceneInstance>> loadedScenes = new Dictionary<string, AsyncOperationHandle<SceneInstance>>();

        public static AsyncOperationHandle<SceneInstance> LoadSceneAsync(string sceneKey, LoadSceneMode loadMode, Action onComplete = null, Action onFail = null)
        {
            if (string.IsNullOrEmpty(sceneKey))
            {
                Debug.LogError("Scene key is null or empty.");
                throw new ArgumentException("Scene key cannot be null or empty.");
            }

            if (loadedScenes.ContainsKey(sceneKey))
            {
                Debug.LogWarning($"Scene '{sceneKey}' is already loaded.");
                onComplete?.Invoke();
                return default;
            }

            var handle = Addressables.LoadSceneAsync(sceneKey, loadMode);
            handle.Completed += operation =>
            {
                if (operation.Status == AsyncOperationStatus.Succeeded)
                {
                    loadedScenes[sceneKey] = handle;
                    onComplete?.Invoke();
                    Debug.Log($"Scene '{sceneKey}' loaded successfully.");
                }
                else
                {
                    Debug.LogError($"Failed to load scene '{sceneKey}': {operation.OperationException}");
                    onFail?.Invoke();
                }
            };

            return handle;
        }

        public static void LoadScene(string sceneKey, LoadSceneMode loadMode = LoadSceneMode.Single, Action onComplete = null, Action onFail = null)
        {
            if (loadedScenes.ContainsKey(sceneKey))
            {
                Debug.LogWarning($"Scene '{sceneKey}' is already loaded.");
                onComplete?.Invoke();
                return;
            }

            LoadSceneAsync(sceneKey, loadMode, onComplete, onFail);
        }

        public static void LoadScene(AssetReference sceneReference, LoadSceneMode loadMode = LoadSceneMode.Single, Action onComplete = null, Action onFail = null)
        {
            string sceneKey = sceneReference.RuntimeKey.ToString();
            LoadScene(sceneKey, loadMode, onComplete, onFail);
        }

        public static void UnloadScene(string sceneKey, Action onComplete = null, Action onFail = null)
        {
            if (!loadedScenes.TryGetValue(sceneKey, out var handle))
            {
                Debug.LogWarning($"Scene '{sceneKey}' is not loaded.");
                onFail?.Invoke();
                return;
            }

            var unloadHandle = Addressables.UnloadSceneAsync(handle);
            unloadHandle.Completed += operation =>
            {
                if (operation.Status == AsyncOperationStatus.Succeeded)
                {
                    loadedScenes.Remove(sceneKey);
                    onComplete?.Invoke();
                    Debug.Log($"Scene '{sceneKey}' unloaded successfully.");
                }
                else
                {
                    Debug.LogError($"Failed to unload scene '{sceneKey}': {operation.OperationException}");
                    onFail?.Invoke();
                }
            };
        }

        public static void UnloadScene(AssetReference sceneReference, Action onComplete = null, Action onFail = null)
        {
            string sceneKey = sceneReference.RuntimeKey.ToString();
            UnloadScene(sceneKey, onComplete, onFail);
        }

        public static bool IsSceneLoaded(string sceneKey)
        {
            return loadedScenes.ContainsKey(sceneKey);
        }

        public static bool IsSceneLoaded(AssetReference sceneReference)
        {
            string sceneKey = sceneReference.RuntimeKey.ToString();
            return IsSceneLoaded(sceneKey);
        }

        public static void UnloadAllScenes(Action onComplete = null)
        {
            var keys = new List<string>(loadedScenes.Keys);
            if (keys.Count == 0)
            {
                onComplete?.Invoke();
                return;
            }

            int unloadedCount = 0;
            foreach (var sceneKey in keys)
            {
                if (!loadedScenes.TryGetValue(sceneKey, out var handle))
                {
                    Debug.LogWarning($"Scene '{sceneKey}' is not loaded.");
                    continue;
                }

                var unloadHandle = Addressables.UnloadSceneAsync(handle);
                unloadHandle.Completed += operation =>
                {
                    if (operation.Status == AsyncOperationStatus.Succeeded)
                    {
                        loadedScenes.Remove(sceneKey);
                        unloadedCount++;
                        if (unloadedCount == keys.Count)
                        {
                            onComplete?.Invoke();
                            Debug.Log("All scenes unloaded successfully.");
                        }
                    }
                    else
                    {
                        Debug.LogError($"Failed to unload scene '{sceneKey}': {operation.OperationException}");
                    }
                };
            }
        }
    }
}
#endif