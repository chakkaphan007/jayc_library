using UnityEngine;
using System.Collections.Generic;
using JayC;
using JayC.Utility.RayCast;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace JayC.Utility
{
    public class JDistanceComparer : MonoBehaviour
    {
        public JRaycastShape raycastShape; // The specific raycast shape to use.
        public List<Transform> triggerObjects = new List<Transform>(); // List of trigger objects within range.

        public Transform mostNearbyObject; // Reference to the most nearby object.
        public Transform mostDistantObject; // Reference to the most distant object.

        protected virtual void Update()
        {
            // Ensure the raycast shape has a reference point.
            if (raycastShape == null || raycastShape.referencePoint == null)
                return;

            // Find objects within the specified shape.
            triggerObjects = new List<Transform>(raycastShape.FindObjectsInShape());

            // Find the most nearby and most distant objects.
            FindMostNearbyAndMostDistant();
        }
#if UNITY_EDITOR
        protected virtual void OnDrawGizmos()
        {
            // Ensure the raycast shape has a reference point.
            if (raycastShape == null || raycastShape.referencePoint == null)
                return;

            // Draw Gizmos for the specified shape.
            raycastShape.OnDrawGizmos();

            foreach (Transform triggerObject in triggerObjects)
            {
                if (triggerObject == null)
                    continue;

                // Calculate the direction from the reference point to the trigger object.
                Vector3 direction = triggerObject.position - raycastShape.referencePoint.position;

                // Calculate the distance between the reference point and the trigger object.
                float distance = direction.magnitude;

                // Draw a Gizmo line from the reference point to the trigger object.
                Gizmos.color = Color.red; // You can choose any color you prefer.
                Gizmos.DrawLine(raycastShape.referencePoint.position, triggerObject.position);

                // Display the distance as text near the trigger object.
                GUIStyle style = new GUIStyle();
                style.normal.textColor = Color.red; // You can choose any text color you prefer.
                Handles.Label(triggerObject.position, "Distance: " + distance.ToString("F2"), style);
            }

            // Draw additional Gizmos to highlight the most nearby and most distant objects.
            if (mostNearbyObject != null)
            {
                Gizmos.color = Color.green; // You can choose any color you prefer.
                Gizmos.DrawLine(raycastShape.referencePoint.position, mostNearbyObject.position);
            }

            if (mostDistantObject != null)
            {
                Gizmos.color = Color.yellow; // You can choose any color you prefer.
                Gizmos.DrawLine(raycastShape.referencePoint.position, mostDistantObject.position);
            }
        }
#endif

        protected virtual void FindMostNearbyAndMostDistant()
        {
            float closestDistance = float.MaxValue;
            float farthestDistance = float.MinValue;

            foreach (Transform triggerObject in triggerObjects)
            {
                if (triggerObject == null)
                    continue;

                // Calculate the direction from the reference point to the trigger object.
                Vector3 direction = triggerObject.position - raycastShape.referencePoint.position;

                // Calculate the distance between the reference point and the trigger object.
                float distance = direction.magnitude;

                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    mostNearbyObject = triggerObject;
                }

                if (distance > farthestDistance)
                {
                    farthestDistance = distance;
                    mostDistantObject = triggerObject;
                }
            }
        }
    }
}
