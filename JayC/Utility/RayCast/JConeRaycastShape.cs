using UnityEngine;

namespace JayC.Utility.RayCast
{
    public class JConeRaycastShape : JRaycastShape
    {
        [Range(0.1f, 10f)] public float coneRadius = 1f;
        [Range(1f, 360f)] public float coneAngle = 45f;

        protected override bool CheckIntersection(Vector3 objectPosition)
        {
            Vector3 directionToObject = objectPosition - referencePoint.position;
            float distance = directionToObject.magnitude;

            // Check if the object is within the maximum raycast distance.
            if (distance > maxRaycastDistance)
                return false;

            // Calculate the angle between the direction to the object and the cone's forward vector.
            float angle = Vector3.Angle(referencePoint.forward, directionToObject);

            // Check if the object is outside the cone's angle or distance.
            return angle > coneAngle / 2f || distance > maxRaycastDistance ||
                   distance * Mathf.Tan(Mathf.Deg2Rad * (coneAngle / 2f)) > coneRadius;
        }

        protected override void DrawGizmoShape()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawRay(referencePoint.position, referencePoint.forward * maxRaycastDistance);

            // Draw the inverted cone shape using Gizmos.
            float halfAngle = coneAngle / 2f;
            float halfHeight = maxRaycastDistance * Mathf.Tan(Mathf.Deg2Rad * halfAngle);
            Vector3 apex = referencePoint.position + referencePoint.forward * maxRaycastDistance;
            Vector3 left = Quaternion.Euler(0, -halfAngle, 0) * referencePoint.forward * coneRadius;
            Vector3 right = Quaternion.Euler(0, halfAngle, 0) * referencePoint.forward * coneRadius;

            Gizmos.DrawLine(apex, apex - referencePoint.up * halfHeight);
            Gizmos.DrawLine(apex, apex + left);
            Gizmos.DrawLine(apex, apex + right);
        }

        protected internal override Transform[] FindObjectsInShape()
        {
            Collider[] colliders = Physics.OverlapCapsule(referencePoint.position,
                referencePoint.position + referencePoint.forward * maxRaycastDistance, coneRadius, objectLayer);

            Transform[] objectsInShape = new Transform[colliders.Length];
            int count = 0;

            foreach (Collider collider in colliders)
            {
                if (collider == null)
                    continue;

                Transform objectTransform = collider.transform;

                if (!CheckIntersection(objectTransform.position))
                {
                    objectsInShape[count] = objectTransform;
                    count++;
                }
            }

            return objectsInShape;
        }
    }
}
