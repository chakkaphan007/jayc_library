#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace JayC.Utility.RayCast
{
    public abstract class JRaycastShape : MonoBehaviour
    {
        public Transform referencePoint; // The reference point, e.g., the player's position.
        public float maxRaycastDistance = 10f; // Maximum distance for the raycast.
        public LayerMask objectLayer;

        protected abstract bool CheckIntersection(Vector3 objectPosition);
#if UNITY_EDITOR
        protected internal virtual void OnDrawGizmos()
        {
            if (referencePoint == null)
                return;

            // Draw a Gizmo shape to represent the raycast shape.
            DrawGizmoShape();

            foreach (Transform triggerObject in FindObjectsInShape())
            {
                if (triggerObject == null)
                    continue;

                // Calculate the direction from the reference point to the trigger object.
                Vector3 direction = triggerObject.position - referencePoint.position;

                // Calculate the distance between the reference point and the trigger object.
                float distance = direction.magnitude;

                // Draw a Gizmo line from the reference point to the trigger object.
                Gizmos.color = Color.red; // You can choose any color you prefer.
                Gizmos.DrawLine(referencePoint.position, triggerObject.position);

                // Display the distance as text near the trigger object.
                GUIStyle style = new GUIStyle();
                style.normal.textColor = Color.red; // You can choose any text color you prefer.
                Handles.Label(triggerObject.position, "Distance: " + distance.ToString("F2"), style);
            }
        }
#endif
        protected abstract void DrawGizmoShape();

        protected internal abstract Transform[] FindObjectsInShape();
    }
}
