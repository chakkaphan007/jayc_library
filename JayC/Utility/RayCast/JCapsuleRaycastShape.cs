using UnityEngine;

namespace JayC.Utility.RayCast
{
    public class JCapsuleRaycastShape : JRaycastShape
    {
        [Range(0.1f, 10f)] public float capsuleRadius = 1f;

        protected override bool CheckIntersection(Vector3 objectPosition)
        {
            float distance = Vector3.Distance(referencePoint.position, objectPosition);

            // Check if the object is within the maximum raycast distance.
            if (distance > maxRaycastDistance)
                return false;

            // Calculate the closest point on the capsule's line segment to the object's position.
            Vector3 closestPoint = ClosestPointOnCapsule(referencePoint.position,
                referencePoint.position + referencePoint.forward * maxRaycastDistance, objectPosition);

            // Check if the closest point is within the capsule's radius.
            return Vector3.Distance(objectPosition, closestPoint) <= capsuleRadius;
        }

        protected override void DrawGizmoShape()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(referencePoint.position, capsuleRadius);

            // Draw the capsule shape using Gizmos.
            Vector3 start = referencePoint.position;
            Vector3 end = referencePoint.position + referencePoint.forward * maxRaycastDistance;
            Gizmos.DrawLine(start + referencePoint.up * capsuleRadius, end + referencePoint.up * capsuleRadius);
            Gizmos.DrawLine(start - referencePoint.up * capsuleRadius, end - referencePoint.up * capsuleRadius);
        }

        protected internal override Transform[] FindObjectsInShape()
        {
            Collider[] colliders = Physics.OverlapCapsule(referencePoint.position,
                referencePoint.position + referencePoint.forward * maxRaycastDistance, capsuleRadius, objectLayer);

            Transform[] objectsInShape = new Transform[colliders.Length];
            int count = 0;

            foreach (Collider collider in colliders)
            {
                if (collider == null)
                    continue;

                Transform objectTransform = collider.transform;

                if (CheckIntersection(objectTransform.position))
                {
                    objectsInShape[count] = objectTransform;
                    count++;
                }
            }

            return objectsInShape;
        }

        private Vector3 ClosestPointOnCapsule(Vector3 capsuleStart, Vector3 capsuleEnd, Vector3 point)
        {
            Vector3 direction = capsuleEnd - capsuleStart;
            float length = direction.magnitude;
            direction.Normalize();

            float t = Mathf.Clamp01(Vector3.Dot(point - capsuleStart, direction) / length);
            return capsuleStart + t * direction;
        }
    }
}
