using UnityEngine;

namespace JayC.Utility.RayCast
{
    public class JSphereRaycastShape : JRaycastShape
    {
        [Range(0.1f, 10f)]
        public float sphereRadius = 1f;

        protected override bool CheckIntersection(Vector3 objectPosition)
        {
            float distance = Vector3.Distance(referencePoint.position, objectPosition);
            return distance <= maxRaycastDistance && distance <= sphereRadius;
        }

        protected override void DrawGizmoShape()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(referencePoint.position, sphereRadius);
        }

        protected internal override Transform[] FindObjectsInShape()
        {
            Collider[] colliders = Physics.OverlapSphere(referencePoint.position, maxRaycastDistance,objectLayer);

            Transform[] objectsInShape = new Transform[colliders.Length];
            int count = 0;

            foreach (Collider collider in colliders)
            {
                if (collider == null)
                    continue;

                Transform objectTransform = collider.transform;

                if (CheckIntersection(objectTransform.position))
                {
                    objectsInShape[count] = objectTransform;
                    count++;
                }
            }

            return objectsInShape;
        }
    }
}
