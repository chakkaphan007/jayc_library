using UnityEngine;

namespace JayC.Utility.RayCast
{
    public class JCubeRaycastShape : JRaycastShape
    {
        public Vector3 cubeSize = Vector3.one;

        protected override bool CheckIntersection(Vector3 objectPosition)
        {
            Vector3 localObjectPosition = referencePoint.InverseTransformPoint(objectPosition);
            return Mathf.Abs(localObjectPosition.x) <= cubeSize.x / 2f &&
                   Mathf.Abs(localObjectPosition.y) <= cubeSize.y / 2f &&
                   Mathf.Abs(localObjectPosition.z) <= cubeSize.z / 2f;
        }

        protected override void DrawGizmoShape()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireCube(referencePoint.position, cubeSize);
        }

        protected internal override Transform[] FindObjectsInShape()
        {
            Collider[] colliders = Physics.OverlapBox(referencePoint.position, cubeSize / 2f, referencePoint.rotation,objectLayer);

            Transform[] objectsInShape = new Transform[colliders.Length];
            int count = 0;

            foreach (Collider collider in colliders)
            {
                if (collider == null)
                    continue;

                Transform objectTransform = collider.transform;

                if (CheckIntersection(objectTransform.position))
                {
                    objectsInShape[count] = objectTransform;
                    count++;
                }
            }

            return objectsInShape;
        }
    }
}
