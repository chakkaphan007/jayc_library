using System.Collections;
using System.Collections.Generic;
using JayC.CommandPattern.Interface;
using UnityEngine;

namespace JayC.Utility
{
    public class UndoManager
    {
        private Stack<ICommand> commandHistory = new();
        private Stack<ICommand> redoHistory = new();

        public void ExecuteCommand(ICommand command)
        {
            command.Execute();
            commandHistory.Push(command);
            // Clear the redo history when a new command is executed.
            redoHistory.Clear();
        }

        public void Undo()
        {
            if (commandHistory.Count > 0)
            {
                var command = commandHistory.Pop();
                command.Undo();
                redoHistory.Push(command);
            }
        }

        public void Redo()
        {
            if (redoHistory.Count > 0)
            {
                var command = redoHistory.Pop();
                command.Execute();
                commandHistory.Push(command);
            }
        }
    }
}
