using System.Collections.Generic;
using UnityEngine;

namespace JayC.Utility
{
    public class JCustomAreaChecker : MonoBehaviour
    {
        public delegate void AreaChangedDelegate(bool newValue);

        [SerializeField] private List<Transform> objects = new();
        
        public Transform playerTransform;
        public Transform UseTestObject;

        [SerializeField] private bool isInsideArea;

        protected Vector3 Position;
        protected bool previousValue;

        protected virtual void Update()
        {
            if (isInsideArea != previousValue)
            {
                previousValue = isInsideArea;

                if (OnAreaChanged != null) OnAreaChanged.Invoke(isInsideArea);
            }
        }

        protected virtual void OnEnable()
        {
            OnAreaChanged += HandleChange;
        }

        protected virtual void OnDisable()
        {
            OnAreaChanged -= HandleChange;
        }

        protected virtual void OnDrawGizmos()
        {
            Position = !UseTestObject ? playerTransform.position : UseTestObject.position;

            var count = objects.Count;

            // Draw the connections
            for (var i = 0; i < count; i++)
            {
                var startObject = objects[i];
                var endObject = objects[(i + 1) % count];

                if (startObject != null && endObject != null)
                {
                    Gizmos.color = Color.yellow;
                    Gizmos.DrawLine(startObject.position, endObject.position);
                }
            }

            Vector3 pointToCheck;
            // Check if a point is inside the area formed by the connections
            pointToCheck = Position;

            isInsideArea = IsPointInsidePolygon(pointToCheck);

            Gizmos.color = isInsideArea ? Color.green : Color.red;
            Gizmos.DrawSphere(pointToCheck, 1f);
        }

        protected virtual void OnDrawGizmosSelected()
        {
            OnDrawGizmos();
        }

        public static event AreaChangedDelegate OnAreaChanged;
        

        protected virtual void HandleChange(bool isInsideArea)
        {
            if (isInsideArea){}
            else{}
                
        }

        protected virtual bool IsPointInsidePolygon(Vector3 point)
        {
            var count = objects.Count;
            var isInside = false;

            for (int i = 0, j = count - 1; i < count; j = i++)
            {
                var vertexI = objects[i].position;
                var vertexJ = objects[j].position;

                if (vertexI.z > point.z != vertexJ.z > point.z &&
                    point.x < (vertexJ.x - vertexI.x) * (point.z - vertexI.z) / (vertexJ.z - vertexI.z) + vertexI.x)
                    isInside = !isInside;
            }

            return isInside;
        }
    }
}