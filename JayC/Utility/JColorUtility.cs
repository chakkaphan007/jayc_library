using UnityEngine;

namespace JayC.Utility
{
    public static class JColorUtility
    {
        public static string ColorToHex(Color color)
        {
            // Convert each channel (R, G, B) to its hexadecimal representation
            int r = Mathf.RoundToInt(color.r * 255);
            int g = Mathf.RoundToInt(color.g * 255);
            int b = Mathf.RoundToInt(color.b * 255);

            // Combine the hexadecimal values into a single string
            string hex = string.Format("#{0:X2}{1:X2}{2:X2}", r, g, b);

            return hex;
        }

        public static Color HexToColor(string hex)
        {
            // Remove the '#' character if it's present
            hex = hex.TrimStart('#');

            // Parse the hexadecimal values for R, G, and B
            if (int.TryParse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber, null, out int r) &&
                int.TryParse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber, null, out int g) &&
                int.TryParse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber, null, out int b))
            {
                // Convert the hexadecimal values back to the range [0, 1]
                float rNormalized = r / 255.0f;
                float gNormalized = g / 255.0f;
                float bNormalized = b / 255.0f;

                // Create a Unity Color
                Color color = new Color(rNormalized, gNormalized, bNormalized);

                return color;
            }
            else
            {
                // Handle parsing error, return white color as a fallback
                Debug.LogWarning("Failed to parse Hex color: " + hex);
                return Color.white;
            }
        }

        public static Color HexToColor255(string hex)
        {
            // Remove the '#' character if it's present
            hex = hex.TrimStart('#');

            // Parse the hexadecimal values for R, G, and B
            if (int.TryParse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber, null, out int r) &&
                int.TryParse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber, null, out int g) &&
                int.TryParse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber, null, out int b))
            {
                // Create a Unity Color with values in the range [0, 255]
                Color color = new Color(r, g, b, 255);

                return color;
            }
            else
            {
                // Handle parsing error, return white color as a fallback
                Debug.LogWarning("Failed to parse Hex color: " + hex);
                return new Color(255, 255, 255, 255);
            }
        }

        public static string RGB1ToHex(float r, float g, float b)
        {
            // Convert RGB values in the range [0, 1] to Hex
            int r255 = Mathf.RoundToInt(r * 255);
            int g255 = Mathf.RoundToInt(g * 255);
            int b255 = Mathf.RoundToInt(b * 255);

            string hex = string.Format("#{0:X2}{1:X2}{2:X2}", r255, g255, b255);

            return hex;
        }

        public static string RGB255ToHex(int r, int g, int b)
        {
            // Convert RGB values in the range [0, 255] to Hex
            string hex = string.Format("#{0:X2}{1:X2}{2:X2}", r, g, b);

            return hex;
        }

        public static string HSVToHex(float h, float s, float v)
        {
            // Convert HSV values to Unity Color
            Color color = Color.HSVToRGB(h, s, v);

            // Convert the Color to Hex
            string hex = ColorToHex(color);

            return hex;
        }
    }
}