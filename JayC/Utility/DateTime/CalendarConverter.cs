///-----------------------------------------------------------------
///   Author : JayC        
///   Date   : 4/12/2024
///-----------------------------------------------------------------
using System;
using System.Globalization;

namespace JayC.Utility
{
    public static class ThaiBuddhistCalendarConverter
    {
        private static readonly ThaiBuddhistCalendar ThaiCalendar = new ThaiBuddhistCalendar();

        /// <summary>
        /// Converts a Gregorian DateTime to its Thai Buddhist Calendar equivalent.
        /// </summary>
        /// <param name="dateTime">The Gregorian DateTime to convert.</param>
        /// <returns>A DateTime adjusted for the Thai Buddhist Calendar.</returns>
        public static DateTime ToThaiBuddhistDateTime(DateTime dateTime)
        {
            int thaiYear = ThaiCalendar.GetYear(dateTime);
            int thaiMonth = ThaiCalendar.GetMonth(dateTime);
            int thaiDay = ThaiCalendar.GetDayOfMonth(dateTime);
            int thaiHour = ThaiCalendar.GetHour(dateTime);
            int thaiMinute = ThaiCalendar.GetMinute(dateTime);
            int thaiSecond = ThaiCalendar.GetSecond(dateTime);

            return ThaiCalendar.ToDateTime(thaiYear, thaiMonth, thaiDay, thaiHour, thaiMinute, thaiSecond, 0);
        }

        /// <summary>
        /// Converts a Thai Buddhist Calendar DateTime to its Gregorian Calendar equivalent.
        /// </summary>
        /// <param name="thaiDateTime">The Thai Buddhist Calendar DateTime to convert.</param>
        /// <returns>A DateTime adjusted for the Gregorian Calendar.</returns>
        public static DateTime ToGregorianDateTime(DateTime thaiDateTime)
        {
            int gregorianYear = ThaiCalendar.ToFourDigitYear(ThaiCalendar.GetYear(thaiDateTime));
            int month = ThaiCalendar.GetMonth(thaiDateTime);
            int day = ThaiCalendar.GetDayOfMonth(thaiDateTime);
            int hour = ThaiCalendar.GetHour(thaiDateTime);
            int minute = ThaiCalendar.GetMinute(thaiDateTime);
            int second = ThaiCalendar.GetSecond(thaiDateTime);

            return new DateTime(gregorianYear, month, day, hour, minute, second, thaiDateTime.Kind);
        }

        /// <summary>
        /// Formats a DateTime using the Thai Buddhist Calendar.
        /// </summary>
        /// <param name="dateTime">The DateTime to format.</param>
        /// <param name="format">The desired date format (e.g., "dd/MM/yyyy").</param>
        /// <returns>A string formatted using the Thai Buddhist Calendar.</returns>
        public static string FormatThaiBuddhistDateTime(DateTime dateTime, string format = "dd/MM/yyyy")
        {
            var thaiCulture = new CultureInfo("th-TH") { DateTimeFormat = { Calendar = ThaiCalendar } };
            return dateTime.ToString(format, thaiCulture);
        }

        /// <summary>
        /// Adjusts the input DateTime to the Thai time zone (ICT).
        /// </summary>
        /// <param name="dateTime">The DateTime to adjust.</param>
        /// <returns>A DateTime adjusted to the Thai time zone (ICT).</returns>
        public static DateTime ConvertToThaiTimeZone(DateTime dateTime)
        {
            TimeZoneInfo thaiTimeZone = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
            return TimeZoneInfo.ConvertTime(dateTime, thaiTimeZone);
        }
    }
}
