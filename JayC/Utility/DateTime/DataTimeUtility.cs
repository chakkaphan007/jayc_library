///-----------------------------------------------------------------
///   Author : JayC
///   Date   : 25/09/2024
///-----------------------------------------------------------------
using System;
using UnityEngine;

namespace JayC.Utility
{
    public static class DataTimeUtility
    {
        // Get current date and time
        public static DateTime GetCurrentDateTime()
        {
            return DateTime.Now;
        }

        // Get current date only
        public static DateTime GetCurrentDate()
        {
            return DateTime.Today;
        }

        // Get current time only
        public static TimeSpan GetCurrentTime()
        {
            return DateTime.Now.TimeOfDay;
        }

        public static bool IsLeapYear(int year)
        {
            return DateTime.IsLeapYear(year);
        }

        // Add days
        public static DateTime AddDays(DateTime date, int days)
        {
            return date.AddDays(days);
        }

        // Add months
        public static DateTime AddMonths(DateTime date, int months)
        {
            return date.AddMonths(months);
        }

        // Add years
        public static DateTime AddYears(DateTime date, int years)
        {
            return date.AddYears(years);
        }

        // Subtract days
        public static DateTime SubtractDays(DateTime date, int days)
        {
            return date.AddDays(-days);
        }

        // Get the start of the day
        public static DateTime StartOfDay(DateTime date)
        {
            return date.Date;
        }

        // Get the start of the week (assuming Sunday is the start of the week)
        public static DateTime StartOfWeek(DateTime date)
        {
            int diff = (int)date.DayOfWeek;
            return date.AddDays(-diff).Date;
        }

        // Get the start of the month
        public static DateTime StartOfMonth(DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }

        // Get the start of the year
        public static DateTime StartOfYear(DateTime date)
        {
            return new DateTime(date.Year, 1, 1);
        }

        // Convert date to a custom format
        public static string FormatDate(DateTime date, string format = "yyyy-MM-dd")
        {
            return date.ToString(format);
        }

        public static int CalculateAge(DateTime birthDate)
        {
            var today = DateTime.Today;
            var age = today.Year - birthDate.Year;

            if (birthDate.Date > today.AddYears(-age)) age--;

            return age;
        }

        public static int CalculateDaysDifference(DateTime startDate, DateTime endDate)
        {
            // Subtract the start date from the end date to get the TimeSpan
            TimeSpan difference = endDate - startDate;

            // Return the number of days in the TimeSpan
            return difference.Days;
        }
    }
}