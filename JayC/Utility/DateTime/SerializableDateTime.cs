using System;
using UnityEngine;
using System.Globalization;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace JayC.Utility
{
    [Serializable]
    public class SerializableDateTime
    {
        [SerializeField] private int year;
        [SerializeField] private int month;
        [SerializeField] private int day;
        [SerializeField] private int hour;
        [SerializeField] private int minute;
        [SerializeField] private int second;

        public SerializableDateTime()
        {
            DateTimeValue = DateTime.Now;
        }

        public DateTime DateTimeValue
        {
            get => new DateTime(year, month, day, hour, minute, second);
            set
            {
                year = value.Year;
                month = value.Month;
                day = value.Day;
                hour = value.Hour;
                minute = value.Minute;
                second = value.Second;
            }
        }

        public override string ToString() => DateTimeValue.ToString("yyyy-MM-dd HH:mm:ss");
    }

#if UNITY_EDITOR

    [CustomPropertyDrawer(typeof(SerializableDateTime))]
    public class SerializableDateTimeDrawer : PropertyDrawer
    {
        private const float PADDING = 2f;
        private const float HEADER_HEIGHT = 22f;
        private GUIStyle headerStyle;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return HEADER_HEIGHT + (EditorGUIUtility.singleLineHeight * 4) + (EditorGUIUtility.standardVerticalSpacing * 5) + (PADDING * 2);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            if (headerStyle == null)
            {
                headerStyle = new GUIStyle(EditorStyles.helpBox)
                {
                    fontSize = 12,
                    fontStyle = FontStyle.Bold,
                    alignment = TextAnchor.MiddleCenter
                };
            }

            var yearProp = property.FindPropertyRelative("year");
            var monthProp = property.FindPropertyRelative("month");
            var dayProp = property.FindPropertyRelative("day");
            var hourProp = property.FindPropertyRelative("hour");
            var minuteProp = property.FindPropertyRelative("minute");
            var secondProp = property.FindPropertyRelative("second");

            DateTime currentValue = new DateTime(yearProp.intValue, monthProp.intValue, dayProp.intValue, hourProp.intValue, minuteProp.intValue, secondProp.intValue);

            Rect contentRect = EditorGUI.PrefixLabel(position, label);
            contentRect = new Rect(contentRect.x - EditorGUIUtility.labelWidth, contentRect.y, contentRect.width + EditorGUIUtility.labelWidth, contentRect.height);

            // Header with day of week and other useful data
            Rect headerRect = new Rect(contentRect.x, contentRect.y, contentRect.width, HEADER_HEIGHT);
            EditorGUI.DrawRect(headerRect, new Color(0.7f, 0.7f, 0.7f, 0.1f));
            var dayofweek = DateTime.IsLeapYear(currentValue.Year) ? 366 : 365;
            string headerText = $"{currentValue.DayOfWeek}, Week {GetIso8601WeekOfYear(currentValue)}, Day {currentValue.DayOfYear} of {dayofweek}";
            EditorGUI.LabelField(headerRect, headerText, headerStyle);

            float lineHeight = EditorGUIUtility.singleLineHeight;
            float spacing = EditorGUIUtility.standardVerticalSpacing;
            float y = headerRect.y + headerRect.height + spacing;

            // Date fields
            Rect dateRect = new Rect(contentRect.x, y, contentRect.width, lineHeight * 2 + spacing);
            float dateWidth = (dateRect.width - spacing * 2) / 3;

            DrawDateField(new Rect(dateRect.x, dateRect.y, dateWidth, lineHeight * 2 + spacing), "Year", yearProp, GetYearOptions(), GetYearValues());
            DrawDateField(new Rect(dateRect.x + dateWidth + spacing, dateRect.y, dateWidth, lineHeight * 2 + spacing), "Month", monthProp, GetMonthOptions(), GetMonthValues());
            DrawDateField(new Rect(dateRect.x + (dateWidth + spacing) * 2, dateRect.y, dateWidth, lineHeight * 2 + spacing), "Day", dayProp, GetDayOptions(yearProp.intValue, monthProp.intValue), GetDayValues(yearProp.intValue, monthProp.intValue));

            // Time fields
            y += dateRect.height + spacing;
            Rect timeRect = new Rect(contentRect.x, y, contentRect.width, lineHeight * 2 + spacing);
            float timeWidth = (timeRect.width - spacing * 2) / 3;

            DrawTimeField(new Rect(timeRect.x, timeRect.y, timeWidth, lineHeight * 2 + spacing), "Hour", hourProp, GetHourOptions(), GetHourValues());
            DrawTimeField(new Rect(timeRect.x + timeWidth + spacing, timeRect.y, timeWidth, lineHeight * 2 + spacing), "Minute", minuteProp, GetMinuteOptions(), GetMinuteValues());
            DrawTimeField(new Rect(timeRect.x + (timeWidth + spacing) * 2, timeRect.y, timeWidth, lineHeight * 2 + spacing), "Second", secondProp, GetSecondOptions(), GetSecondValues());

            EditorGUI.EndProperty();
        }

        private void DrawDateField(Rect rect, string label, SerializedProperty prop, string[] options, int[] values)
        {
            EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), label);
            EditorGUI.DrawRect(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight + 2, rect.width, rect.height - EditorGUIUtility.singleLineHeight - 2), new Color(0.5f, 0.5f, 0.5f, 0.1f));
            prop.intValue = EditorGUI.IntPopup(new Rect(rect.x + 2, rect.y + EditorGUIUtility.singleLineHeight + 4, rect.width - 4, EditorGUIUtility.singleLineHeight), prop.intValue, options, values);
        }

        private void DrawTimeField(Rect rect, string label, SerializedProperty prop, string[] options, int[] values)
        {
            EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), label);
            EditorGUI.DrawRect(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight + 2, rect.width, rect.height - EditorGUIUtility.singleLineHeight - 2), new Color(0.5f, 0.5f, 0.5f, 0.1f));
            prop.intValue = EditorGUI.IntPopup(new Rect(rect.x + 2, rect.y + EditorGUIUtility.singleLineHeight + 4, rect.width - 4, EditorGUIUtility.singleLineHeight), prop.intValue, options, values);
        }

        // ISO 8601 week of year calculation
        private int GetIso8601WeekOfYear(DateTime time)
        {
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        private string[] GetYearOptions()
        {
            int currentYear = DateTime.Now.Year;
            string[] options = new string[11];
            for (int i = 0; i < 11; i++)
            {
                options[i] = (currentYear - 5 + i).ToString();
            }
            return options;
        }

        private int[] GetYearValues()
        {
            int currentYear = DateTime.Now.Year;
            int[] values = new int[11];
            for (int i = 0; i < 11; i++)
            {
                values[i] = currentYear - 5 + i;
            }
            return values;
        }

        private string[] GetMonthOptions() => new string[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

        private int[] GetMonthValues() => new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

        private string[] GetDayOptions(int year, int month)
        {
            int daysInMonth = DateTime.DaysInMonth(year, month);
            string[] options = new string[daysInMonth];
            for (int i = 0; i < daysInMonth; i++)
            {
                options[i] = (i + 1).ToString();
            }
            return options;
        }

        private int[] GetDayValues(int year, int month)
        {
            int daysInMonth = DateTime.DaysInMonth(year, month);
            int[] values = new int[daysInMonth];
            for (int i = 0; i < daysInMonth; i++)
            {
                values[i] = i + 1;
            }
            return values;
        }

        private string[] GetHourOptions()
        {
            string[] options = new string[24];
            for (int i = 0; i < 24; i++)
            {
                options[i] = i.ToString("D2");
            }
            return options;
        }

        private int[] GetHourValues()
        {
            int[] values = new int[24];
            for (int i = 0; i < 24; i++)
            {
                values[i] = i;
            }
            return values;
        }

        private string[] GetMinuteOptions()
        {
            string[] options = new string[60];
            for (int i = 0; i < 60; i++)
            {
                options[i] = i.ToString("D2");
            }
            return options;
        }

        private int[] GetMinuteValues()
        {
            int[] values = new int[60];
            for (int i = 0; i < 60; i++)
            {
                values[i] = i;
            }
            return values;
        }

        private string[] GetSecondOptions() => GetMinuteOptions();

        private int[] GetSecondValues() => GetMinuteValues();
    }

#endif
}