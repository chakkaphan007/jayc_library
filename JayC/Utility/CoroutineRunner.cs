///-----------------------------------------------------------------
///   Author : JayC
///   Date   : 30/10/2024
///-----------------------------------------------------------------
using System.Collections;
using UnityEngine;

//Coroutine myCoroutine = CoroutineRunner.Start(MyCoroutineMethod());
//CoroutineRunner.Stop(myCoroutine);

namespace JayC.Utility
{
    public class CoroutineRunner : MonoBehaviour
    {
        private static CoroutineRunner instance;

        public static Coroutine Start(IEnumerator coroutine)
        {
            if (instance == null)
            {
                var go = new GameObject("CoroutineRunner");
                instance = go.AddComponent<CoroutineRunner>();
                DontDestroyOnLoad(go);
            }
            return instance.StartCoroutine(coroutine);
        }

        public static void Stop(Coroutine coroutine)
        {
            if (instance != null)
            {
                instance.StopCoroutine(coroutine);
            }
        }
    }
}