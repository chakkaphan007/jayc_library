#if JayC_Addressable
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Audio;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace JayC.Sound
{
    public class AudioPreloader : MonoBehaviour
    {
        public List<string> audioAddresses;
        public AudioMixerGroup preloadMixerGroup;

        private void Start()
        {
            PreloadAudioClips(audioAddresses, preloadMixerGroup, () => Debug.Log("Audio Preloading Complete"));
        }

        public void PreloadAudioClips(List<string> addresses, AudioMixerGroup mixerGroup, System.Action onComplete)
        {
            int loadedCount = 0;

            foreach (string address in addresses)
            {
                Addressables.LoadAssetAsync<AudioClip>(address).Completed += (operation) =>
                {
                    if (operation.Status == AsyncOperationStatus.Succeeded)
                    {
                        AudioClip clip = operation.Result;

                        // Create an AudioSource to hold the preloaded clip
                        AudioSource source = gameObject.AddComponent<AudioSource>();
                        source.clip = clip;
                        source.outputAudioMixerGroup = mixerGroup;  // Assign the mixer group

                        loadedCount++;
                        Debug.Log($"Preloaded: {address}");
                        if (loadedCount == addresses.Count)
                        {
                            onComplete?.Invoke();
                        }
                    }
                    else
                    {
                        Debug.LogError($"Failed to preload audio: {address}");
                    }
                };
            }
        }
    }
}
#endif