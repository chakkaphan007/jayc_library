using UnityEngine;

namespace JayC.Sound
{
    [CreateAssetMenu(fileName = "SoundType", menuName = "JayC/Sound/UTK_SFXType", order = 1)]
    public class SoundTypeScriptableObject : ScriptableObject
    {
        public string typeName;
    }
}