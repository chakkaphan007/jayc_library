#if JayC_Addressable
using JayC.Utility;
using JayC.Utility.Addressable;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.Audio;

namespace JayC.Sound
{
    public class SoundEffectsManager : ObservableObject
    {
        public static SoundEffectsManager Instance { get; private set; }
        private List<AudioSource> audioSources = new List<AudioSource>();

        public void Initialize(GameObject audioManagerGameObject, AudioMixer audioMixer)
        {
            // Initialize AudioSources-
            for (int i = 0; i < 10; i++)
            {
                AudioSource audioSource = audioManagerGameObject.AddComponent<AudioSource>();
                audioSource.playOnAwake = false;
                audioSources.Add(audioSource);
            }
        }

        public AudioSource GetAvailableAudioSource()
        {
            foreach (AudioSource source in audioSources)
            {
                if (!source.isPlaying)
                {
                    return source;
                }
            }

            // If no AudioSource is available, create a new one and add it to the pool.
            AudioSource newSource = new GameObject("AudioSource").AddComponent<AudioSource>();
            newSource.playOnAwake = false;
            audioSources.Add(newSource);
            return newSource;
        }

        public AudioMixerGroup sfxMixerGroup;

        private float sfxVolume
        {
            get => GetValue<float>(nameof(sfxVolume));
            set => SetValue(value, nameof(sfxVolume));
        }

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }

            // Initialize AddressableAudioManager
            Initialize(gameObject, null);
        }

        private void OnEnable()
        {
            PropertyChangeNotifier.Subscribe(nameof(sfxVolume), SetVolume);
        }

        private void SetVolume(object arg1, PropertyChangedEventArgs args)
        {
            foreach (var source in audioSources)
            {
                if (source.outputAudioMixerGroup != null)
                {
                    source.outputAudioMixerGroup.audioMixer.SetFloat("SFXVolume", sfxVolume);
                }
            }
        }

        public void PlaySound(string address, bool loop = false, float volume = 1f)
        {
            AddressableAudioManager.LoadAudio(address, (clip) =>
            {
                AudioSource audioSource = GetAvailableAudioSource();
                audioSource.clip = clip;
                audioSource.outputAudioMixerGroup = sfxMixerGroup;
                audioSource.loop = loop;
                audioSource.volume = volume;
                audioSource.Play();
            });
        }

        public void StopAllAudio()
        {
            foreach (var source in audioSources)
            {
                if (source.isPlaying)
                {
                    source.Stop();
                }
            }
        }
    }
}
#endif