#if JayC_Addressable
using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using JayC.Utility.Addressable;
using JayC.Utility;
using System.ComponentModel;

namespace JayC.Sound
{
    public class BackgroundMusicManager : ObservableObject
    {
        public static BackgroundMusicManager Instance { get; private set; }
        public AudioSource currentMusicSource;
        public AudioSource previousMusicSource;
        public AudioMixerGroup musicMixerGroup;

        public float musicVolume
        {
            get => GetValue<float>(nameof(musicVolume));
            set => SetValue(value, nameof(musicVolume));
        }

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }

            // Create an AudioSource for music playback
            currentMusicSource = gameObject.AddComponent<AudioSource>();
            currentMusicSource.loop = true; // Music usually loops
            currentMusicSource.playOnAwake = false;
        }

        private void OnEnable()
        {
            PropertyChangeNotifier.Subscribe(nameof(musicVolume), SetVolume);
        }

        private void SetVolume(object arg1, PropertyChangedEventArgs args)
        {
            if (currentMusicSource != null)
            {
                currentMusicSource.volume = musicVolume;
            }
        }

        public void PlayMusic(string address, float volume = 1f, float fadeDuration = 1f)
        {
            AddressableAudioManager.LoadAudio(address, clip =>
            {
                if (currentMusicSource.isPlaying)
                {
                    StartCoroutine(CrossfadeMusic(clip, volume, fadeDuration));
                }
                else
                {
                    StartCoroutine(FadeInMusic(clip, volume, fadeDuration));
                }
            });
        }

        public void StopMusic(float fadeDuration = 1f)
        {
            StartCoroutine(FadeOutMusic(fadeDuration));
        }

        private IEnumerator CrossfadeMusic(AudioClip newClip, float targetVolume, float duration)
        {
            // Fade out the current music
            if (previousMusicSource != null)
            {
                Destroy(previousMusicSource);
            }

            previousMusicSource = currentMusicSource;
            currentMusicSource = gameObject.AddComponent<AudioSource>();
            currentMusicSource.clip = newClip;
            currentMusicSource.outputAudioMixerGroup = musicMixerGroup;
            currentMusicSource.volume = 0;
            currentMusicSource.loop = true;
            currentMusicSource.Play();

            float elapsedTime = 0f;

            // Fade out previous music and fade in new music
            while (elapsedTime < duration)
            {
                elapsedTime += Time.deltaTime;
                float t = elapsedTime / duration;
                if (previousMusicSource != null)
                {
                    previousMusicSource.volume = Mathf.Lerp(targetVolume, 0, t);
                }
                currentMusicSource.volume = Mathf.Lerp(0, targetVolume, t);
                yield return null;
            }

            if (previousMusicSource != null)
            {
                previousMusicSource.Stop();
                Destroy(previousMusicSource);
            }
        }

        private IEnumerator FadeOutMusic(float duration)
        {
            if (currentMusicSource == null) yield break;

            float startVolume = currentMusicSource.volume;
            float elapsedTime = 0f;

            while (elapsedTime < duration)
            {
                elapsedTime += Time.deltaTime;
                currentMusicSource.volume = Mathf.Lerp(startVolume, 0, elapsedTime / duration);
                yield return null;
            }

            currentMusicSource.Stop();
            currentMusicSource.volume = startVolume; // Reset volume to initial value
        }

        private IEnumerator FadeInMusic(AudioClip newClip, float volume, float duration)
        {
            currentMusicSource.clip = newClip;
            currentMusicSource.outputAudioMixerGroup = musicMixerGroup;
            currentMusicSource.volume = 0;
            currentMusicSource.Play();

            float elapsedTime = 0f;

            while (elapsedTime < duration)
            {
                elapsedTime += Time.deltaTime;
                currentMusicSource.volume = Mathf.Lerp(0, volume, elapsedTime / duration);
                yield return null;
            }

            currentMusicSource.volume = volume; // Ensure the volume is set to the target value
        }
    }
}
#endif