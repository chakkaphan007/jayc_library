#if JayC_Addressable
using JayC.UI.UIToolkit;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;

namespace JayC.Sound
{
    [System.Serializable]
    public class UISoundEffect
    {
        public SoundTypeScriptableObject SoundType; // Use ScriptableObject reference
        public string SoundAddress; // Addressables address or path to the sound
    }

    public abstract class AbstractUISoundEffects : MonoBehaviour
    {
        [SerializeField]
        protected List<UISoundEffect> soundEffects = new List<UISoundEffect>()
        {
            new UISoundEffect { SoundType = null, SoundAddress = "ButtonClick" },
            new UISoundEffect { SoundType = null, SoundAddress = "Hover" },
            new UISoundEffect { SoundType = null, SoundAddress = "ToggleOn" },
            new UISoundEffect { SoundType = null, SoundAddress = "ToggleOff" },
        };

        /// <summary>
        /// Example of setting up sound effects for buttons and toggles.
        /// </summary>
        //private void OnEnable()
        //{
        //    if (soundEffects.Count != 0)
        //    {
        //        UTKReferenceTool.GetButton(uiDocument, "Button").RegisterCallback<ClickEvent>(evt => PlaySoundByType("ButtonClick"));
        //    }
        //    SetupCommonUISound(gameObject.GetComponent<UIDocument>().rootVisualElement);
        //}

        /// <summary>
        /// Finds the sound address by matching the SoundTypeScriptableObject.
        /// </summary>
        /// <param name="type">SoundTypeScriptableObject to find the corresponding address.</param>
        /// <returns>Sound address string if found, otherwise an empty string.</returns>
        protected virtual string GetSoundAddress(SoundTypeScriptableObject type)
        {
            return soundEffects.FirstOrDefault(s => s.SoundType == type)?.SoundAddress ?? "";
        }

        /// <summary>
        /// Sets up common UI sound events such as button clicks and toggles.
        /// </summary>
        /// <param name="root">Root VisualElement for setting up sound effects.</param>
        public virtual void SetupCommonUISound(VisualElement root)
        {
            SetupButtonSounds(root);
            SetupToggleSounds(root);
        }

        /// <summary>
        /// Sets up sound effects for buttons.
        /// </summary>
        /// <param name="root">Root VisualElement containing the buttons.</param>
        protected virtual void SetupButtonSounds(VisualElement root)
        {
            var buttons = root.Query<Button>().ToList();
            foreach (var button in buttons)
            {
                button.RegisterCallback<ClickEvent>(evt => PlaySoundByType("ButtonClick")); // Updated for ScriptableObject-based sound type
                button.RegisterCallback<MouseEnterEvent>(evt => PlaySoundByType("Hover"));
            }
        }

        /// <summary>
        /// Sets up sound effects for toggles.
        /// </summary>
        /// <param name="root">Root VisualElement containing the toggles.</param>
        protected virtual void SetupToggleSounds(VisualElement root)
        {
            var toggles = root.Query<Toggle>().ToList();
            foreach (var toggle in toggles)
            {
                toggle.RegisterValueChangedCallback(evt =>
                {
                    if (evt.newValue)
                        PlaySoundByType("ToggleOn");
                    else
                        PlaySoundByType("ToggleOff");
                });
            }
        }

        /// <summary>
        /// Plays a sound based on the given sound type name.
        /// </summary>
        /// <param name="typeName">The type of the sound to play (e.g., "ButtonClick").</param>
        public virtual void PlaySoundByType(string typeName)
        {
            SoundTypeScriptableObject soundType = GetSoundTypeByName(typeName);
            if (soundType != null)
            {
                PlaySound(soundType);
            }
        }

        /// <summary>
        /// Gets the SoundTypeScriptableObject by name.
        /// </summary>
        /// <param name="typeName">The name of the sound type.</param>
        /// <returns>Corresponding SoundTypeScriptableObject or null if not found.</returns>
        protected virtual SoundTypeScriptableObject GetSoundTypeByName(string typeName)
        {
            // Assuming SoundTypeScriptableObjects are stored in a resource folder
            SoundTypeScriptableObject soundType = Resources.LoadAll<SoundTypeScriptableObject>("")
                .FirstOrDefault(st => st.typeName == typeName);
            return soundType;
        }

        /// <summary>
        /// Plays the sound using Addressables based on the sound type.
        /// </summary>
        /// <param name="type">SoundTypeScriptableObject representing the sound type.</param>
        protected virtual void PlaySound(SoundTypeScriptableObject type)
        {
            string address = GetSoundAddress(type);
            if (!string.IsNullOrEmpty(address))
            {
                SoundEffectsManager.Instance.PlaySound(address);
            }
        }

        /// <summary>
        /// Adds a new sound effect to the list.
        /// </summary>
        /// <param name="type">SoundTypeScriptableObject representing the type of the sound.</param>
        /// <param name="address">The address or path to the sound file.</param>
        public virtual void AddSound(SoundTypeScriptableObject type, string address)
        {
            soundEffects.Add(new UISoundEffect { SoundType = type, SoundAddress = address });
        }

        /// <summary>
        /// Removes a sound effect from the list based on the sound type.
        /// </summary>
        /// <param name="type">SoundTypeScriptableObject representing the sound type to remove.</param>
        public virtual void RemoveSound(SoundTypeScriptableObject type)
        {
            soundEffects.RemoveAll(s => s.SoundType == type);
        }
    }
}
#endif