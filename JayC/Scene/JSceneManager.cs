#if JayC_Addressable
using JayC.UI.UIToolkit;
using JayC.Utility.Addressable;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace JayC.SceneManagement
{
    public class JSceneManager : MonoBehaviour
    {
        public static JSceneManager Instance { get; private set; }

        [Header("UI Toolkit Fade Transition")]
        public VisualElement fadeVisualElement; // VisualElement for fade effect
        public float fadeDuration = 1f; // Default duration for fade transitions
        public ProgressBar progressBar; // Reference to the progress bar for scene loading

        [SerializeField] private UIDocument uiDocument; // Reference to the UI document
        private bool isTransitioning = false;

        /// <summary>
        /// Debug Variable
        /// </summary>
        public string sceneKey;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                Instance = this;
                DontDestroyOnLoad(gameObject); // Persist between scenes
            }
        }

        private void Start()
        {
            if (uiDocument != null)
            {
                fadeVisualElement = UTKReferenceTool.GetVisualElement(uiDocument, "FadeElement");
                fadeVisualElement.style.opacity = 0; // Ensure it's transparent at start

                progressBar = UTKReferenceTool.GetProgressBar(uiDocument, "ProgressBar");
                if (progressBar != null)
                {
                    progressBar.visible = false; // Hide progress bar at start
                }
            }
        }

        public void LoadScene(string sceneKey, bool fade = true, float? customFadeDuration = null, bool singleMode = false)
        {
            Debug.Log($"Load Scene : {sceneKey}");

            if (!isTransitioning)
            {
                float fadeTime = customFadeDuration ?? fadeDuration;

                // Check if this is a single mode load, if so, we need to unload previous scenes
                if (singleMode)
                {
                    UnloadAllScenes(() => StartCoroutine(LoadSceneCoroutine(sceneKey, fade, fadeTime, singleMode)));
                }
                else
                {
                    StartCoroutine(LoadSceneCoroutine(sceneKey, fade, fadeTime, singleMode));
                }
            }
        }

        private void UnloadAllScenes(Action onComplete)
        {
            // Unload all scenes in AddressableSceneManager except the active scene
            List<string> scenesToUnload = new List<string>(AddressableSceneManager.loadedScenes.Keys);

            StartCoroutine(UnloadScenesCoroutine(scenesToUnload, onComplete));
        }

        public void UnloadScene(string sceneKey, bool fade = true)
        {
            if (!isTransitioning)
            {
                StartCoroutine(UnloadSceneCoroutine(sceneKey, fade));
            }
        }

        private IEnumerator UnloadScenesCoroutine(List<string> scenesToUnload, Action onComplete)
        {
            foreach (var sceneKey in scenesToUnload)
            {
                yield return StartCoroutine(UnloadSceneCoroutine(sceneKey, fade: false));
            }

            onComplete?.Invoke();
        }

        public void TransitionScenes(string oldSceneKey, string newSceneKey, bool fade = true)
        {
            if (!isTransitioning)
            {
                StartCoroutine(SceneTransitionCoroutine(oldSceneKey, newSceneKey, fade));
            }
        }

        // Direct Scene Management Methods

        public void LoadSceneDirectly(string sceneKey, LoadSceneMode loadMode = LoadSceneMode.Single, Action onComplete = null, Action onFail = null)
        {
            if (isTransitioning) return;

            StartCoroutine(LoadSceneDirectlyCoroutine(sceneKey, loadMode, onComplete, onFail));
        }

        public void UnloadSceneDirectly(string sceneKey, Action onComplete = null, Action onFail = null)
        {
            if (isTransitioning) return;

            StartCoroutine(UnloadSceneDirectlyCoroutine(sceneKey, onComplete, onFail));
        }

        private IEnumerator LoadSceneDirectlyCoroutine(string sceneKey, LoadSceneMode loadMode, Action onComplete, Action onFail)
        {
            isTransitioning = true;

            AsyncOperationHandle<SceneInstance> handle = AddressableSceneManager.LoadSceneAsync(sceneKey, loadMode);

            while (!handle.IsDone)
            {
                yield return null;
            }

            if (handle.Status == AsyncOperationStatus.Succeeded)
            {
                Debug.Log($"Scene '{sceneKey}' loaded successfully.");
                onComplete?.Invoke();
            }
            else
            {
                Debug.LogError($"Failed to load scene '{sceneKey}': {handle.OperationException}");
                onFail?.Invoke();
            }

            isTransitioning = false;
        }

        private IEnumerator UnloadSceneDirectlyCoroutine(string sceneKey, Action onComplete, Action onFail)
        {
            isTransitioning = true;

            bool unloadSuccess = false;
            AddressableSceneManager.UnloadScene(sceneKey, () => { unloadSuccess = true; }, () => { unloadSuccess = false; });

            yield return new WaitUntil(() => unloadSuccess);

            if (unloadSuccess)
            {
                Debug.Log($"Scene '{sceneKey}' unloaded successfully.");
                onComplete?.Invoke();
            }
            else
            {
                Debug.LogError($"Failed to unload scene '{sceneKey}'.");
                onFail?.Invoke();
            }

            isTransitioning = false;
        }

        // Private Methods

        private IEnumerator LoadSceneCoroutine(string sceneKey, bool fade, float fadeTime, bool singleMode)
        {
            isTransitioning = true;

            if (fade)
            {
                yield return StartCoroutine(FadeOut(fadeTime));
            }

            // Show progress bar
            if (progressBar != null)
            {
                progressBar.visible = true;
                progressBar.value = 0;
            }

            // Check if the scene is already being loaded or is loaded
            if (AddressableSceneManager.loadedScenes.ContainsKey(sceneKey))
            {
                Debug.LogWarning($"Scene '{sceneKey}' is already loaded.");
                StartCoroutine(FadeIn(fadeTime));
                isTransitioning = false;
                yield break;
            }

            // Load the scene using AddressableSceneManager
            AsyncOperationHandle<SceneInstance> handle = AddressableSceneManager.LoadSceneAsync(sceneKey, singleMode ? LoadSceneMode.Single : LoadSceneMode.Additive);

            while (!handle.IsDone)
            {
                if (progressBar != null)
                {
                    progressBar.value = handle.PercentComplete;
                }
                yield return null;
            }

            // Hide progress bar
            if (progressBar != null)
            {
                progressBar.visible = false;
            }

            if (handle.Status == AsyncOperationStatus.Succeeded)
            {
                Debug.Log($"Scene '{sceneKey}' loaded successfully.");
            }
            if (handle.Status == AsyncOperationStatus.Failed)
            {
                Debug.LogError($"Failed to load scene '{sceneKey}': {handle.OperationException}");
            }

            if (fade)
            {
                yield return StartCoroutine(FadeIn(fadeTime));
            }

            isTransitioning = false;
        }

        private IEnumerator UnloadSceneCoroutine(string sceneKey, bool fade)
        {
            isTransitioning = true;

            if (fade)
            {
                yield return StartCoroutine(FadeOut(fadeDuration));
            }

            bool unloadSuccess = false;
            AddressableSceneManager.UnloadScene(sceneKey, () => { unloadSuccess = true; }, () => { unloadSuccess = false; });

            yield return new WaitUntil(() => unloadSuccess);

            if (fade)
            {
                yield return StartCoroutine(FadeIn(fadeDuration));
            }

            isTransitioning = false;
        }

        private IEnumerator SceneTransitionCoroutine(string oldSceneKey, string newSceneKey, bool fade)
        {
            isTransitioning = true;
            if (fade)
            {
                yield return StartCoroutine(FadeOut(fadeDuration));
            }

            bool unloadSuccess = false;
            AddressableSceneManager.UnloadScene(oldSceneKey, () => { unloadSuccess = true; }, () => { unloadSuccess = false; });
            yield return new WaitUntil(() => unloadSuccess);

            bool loadSuccess = false;
            AddressableSceneManager.LoadScene(newSceneKey, LoadSceneMode.Additive, () => { loadSuccess = true; }, () => { loadSuccess = false; });
            yield return new WaitUntil(() => loadSuccess);

            if (fade)
            {
                yield return StartCoroutine(FadeIn(fadeDuration));
            }

            isTransitioning = false;
        }

        private IEnumerator FadeOut(float duration)
        {
            if (fadeVisualElement == null) yield break;

            fadeVisualElement.style.opacity = 0;
            fadeVisualElement.style.display = DisplayStyle.Flex;

            float elapsed = 0f;

            while (elapsed < duration)
            {
                elapsed += Time.deltaTime;
                float newOpacity = Mathf.Clamp01(elapsed / duration);
                fadeVisualElement.style.opacity = newOpacity;
                yield return null;
            }

            fadeVisualElement.style.opacity = 1f;
        }

        private IEnumerator FadeIn(float duration)
        {
            if (fadeVisualElement == null) yield break;

            float elapsed = 0f;

            while (elapsed < duration)
            {
                elapsed += Time.deltaTime;
                float newOpacity = Mathf.Clamp01(1f - (elapsed / duration));
                fadeVisualElement.style.opacity = newOpacity;
                yield return null;
            }

            fadeVisualElement.style.opacity = 0f;
            fadeVisualElement.style.display = DisplayStyle.None;
        }

        public void ReloadScene(string sceneKey, bool fade = true)
        {
            if (AddressableSceneManager.IsSceneLoaded(sceneKey))
            {
                // Unload the scene and reload it after
                UnloadSceneDirectly(sceneKey, () =>
                {
                    // Delay the loading to avoid modifying the collection during iteration
                    StartCoroutine(LoadSceneAfterUnload(sceneKey, fade));
                });
            }
            else
            {
                // Load the scene if not loaded
                LoadScene(sceneKey, fade);
            }
        }

        private IEnumerator LoadSceneAfterUnload(string sceneKey, bool fade)
        {
            yield return null; // Wait one frame to ensure collection isn't being modified

            // Reload the scene after unload
            LoadScene(sceneKey, fade);
        }

        public void ReloadActiveScene(bool fade = true)
        {
            string currentSceneName = SceneManager.GetActiveScene().name;

            // Ensure the scene can be reloaded if already active
            UnloadSceneDirectly(currentSceneName, () =>
            {
                LoadScene(currentSceneName, fade, null, true);
            });
        }
    }
}
#endif