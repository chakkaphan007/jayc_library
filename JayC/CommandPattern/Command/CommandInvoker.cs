using System.Collections.Generic;
using JayC.CommandPattern.Interface;

namespace JayC.CommandPattern.Command
{
    public class CommandInvoker
    {
        protected readonly List<ICommand> _commandHistory = new();

        public void ExecuteCommand(ICommand command)
        {
            command.Execute();
            _commandHistory.Add(command);
        }

        public void UndoLastCommand()
        {
            if (_commandHistory.Count > 0)
            {
                var lastCommand = _commandHistory[_commandHistory.Count - 1];
                lastCommand.Undo();
                _commandHistory.RemoveAt(_commandHistory.Count - 1);
            }
        }
    }
}