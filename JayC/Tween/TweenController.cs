#if JayC_Tween
using Tweens;
using UnityEngine;

public class TweenController : MonoBehaviour
{
    [System.Serializable]
    public class TweenSettings
    {
        public Vector3 targetValue;
        public float duration = 1f;
        public EaseType easeType = EaseType.Linear;
        public bool useAnimationCurve = false;
        public AnimationCurve animationCurve = AnimationCurve.Linear(0, 0, 1, 1);
    }

    public TweenSettings positionSettings = new TweenSettings();
    public TweenSettings scaleSettings = new TweenSettings();
    public TweenSettings rotationSettings = new TweenSettings();

    private TweenInstance<Transform, Vector3> currentPositionTween;
    private TweenInstance<Transform, Vector3> currentScaleTween;
    private TweenInstance<Transform, Vector3> currentRotationTween;

    public void PlayPositionTween()
    {
        if (currentPositionTween != null) currentPositionTween.Cancel();
        var tween = new PositionTween
        {
            to = positionSettings.targetValue,
            duration = positionSettings.duration,
            easeType = positionSettings.useAnimationCurve ? EaseType.Linear : positionSettings.easeType,
            animationCurve = positionSettings.useAnimationCurve ? positionSettings.animationCurve : null
        };
        currentPositionTween = this.gameObject.AddTween(tween);
    }

    public void PlayScaleTween()
    {
        if (currentScaleTween != null) currentScaleTween.Cancel();
        var tween = new LocalScaleTween
        {
            to = scaleSettings.targetValue,
            duration = scaleSettings.duration,
            easeType = scaleSettings.useAnimationCurve ? EaseType.Linear : scaleSettings.easeType,
            animationCurve = scaleSettings.useAnimationCurve ? scaleSettings.animationCurve : null
        };
        currentScaleTween = this.gameObject.AddTween(tween);
    }

    public void PlayRotationTween()
    {
        if (currentRotationTween != null) currentRotationTween.Cancel();
        var tween = new EulerAnglesTween
        {
            to = rotationSettings.targetValue,
            duration = rotationSettings.duration,
            easeType = rotationSettings.useAnimationCurve ? EaseType.Linear : rotationSettings.easeType,
            animationCurve = rotationSettings.useAnimationCurve ? rotationSettings.animationCurve : null
        };
        currentRotationTween = this.gameObject.AddTween(tween);
    }

    public void PauseTween(string tweenType)
    {
        switch (tweenType)
        {
            case "position":
                if (currentPositionTween != null) currentPositionTween.isPaused = !currentPositionTween.isPaused;
                break;

            case "scale":
                if (currentScaleTween != null) currentScaleTween.isPaused = !currentScaleTween.isPaused;
                break;

            case "rotation":
                if (currentRotationTween != null) currentRotationTween.isPaused = !currentRotationTween.isPaused;
                break;
        }
    }

    public void CancelTween(string tweenType)
    {
        switch (tweenType)
        {
            case "position":
                if (currentPositionTween != null) { currentPositionTween.Cancel(); currentPositionTween = null; }
                break;

            case "scale":
                if (currentScaleTween != null) { currentScaleTween.Cancel(); currentScaleTween = null; }
                break;

            case "rotation":
                if (currentRotationTween != null) { currentRotationTween.Cancel(); currentRotationTween = null; }
                break;
        }
    }

    public void ResetToOrigin()
    {
        transform.position = Vector3.zero;
        transform.localScale = Vector3.one;
        transform.rotation = Quaternion.identity;
    }
}
#endif