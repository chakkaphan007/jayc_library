#if JayC_Tween

using System;
using UnityEngine;
using Tweens;
using System.Collections.Generic;

namespace JayC.TweenManager
{
    public static class TweenManager
    {
        // Method to move a GameObject to a specific position over time
        public static TweenInstance<Transform, Vector3> MoveTo(GameObject target, Vector3 to
            , float duration = 1f, EaseType easeType = EaseType.Linear, Action onEnd = null, Action onAdd = null,
            Action onCancel = null, Action onFinally = null, Action onStart = null, Action<Vector3> onUpdate = null, float delay = 0f,
            int loops = 0, bool pingPong = false, float offset = 0f, AnimationCurve animationCurve = null,
            bool useUnscaledTime = false)
        {
            var tween = new PositionTween
            {
                to = to,
                duration = duration,
                easeType = easeType,
                delay = delay,
                loops = loops,
                usePingPong = pingPong,
                offset = offset,
                animationCurve = animationCurve,
                useUnscaledTime = useUnscaledTime,
                onEnd = instance => onEnd?.Invoke(),
                onAdd = instance => onAdd?.Invoke(),
                onCancel = instance => onCancel?.Invoke(),
                onFinally = instance => onFinally?.Invoke(),
                onStart = instance => onStart?.Invoke(),
                onUpdate = (_, value) => onUpdate(value),
            };
            return target.AddTween(tween);
        }

        // Method to scale a GameObject to a specific size over time
        public static TweenInstance<Transform, Vector3> ScaleTo(GameObject target, Vector3 to
            , float duration = 1f, EaseType easeType = EaseType.Linear, Action onEnd = null, Action onAdd = null,
            Action onCancel = null, Action onFinally = null, Action onStart = null, Action<Vector3> onUpdate = null, float delay = 0f,
            int loops = 0, bool pingPong = false, float offset = 0f, AnimationCurve animationCurve = null,
            bool useUnscaledTime = false)
        {
            var tween = new LocalScaleTween
            {
                to = to,
                duration = duration,
                easeType = easeType,
                delay = delay,
                loops = loops,
                usePingPong = pingPong,
                offset = offset,
                animationCurve = animationCurve,
                useUnscaledTime = useUnscaledTime,
                onEnd = instance => onEnd?.Invoke(),
                onAdd = instance => onAdd?.Invoke(),
                onCancel = instance => onCancel?.Invoke(),
                onFinally = instance => onFinally?.Invoke(),
                onStart = instance => onStart?.Invoke(),
                onUpdate = (_, value) => onUpdate(value),
            };
            return target.AddTween(tween);
        }

        // Method to rotate a GameObject to a specific rotation over time
        public static TweenInstance<Transform, Quaternion> RotateTo(GameObject target, Quaternion to
            , float duration = 1f, EaseType easeType = EaseType.Linear, Action onEnd = null, Action onAdd = null,
            Action onCancel = null, Action onFinally = null, Action onStart = null, Action<Quaternion> onUpdate = null, float delay = 0f,
            int loops = 0, bool pingPong = false, float offset = 0f, AnimationCurve animationCurve = null,
            bool useUnscaledTime = false)
        {
            var tween = new RotationTween
            {
                to = to,
                duration = duration,
                easeType = easeType,
                delay = delay,
                loops = loops,
                usePingPong = pingPong,
                offset = offset,
                animationCurve = animationCurve,
                useUnscaledTime = useUnscaledTime,
                onEnd = instance => onEnd?.Invoke(),
                onAdd = instance => onAdd?.Invoke(),
                onCancel = instance => onCancel?.Invoke(),
                onFinally = instance => onFinally?.Invoke(),
                onStart = instance => onStart?.Invoke(),
                onUpdate = (_, value) => onUpdate(value),
            };
            return target.AddTween(tween);
        }

        // Overload to handle rotation with Euler angles (Vector3)
        public static TweenInstance<Transform, Quaternion> RotateTo(GameObject target, Vector3 eulerAngles
            , float duration = 1f, EaseType easeType = EaseType.Linear, Action onEnd = null, Action onAdd = null,
            Action onCancel = null, Action onFinally = null, Action onStart = null, Action<Quaternion> onUpdate = null, float delay = 0f,
            int loops = 0, bool pingPong = false, float offset = 0f, AnimationCurve animationCurve = null,
            bool useUnscaledTime = false)
        {
            var tween = new RotationTween
            {
                to = Quaternion.Euler(eulerAngles),
                duration = duration,
                easeType = easeType,
                delay = delay,
                loops = loops,
                usePingPong = pingPong,
                offset = offset,
                animationCurve = animationCurve,
                useUnscaledTime = useUnscaledTime,
                onEnd = instance => onEnd?.Invoke(),
                onAdd = instance => onAdd?.Invoke(),
                onCancel = instance => onCancel?.Invoke(),
                onFinally = instance => onFinally?.Invoke(),
                onStart = instance => onStart?.Invoke(),
                onUpdate = (_, value) => onUpdate(value),
            };
            return target.AddTween(tween);
        }

        // Method to change the alpha of a SpriteRenderer over time
        public static TweenInstance<SpriteRenderer, float> FadeTo(GameObject target, float alpha
            , float duration = 1f, EaseType easeType = EaseType.Linear, Action onEnd = null, Action onAdd = null,
            Action onCancel = null, Action onFinally = null, Action onStart = null, Action<float> onUpdate = null, float delay = 0f,
            int loops = 0, bool pingPong = false, float offset = 0f, AnimationCurve animationCurve = null,
            bool useUnscaledTime = false)
        {
            var spriteRenderer = target.GetComponent<SpriteRenderer>();
            if (spriteRenderer != null)
            {
                var tween = new SpriteRendererAlphaTween
                {
                    to = alpha,
                    duration = duration,
                    easeType = easeType,
                    delay = delay,
                    loops = loops,
                    usePingPong = pingPong,
                    offset = offset,
                    animationCurve = animationCurve,
                    useUnscaledTime = useUnscaledTime,
                    onEnd = instance => onEnd?.Invoke(),
                    onAdd = instance => onAdd?.Invoke(),
                    onCancel = instance => onCancel?.Invoke(),
                    onFinally = instance => onFinally?.Invoke(),
                    onStart = instance => onStart?.Invoke(),
                    onUpdate = (_, value) => onUpdate(value),
                };
                return spriteRenderer.gameObject.AddTween(tween);
            }
            Debug.LogError("No SpriteRenderer found on target object!");
            return null;
        }

        // Float tweening
        public static TweenInstance<Transform, float> TweenFloat(
            GameObject target, float from, float to, Action<float> onUpdate,
            float duration = 1f, EaseType easeType = EaseType.Linear, Action onEnd = null, Action onAdd = null,
            Action onCancel = null, Action onFinally = null, Action onStart = null,
            float delay = 0f, int loops = 0, bool pingPong = false, float offset = 0f,
            AnimationCurve animationCurve = null, bool useUnscaledTime = false)
        {
            var tween = new FloatTween
            {
                from = from,
                to = to,
                duration = duration,
                easeType = easeType,
                delay = delay,
                loops = loops,
                usePingPong = pingPong,
                offset = offset,
                animationCurve = animationCurve,
                useUnscaledTime = useUnscaledTime,
                onUpdate = (_, value) => onUpdate(value),
                onEnd = instance => onEnd?.Invoke(),
                onAdd = instance => onAdd?.Invoke(),
                onCancel = instance => onCancel?.Invoke(),
                onFinally = instance => onFinally?.Invoke(),
                onStart = instance => onStart?.Invoke(),
            };
            return target.AddTween(tween);
        }

        // Vector2 tweening
        public static TweenInstance<Transform, Vector2> TweenVector2(
            GameObject target, Vector2 from, Vector2 to, Action<Vector2> onUpdate,
            float duration = 1f, EaseType easeType = EaseType.Linear, Action onEnd = null, Action onAdd = null,
            Action onCancel = null, Action onFinally = null, Action onStart = null,
            float delay = 0f, int loops = 0, bool pingPong = false, float offset = 0f,
            AnimationCurve animationCurve = null, bool useUnscaledTime = false)
        {
            var tween = new Vector2Tween
            {
                from = from,
                to = to,
                duration = duration,
                easeType = easeType,
                delay = delay,
                loops = loops,
                usePingPong = pingPong,
                offset = offset,
                animationCurve = animationCurve,
                useUnscaledTime = useUnscaledTime,
                onUpdate = (_, value) => onUpdate(value),
                onEnd = instance => onEnd?.Invoke(),
                onAdd = instance => onAdd?.Invoke(),
                onCancel = instance => onCancel?.Invoke(),
                onFinally = instance => onFinally?.Invoke(),
                onStart = instance => onStart?.Invoke(),
            };
            return target.AddTween(tween);
        }

        // Vector3 tweening
        public static TweenInstance<Transform, Vector3> TweenVector3(
            GameObject target, Vector3 from, Vector3 to, Action<Vector3> onUpdate,
            float duration = 1f, EaseType easeType = EaseType.Linear, Action onEnd = null, Action onAdd = null,
            Action onCancel = null, Action onFinally = null, Action onStart = null,
            float delay = 0f, int loops = 0, bool pingPong = false, float offset = 0f,
            AnimationCurve animationCurve = null, bool useUnscaledTime = false)
        {
            var tween = new Vector3Tween
            {
                from = from,
                to = to,
                duration = duration,
                easeType = easeType,
                delay = delay,
                loops = loops,
                usePingPong = pingPong,
                offset = offset,
                animationCurve = animationCurve,
                useUnscaledTime = useUnscaledTime,
                onUpdate = (_, value) => onUpdate(value),
                onEnd = instance => onEnd?.Invoke(),
                onAdd = instance => onAdd?.Invoke(),
                onCancel = instance => onCancel?.Invoke(),
                onFinally = instance => onFinally?.Invoke(),
                onStart = instance => onStart?.Invoke(),
            };
            return target.AddTween(tween);
        }

        // Vector4 tweening
        public static TweenInstance<Transform, Vector4> TweenVector4(
            GameObject target, Vector4 from, Vector4 to, Action<Vector4> onUpdate,
            float duration = 1f, EaseType easeType = EaseType.Linear, Action onEnd = null, Action onAdd = null,
            Action onCancel = null, Action onFinally = null, Action onStart = null,
            float delay = 0f, int loops = 0, bool pingPong = false, float offset = 0f,
            AnimationCurve animationCurve = null, bool useUnscaledTime = false)
        {
            var tween = new Vector4Tween
            {
                from = from,
                to = to,
                duration = duration,
                easeType = easeType,
                delay = delay,
                loops = loops,
                usePingPong = pingPong,
                offset = offset,
                animationCurve = animationCurve,
                useUnscaledTime = useUnscaledTime,
                onUpdate = (_, value) => onUpdate(value),
                onEnd = instance => onEnd?.Invoke(),
                onAdd = instance => onAdd?.Invoke(),
                onCancel = instance => onCancel?.Invoke(),
                onFinally = instance => onFinally?.Invoke(),
                onStart = instance => onStart?.Invoke(),
            };
            return target.AddTween(tween);
        }

        // Color tweening
        public static TweenInstance<Transform, Color> TweenColor(
            GameObject target, Color from, Color to, Action<Color> onUpdate,
            float duration = 1f, EaseType easeType = EaseType.Linear, Action onEnd = null, Action onAdd = null,
            Action onCancel = null, Action onFinally = null, Action onStart = null,
            float delay = 0f, int loops = 0, bool pingPong = false, float offset = 0f,
            AnimationCurve animationCurve = null, bool useUnscaledTime = false)
        {
            var tween = new ColorTween
            {
                from = from,
                to = to,
                duration = duration,
                easeType = easeType,
                delay = delay,
                loops = loops,
                usePingPong = pingPong,
                offset = offset,
                animationCurve = animationCurve,
                useUnscaledTime = useUnscaledTime,
                onUpdate = (_, value) => onUpdate(value),
                onEnd = instance => onEnd?.Invoke(),
                onAdd = instance => onAdd?.Invoke(),
                onCancel = instance => onCancel?.Invoke(),
                onFinally = instance => onFinally?.Invoke(),
                onStart = instance => onStart?.Invoke(),
            };
            return target.AddTween(tween);
        }

        // Quaternion tweening
        public static TweenInstance<Transform, Quaternion> TweenQuaternion(
            GameObject target, Quaternion from, Quaternion to, Action<Quaternion> onUpdate,
            float duration = 1f, EaseType easeType = EaseType.Linear, Action onEnd = null, Action onAdd = null,
            Action onCancel = null, Action onFinally = null, Action onStart = null,
            float delay = 0f, int loops = 0, bool pingPong = false, float offset = 0f,
            AnimationCurve animationCurve = null, bool useUnscaledTime = false)
        {
            var tween = new QuaternionTween
            {
                from = from,
                to = to,
                duration = duration,
                easeType = easeType,
                delay = delay,
                loops = loops,
                usePingPong = pingPong,
                offset = offset,
                animationCurve = animationCurve,
                useUnscaledTime = useUnscaledTime,
                onUpdate = (_, value) => onUpdate(value),
                onEnd = instance => onEnd?.Invoke(),
                onAdd = instance => onAdd?.Invoke(),
                onCancel = instance => onCancel?.Invoke(),
                onFinally = instance => onFinally?.Invoke(),
                onStart = instance => onStart?.Invoke(),
            };
            return target.AddTween(tween);
        }

        // Rect tweening
        public static TweenInstance<Transform, Rect> TweenRect(
            GameObject target, Rect from, Rect to, Action<Rect> onUpdate,
            float duration = 1f, EaseType easeType = EaseType.Linear, Action onEnd = null, Action onAdd = null,
            Action onCancel = null, Action onFinally = null, Action onStart = null,
            float delay = 0f, int loops = 0, bool pingPong = false, float offset = 0f,
            AnimationCurve animationCurve = null, bool useUnscaledTime = false)
        {
            var tween = new RectTween
            {
                from = from,
                to = to,
                duration = duration,
                easeType = easeType,
                delay = delay,
                loops = loops,
                usePingPong = pingPong,
                offset = offset,
                animationCurve = animationCurve,
                useUnscaledTime = useUnscaledTime,
                onUpdate = (_, value) => onUpdate(value),
                onEnd = instance => onEnd?.Invoke(),
                onAdd = instance => onAdd?.Invoke(),
                onCancel = instance => onCancel?.Invoke(),
                onFinally = instance => onFinally?.Invoke(),
                onStart = instance => onStart?.Invoke(),
            };
            return target.AddTween(tween);
        }

        // Method to move multiple GameObjects to specific positions over time
        public static List<TweenInstance<Transform, Vector3>> MoveToMultiple(
            List<GameObject> targets, Vector3 to, float duration = 1f, EaseType easeType = EaseType.Linear,
            Action onEnd = null, Action onAdd = null, Action onCancel = null, Action onFinally = null
            , Action onStart = null, Action<Vector3> onUpdate = null, float delay = 0f, int loops = 0, bool pingPong = false,
            float offset = 0f, AnimationCurve animationCurve = null, bool useUnscaledTime = false)
        {
            List<TweenInstance<Transform, Vector3>> tweens = new List<TweenInstance<Transform, Vector3>>();

            foreach (var target in targets)
            {
                var tween = new PositionTween
                {
                    to = to,
                    duration = duration,
                    easeType = easeType,
                    delay = delay,
                    loops = loops,
                    usePingPong = pingPong,
                    offset = offset,
                    animationCurve = animationCurve,
                    useUnscaledTime = useUnscaledTime,
                    onEnd = instance => onEnd?.Invoke()
                };
                tweens.Add(target.AddTween(tween));
            }

            return tweens;
        }

        // Method to scale multiple GameObjects to specific sizes over time
        public static List<TweenInstance<Transform, Vector3>> ScaleToMultiple(
            List<GameObject> targets, Vector3 to, float duration = 1f, EaseType easeType = EaseType.Linear,
            Action onEnd = null, Action onAdd = null, Action onCancel = null, Action onFinally = null
            , Action onStart = null, Action<Vector3> onUpdate = null, float delay = 0f, int loops = 0, bool pingPong = false,
            float offset = 0f, AnimationCurve animationCurve = null, bool useUnscaledTime = false)
        {
            List<TweenInstance<Transform, Vector3>> tweens = new List<TweenInstance<Transform, Vector3>>();

            foreach (var target in targets)
            {
                var tween = new LocalScaleTween
                {
                    to = to,
                    duration = duration,
                    easeType = easeType,
                    delay = delay,
                    loops = loops,
                    usePingPong = pingPong,
                    offset = offset,
                    animationCurve = animationCurve,
                    useUnscaledTime = useUnscaledTime,
                    onEnd = instance => onEnd?.Invoke()
                };
                tweens.Add(target.AddTween(tween));
            }

            return tweens;
        }

        // Method to rotate multiple GameObjects to specific rotations over time
        public static List<TweenInstance<Transform, Quaternion>> RotateToMultiple(
            List<GameObject> targets, Quaternion to, float duration = 1f, EaseType easeType = EaseType.Linear,
            Action onEnd = null, Action onAdd = null, Action onCancel = null, Action onFinally = null
            , Action onStart = null, Action<Quaternion> onUpdate = null, float delay = 0f, int loops = 0, bool pingPong = false,
            float offset = 0f, AnimationCurve animationCurve = null, bool useUnscaledTime = false)
        {
            List<TweenInstance<Transform, Quaternion>> tweens = new List<TweenInstance<Transform, Quaternion>>();

            foreach (var target in targets)
            {
                var tween = new RotationTween
                {
                    to = to,
                    duration = duration,
                    easeType = easeType,
                    delay = delay,
                    loops = loops,
                    usePingPong = pingPong,
                    offset = offset,
                    animationCurve = animationCurve,
                    useUnscaledTime = useUnscaledTime,
                    onEnd = instance => onEnd?.Invoke()
                };
                tweens.Add(target.AddTween(tween));
            }

            return tweens;
        }

        // Method to fade multiple SpriteRenderers to specific alpha over time
        public static List<TweenInstance<SpriteRenderer, float>> FadeToMultiple(
            List<GameObject> targets, float alpha, float duration = 1f, EaseType easeType = EaseType.Linear,
            Action onEnd = null, Action onAdd = null, Action onCancel = null, Action onFinally = null
            , Action onStart = null, Action<float> onUpdate = null, float delay = 0f, int loops = 0, bool pingPong = false,
            float offset = 0f, AnimationCurve animationCurve = null, bool useUnscaledTime = false)
        {
            List<TweenInstance<SpriteRenderer, float>> tweens = new List<TweenInstance<SpriteRenderer, float>>();

            foreach (var target in targets)
            {
                var spriteRenderer = target.GetComponent<SpriteRenderer>();
                if (spriteRenderer != null)
                {
                    FadeTo(target, alpha, duration, easeType, onEnd, onAdd, onCancel
                    , onFinally, onStart, onUpdate, delay, loops, pingPong, offset, animationCurve, useUnscaledTime);

                    var tween = new SpriteRendererAlphaTween
                    {
                        to = alpha,
                        duration = duration,
                        easeType = easeType,
                        delay = delay,
                        loops = loops,
                        usePingPong = pingPong,
                        offset = offset,
                        animationCurve = animationCurve,
                        useUnscaledTime = useUnscaledTime,
                        onEnd = instance => onEnd?.Invoke()
                    };
                    tweens.Add(spriteRenderer.gameObject.AddTween(tween));
                }
                else
                {
                    Debug.LogError($"No SpriteRenderer found on {target.name}!");
                }
            }
            return tweens;
        }

        // General cancel tweens for a GameObject
        public static void CancelTweens(GameObject target, bool includeChildren = false)
        {
            target.CancelTweens(includeChildren);
        }

        // Method to pause tweens
        public static void PauseTween<TTarget, TValue>(TweenInstance<TTarget, TValue> tweenInstance) where TTarget : Component
        {
            if (tweenInstance != null)
            {
                tweenInstance.isPaused = true;
            }
        }

        // Method to pause resume tweens
        public static void ResumeTween<TTarget, TValue>(TweenInstance<TTarget, TValue> tweenInstance) where TTarget : Component
        {
            if (tweenInstance != null)
            {
                tweenInstance.isPaused = false;
            }
        }

        // Method to pause multiple tweens
        public static void PauseTweens<TTarget, TValue>(List<TweenInstance<TTarget, TValue>> tweenInstances) where TTarget : Component
        {
            foreach (var tweenInstance in tweenInstances)
            {
                if (tweenInstance != null)
                {
                    tweenInstance.isPaused = true;
                }
            }
        }

        // Method to resume multiple tweens
        public static void ResumeTweens<TTarget, TValue>(List<TweenInstance<TTarget, TValue>> tweenInstances) where TTarget : Component
        {
            foreach (var tweenInstance in tweenInstances)
            {
                if (tweenInstance != null)
                {
                    tweenInstance.isPaused = false;
                }
            }
        }

        // Method to cancel multiple tweens
        public static void CancelTweens<TTarget, TValue>(List<TweenInstance<TTarget, TValue>> tweenInstances) where TTarget : Component
        {
            foreach (var tweenInstance in tweenInstances)
            {
                if (tweenInstance != null)
                {
                    tweenInstance.Cancel();
                }
            }
        }
    }
}

#endif